package siteo.seguridad

import grails.transaction.Transactional

@Transactional
class ValidaSesionService {

    Boolean sesionActiva(sesion) {
        if (!sesion.user)
            false
        else
            true
    }
    Boolean compruebaRol(sesion, rol){
        if(sesion.nombrerol == rol){
            true
        }else{
            false
        }
    }
}
