<%@ page import="siteo.administracion.catalogos.TipoVialidad" %>



<div class="fieldcontain ${hasErrors(bean: tipoVialidadInstance, field: 'tipovialidad', 'error')} required">
	<label for="tipovialidad">
		<g:message code="tipoVialidad.tipovialidad.label" default="Tipo de vialidad" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipovialidad" maxlength="20" required="" value="${tipoVialidadInstance?.tipovialidad}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoVialidadInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoVialidad.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoVialidadInstance?.estatus}" />

</div>

