/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utilerias;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author TEMOC
 */
public class UtilJasper {
    public UtilJasper(){}
    
    public byte[] generarPDF(String report_name, Map parameters)
    {
       byte[] bytes;
       Conexion conn = new Conexion();
       Connection conne = null;
       //Nombre del archivo de reporte  
       File arch_reporte = new File(report_name);
       //Pasamos parametros al reporte Jasper.
       //Llenando los parametros que pide el reporte
       try{
           //---conne = conn.conectar();
           conne = conn.conectarpruebas();
           bytes = JasperRunManager.runReportToPdf(arch_reporte.getPath(), parameters, conne);

       }
       catch ( Exception e ){
             System.out.println("Error al ejecutar runReport_toPDF -> " + e.getMessage());
             e.printStackTrace();
             bytes = null;
       }
       finally
       {
             try {
                 if (conne!= null && !conne.isClosed()) {
                     conne.close();
                 }
             } catch(SQLException e) {
                 System.out.println("Ocurrió un error al cerrar la conexión de datos:"+e.getMessage());
             }
       }
       return bytes;
    }
    
    public File generarXLS (String report_name, Map parameters, String ruta){
        OutputStream outputfile = null;
        Conexion conn = new Conexion();
        Connection conne = null;
        String outFileName = ruta+"\\reporte.xls";
        File reporte = null;
        try {
            conne = conn.conectarpruebas();
            //---conne = conn.conectar();
            JasperPrint print = JasperFillManager.fillReport(report_name, parameters, conne);//JasperFillManager.fillReport(fileName, hm, jasperReports);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            outputfile = new FileOutputStream(new File(outFileName));

            // Create a XLS exporter
            JRXlsExporter exporter = new JRXlsExporter();

            // Export the thing
            exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, output);
            exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 20000);
            try {
                exporter.exportReport();
            } catch (JRException ex) {
                Logger.getLogger(UtilJasper.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                outputfile.write(output.toByteArray());
                outputfile.flush();
            } catch (IOException ex) {
                Logger.getLogger(UtilJasper.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtilJasper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(UtilJasper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                outputfile.close();
                if (conne!= null && !conne.isClosed())
                    conne.close();
                reporte = new File(outFileName);
            } catch (SQLException ex) {
                Logger.getLogger(UtilJasper.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(UtilJasper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return reporte;
    }
    
}
