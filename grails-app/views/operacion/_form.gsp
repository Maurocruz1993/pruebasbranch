<%@ page import="siteo.seguridad.Operacion" %>



<div class="fieldcontain ${hasErrors(bean: operacionInstance, field: 'operacion', 'error')} required">
	<label for="operacion">
		<g:message code="operacion.operacion.label" default="Operación" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="operacion" maxlength="50" required="" value="${operacionInstance?.operacion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: operacionInstance, field: 'controlador', 'error')} required">
	<label for="controlador">
		<g:message code="operacion.controlador.label" default="Controlador" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="controlador" maxlength="100" required="" value="${operacionInstance?.controlador}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: operacionInstance, field: 'accion', 'error')} required">
	<label for="accion">
		<g:message code="operacion.accion.label" default="Acción" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="accion" maxlength="100" required="" value="${operacionInstance?.accion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: operacionInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="operacion.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${operacionInstance?.estatus}" />

</div>

<div class="fieldcontain ${hasErrors(bean: operacionInstance, field: 'parametros', 'error')} ">
	<label for="parametros">
		<g:message code="operacion.parametros.label" default="Parametros" />
		
	</label>
	<g:textField name="parametros" maxlength="100" value="${operacionInstance?.parametros}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: operacionInstance, field: 'categoria', 'error')} required">
	<label for="categoria">
		<g:message code="operacion.categoria.label" default="Categoria" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="categoria" name="categoria.id" from="${siteo.seguridad.CategoriaOp.list()}" optionKey="id" required="" value="${operacionInstance?.categoria?.id}" class="many-to-one"/>

</div>

