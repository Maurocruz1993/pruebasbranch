package siteo.administracion.catalogos

class EstatusSolicitud {

    String estatussolicitud
    Boolean estatus
    
    static constraints = {
        estatussolicitud(nullable: false, size:1..45, blank: false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        estatussolicitud
    }
}
