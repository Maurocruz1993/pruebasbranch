<%@ page import="siteo.seguridad.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#edit-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="edit-usuario" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${usuarioInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${usuarioInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:usuarioInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${usuarioInstance?.version}" />
				<fieldset class="form">
					<g:render template="formEdit"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                                        <g:field type="button" value="Reset Contraseña" name="contraseña" onClick="resetPass()" />
				</fieldset>
			</g:form>
		</div>
                <div id="dialog-pass" title="Cambiar Contraseña">
                    <p id="agregaMensaje" class="error">Nueva Contraseña</p>
                    Nueva:<g:field type="password" name="pass1" required=""/> <br/>
                    Repita:<g:field type="password" name="pass2" required=""/>
                    <br/>
                </div>
                <script function="javascript">
                    function resetPass(){
                        $( "#dialog-pass" ).dialog( "open" );
                    }
                    $(function() {
                        $( "#dialog-pass" ).dialog({
                        resizable: false,
                        width:300,
                        height:230,
                        modal: true,
                        autoOpen: false,
                        buttons: {
                            "Aceptar": function() {
                                if($("#pass1").val() !=""){
                                    if($("#pass2").val() != ""){
                                        if($("#pass1").val() == $("#pass2").val()){
                                               mandaPass();
                                                $( this ).dialog( "close" );
                                        }
                                        else{
                                            $("#pass1").val("");
                                            $("#pass2").val("");
                                            $("#pass1").focus();
                                        }
                                    }
                                    else{
                                        $("#pass2").focus();
                                    }
                                }else{
                                    $("#pass1").focus();
                                }
                            },
                            "Cancelar": function() {
                                $( this ).dialog( "close" );
                            }
                        }
                        });
                    });
                    function mandaPass(){
                        $.post("../resetPass", {usuario:${usuarioInstance.id}, pass:$("#pass1").val() }, function(){
                            MostrarMensaje("Contraseña modificada")
                        })
                        $("#pass1").val("");
                        $("#pass2").val("");
                    }
                    </script>
	</body>
</html>
