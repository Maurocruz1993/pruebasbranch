
<%@ page import="siteo.administracion.catalogos.EstatusSolicitud" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'estatusSolicitud.label', default: 'Estatus Solicitud')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-estatusSolicitud" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-estatusSolicitud" class="content scaffold-show" role="main">
			<h1><g:message code="Mostrar" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list estatusSolicitud">
			
				<g:if test="${estatusSolicitudInstance?.estatussolicitud}">
				<li class="fieldcontain">
					<span id="estatussolicitud-label" class="property-label"><g:message code="estatusSolicitud.estatussolicitud.label" default="Estatus Solicitud" /></span>
					
						<span class="property-value" aria-labelledby="estatussolicitud-label"><g:fieldValue bean="${estatusSolicitudInstance}" field="estatussolicitud"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${estatusSolicitudInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="estatusSolicitud.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${estatusSolicitudInstance?.estatus}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:estatusSolicitudInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${estatusSolicitudInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
