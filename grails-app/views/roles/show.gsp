
<%@ page import="siteo.seguridad.Roles" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'roles.label', default: 'Roles')}" />
		<title>Muestra Rol</title>
	</head>
	<body>
		<a href="#show-roles" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">Principal</a></li>
				<li><g:link class="list" action="index">Lista</g:link></li>
				<li><g:link class="create" action="create">Nuevo</g:link></li>
			</ul>
		</div>
		<div id="show-roles" class="content scaffold-show" role="main">
			<h1>Rol</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list roles">
			
				<g:if test="${rolesInstance?.rol}">
				<li class="fieldcontain">
					<span id="rol-label" class="property-label"><g:message code="roles.rol.label" default="Rol" /></span>
					
						<span class="property-value" aria-labelledby="rol-label"><g:fieldValue bean="${rolesInstance}" field="rol"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${rolesInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="roles.estatus.label" default="estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label">
                                                    <g:if test="${rolesInstance.estatus}">ACTIVO</g:if><g:else>BAJA</g:else>
                                                </span>
					
				</li>
				</g:if>
			
				<g:if test="${rolesInstance?.operacion}">
				<li class="fieldcontain">
					<span id="operacion-label" class="property-label"><g:message code="roles.operacion.label" default="Operacion" /></span>
					
						<g:each in="${rolesInstance.operacion}" var="o">
						<span class="property-value" aria-labelledby="operacion-label"><g:link controller="operacion" action="show" id="${o.id}">${o?.encodeAsHTML()} - ${o.categoria}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
                        <!--
			<g:form url="[resource:rolesInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${rolesInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>-->
		</div>
	</body>
</html>
