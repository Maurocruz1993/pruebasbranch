package siteo.administracion.catalogos

class UsoSuelo {

    String usosuelo
    Boolean estatus
    
    static constraints = {
        usosuelo (nullable:false, size:1..20, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        usosuelo
    }
}
