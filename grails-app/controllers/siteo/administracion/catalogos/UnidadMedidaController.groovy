package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UnidadMedidaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond UnidadMedida.list(params), model:[unidadMedidaInstanceCount: UnidadMedida.count()]
    }

    def show(UnidadMedida unidadMedidaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond unidadMedidaInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new UnidadMedida(params)
    }

    @Transactional
    def save(UnidadMedida unidadMedidaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (unidadMedidaInstance == null) {
            notFound()
            return
        }

        if (unidadMedidaInstance.hasErrors()) {
            respond unidadMedidaInstance.errors, view:'create'
            return
        }

        unidadMedidaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'unidadMedida.label', default: 'UnidadMedida'), unidadMedidaInstance.id])
                redirect unidadMedidaInstance
            }
            '*' { respond unidadMedidaInstance, [status: CREATED] }
        }
    }

    def edit(UnidadMedida unidadMedidaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond unidadMedidaInstance
    }

    @Transactional
    def update(UnidadMedida unidadMedidaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (unidadMedidaInstance == null) {
            notFound()
            return
        }

        if (unidadMedidaInstance.hasErrors()) {
            respond unidadMedidaInstance.errors, view:'edit'
            return
        }

        unidadMedidaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UnidadMedida.label', default: 'UnidadMedida'), unidadMedidaInstance.id])
                redirect unidadMedidaInstance
            }
            '*'{ respond unidadMedidaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UnidadMedida unidadMedidaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (unidadMedidaInstance == null) {
            notFound()
            return
        }

        unidadMedidaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UnidadMedida.label', default: 'UnidadMedida'), unidadMedidaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def unidad = UnidadMedida.get(params.idele)
        unidad.estatus = !unidad.estatus
        unidad.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'unidadMedida.label', default: 'UnidadMedida'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
