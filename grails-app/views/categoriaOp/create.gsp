<!DOCTYPE html>
<%@ page import="siteo.seguridad.CategoriaOp" %>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'categoriaOp.label', default: 'CategoriaOp')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
                <script type="text/javascript" language="javascript">
                    $(document).ready(function(){
                        $("#estatus").attr("checked","true");
                        $("#sub").click(function(){
                            if($("#sub").is(":checked")){
                                $("#divCat").attr("style", "display:none");
                            }else{
                                $("#divCat").attr("style", "display:");
                            }
                        });
                    });
                </script>
	</head>
	<body>
		<a href="#create-categoriaOp" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-categoriaOp" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${categoriaOpInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${categoriaOpInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:categoriaOpInstance, action:'save']" >
				<fieldset class="form">
					<g:render template="form"/>
                                        <div id="divCat" style="display:none" class="fieldcontain ${hasErrors(bean: categoriaOpInstance, field: 'categoria', 'error')} ">
                                            <label for="catenviada" >Seleccione Categoria Padre</label>
                                           <g:select name="catenviada" from="${CategoriaOp.list(sort:"categoria")}"  optionKey="id" valueKey="categoria" />
                                        </div>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
