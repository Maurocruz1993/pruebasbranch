
<%@ page import="siteo.administracion.catalogos.MovimientoCatastral" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'movimientoCatastral.label', default: 'MovimientoCatastral')}" />
		<title><g:message code="Lista" args="[entityName]" /></title>
	</head>
	<body>
            <g:form name="festatus" controller="MovimientoCatastral" action="cambiaEstatus">
                <input id="idele" name="idele" type="hidden" value=""/>
            </g:form>
		<a href="#list-movimientoCatastral" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-movimientoCatastral" class="content scaffold-list" role="main">
			<h1><g:message code="Lista" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="clave" title="${message(code: 'movimientoCatastral.clave.label', default: 'Clave')}" />
					
						<g:sortableColumn property="movimiento" title="${message(code: 'movimientoCatastral.movimiento.label', default: 'Movimiento')}" />
					
						<g:sortableColumn property="tipotramite" title="${message(code: 'movimientoCatastral.tipotramite.label', default: 'Tipo de Tramite')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'movimientoCatastral.estatus.label', default: 'Estatus')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${movimientoCatastralInstanceList}" status="i" var="movimientoCatastralInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${movimientoCatastralInstance.id}">${fieldValue(bean: movimientoCatastralInstance, field: "clave")}</g:link></td>
					
						<td>${fieldValue(bean: movimientoCatastralInstance, field: "movimiento")}</td>
					
						<td>${fieldValue(bean: movimientoCatastralInstance, field: "tipotramite")}</td>
					
						<td><g:formatBoolean boolean="${movimientoCatastralInstance.estatus}" true="ACTIVO" false="BAJA" />
                                                    <a href="javascript: CambiaEstatus('${movimientoCatastralInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${movimientoCatastralInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
		</div>
	</body>
</html>
