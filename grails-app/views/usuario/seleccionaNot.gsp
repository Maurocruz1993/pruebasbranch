<html>
    <head>
        <meta name="layout" content="main"/>
        <asset:stylesheet src="jmenu.css"/>
        <asset:javascript src="jMenu.jquery.min.js"/>
        <title>Captura Masiva</title>
        <style type="text/css">
            label{
                    float:left;
                    width:65px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function(){
                // simple jMenu plugin called
                //$("#jMenu").jMenu();

                // more complex jMenu plugin called
                $("#jMenu").jMenu({
                  ulWidth : 200,
                  effects : {
                        effectSpeedOpen : 300,
                        effectTypeClose : 'slide'
                  },
                  animatedText : true
                });
            });
        </script>		
    </head>
    <body>
        <g:form name="frm" controller="master" action="index">
            <input id="vista" name="vista" type="hidden" value=""/>
            <input id="control" name="control" type="hidden" value=""/>
            <input id="pagina" name="pagina" type="hidden" value="menu"/>
            <input id="operacion" name="operacion" type="hidden" value=""/>
        </g:form>
        
        <g:if test="${session.user}">
            <br/>
            <div>
                <ul id="jMenu">
                    <g:if test="${session.numRol > 1}">
                        <li><g:link controller="Usuario" action="seleccionaRol" id="${session.user}">Cambiar Rol</g:link></li>
                    </g:if>
                  <li><g:link controller="usuario" action="logout">Salir</g:link></li>
                </ul>
            </div>
        </g:if>
        <g:if test="${flash.message}">
            <div class="message">
                    ${flash.message}
            </div>
        </g:if>
            <br>
            <br>
            <div class="fieldcontain">
            <g:form action="notSelec">
                <label for="nota">Seleccione una notaria:</label>
                <g:select name="nota" from="${roles}" optionKey="id"  />
                <g:submitButton name="enviar" value="Seleccionar" action="rolSelec"/>
            </g:form>
            </div>
    </body>
</html>