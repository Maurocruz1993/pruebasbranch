<%@ page import="siteo.administracion.catalogos.UnidadMedida" %>



<div class="fieldcontain ${hasErrors(bean: unidadMedidaInstance, field: 'unidad', 'error')} required">
	<label for="unidad">
		<g:message code="unidadMedida.unidad.label" default="Unidad" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="unidad" maxlength="20" required="" value="${unidadMedidaInstance?.unidad}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: unidadMedidaInstance, field: 'abreviacion', 'error')} required">
	<label for="abreviacion">
		<g:message code="unidadMedida.abreviacion.label" default="Abreviacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="abreviacion" maxlength="5" required="" value="${unidadMedidaInstance?.abreviacion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: unidadMedidaInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="unidadMedida.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${unidadMedidaInstance?.estatus}" />

</div>

