<%@ page import="siteo.administracion.catalogos.UsoSuelo" %>



<div class="fieldcontain ${hasErrors(bean: usoSueloInstance, field: 'usosuelo', 'error')} required">
	<label for="usosuelo">
		<g:message code="usoSuelo.usosuelo.label" default="Uso suelo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="usosuelo" maxlength="20" required="" value="${usoSueloInstance?.usosuelo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usoSueloInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="usoSuelo.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${usoSueloInstance?.estatus}" />

</div>

