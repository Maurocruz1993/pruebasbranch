
<%@ page import="siteo.administracion.catalogos.EstadoConservacion" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'estadoConservacion.label', default: 'EstadoConservacion')}" />
		<title><g:message code="Lista" args="[entityName]" /></title>
	</head>
	<body>
            <g:form name="festatus" controller="EstadoConservacion" action="cambiaEstatus">
                <input id="idele" name="idele" type="hidden" value=""/>
            </g:form>
		<a href="#list-estadoConservacion" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-estadoConservacion" class="content scaffold-list" role="main">
			<h1><g:message code="Lista" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="estadoConservacion" title="${message(code: 'estadoConservacion.estadoConservacion.label', default: 'Estado Conservacion')}" />
					
						<g:sortableColumn property="factor" title="${message(code: 'estadoConservacion.factor.label', default: 'Factor')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'estadoConservacion.estatus.label', default: 'Estatus')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${estadoConservacionInstanceList}" status="i" var="estadoConservacionInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${estadoConservacionInstance.id}">${fieldValue(bean: estadoConservacionInstance, field: "estadoConservacion")}</g:link></td>
					
						<td>${fieldValue(bean: estadoConservacionInstance, field: "factor")}</td>
					
						<td><g:formatBoolean boolean="${estadoConservacionInstance.estatus}" true="ACTIVO" false="BAJA" />
                                                    <a href="javascript: CambiaEstatus('${estadoConservacionInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a>
                                                </td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${estadoConservacionInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
		</div>
	</body>
</html>
