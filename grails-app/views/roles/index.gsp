
<%@ page import="siteo.seguridad.Roles" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'roles.label', default: 'Roles')}" />
		<title>Lista</title>
	</head>
	<body>
		<a href="#list-roles" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">Principal</a></li>
				<li><g:link class="create" action="create">Nuevo</g:link></li>
			</ul>
		</div>
		<div id="list-roles" class="content scaffold-list" role="main">
			<h1>Roles</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="rol" title="${message(code: 'roles.rol.label', default: 'Rol')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'roles.estatus.label', default: 'Estatus')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${rolesInstanceList}" status="i" var="rolesInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${rolesInstance.id}">${fieldValue(bean: rolesInstance, field: "rol")}</g:link></td>
					
						<td><g:if test="${rolesInstance.estatus}">ACTIVO</g:if><g:else>BAJA</g:else>
                                                    <a href="javascript: CambiaEstatus('${rolesInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${rolesInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
                        <g:form name="festatus" controller="Roles" action="cambiaEstatus">
                            <input id="idele" name="idele" type="hidden" value=""/>
                        </g:form>
		</div>
	</body>
</html>
