package siteo.seguridad



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class OperacionController {

    def validaSesionService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        params.sort = "operacion"
        params.order ="asc"
        params.max = Math.min(max ?: 10, 100)
        respond Operacion.list(params), model:[operacionInstanceCount: Operacion.count()]
    }

    def show(Operacion operacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        respond operacionInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        respond new Operacion(params)
    }

    @Transactional
    def save(Operacion operacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        if (operacionInstance == null) {
            notFound()
            return
        }

        if (operacionInstance.hasErrors()) {
            respond operacionInstance.errors, view:'create'
            return
        }

        operacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'operacion.label', default: 'Operacion'), operacionInstance.id])
                redirect operacionInstance
            }
            '*' { respond operacionInstance, [status: CREATED] }
        }
    }

    def edit(Operacion operacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        respond operacionInstance
    }

    @Transactional
    def update(Operacion operacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        if (operacionInstance == null) {
            notFound()
            return
        }

        if (operacionInstance.hasErrors()) {
            respond operacionInstance.errors, view:'edit'
            return
        }

        operacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Operacion.label', default: 'Operacion'), operacionInstance.id])
                redirect operacionInstance
            }
            '*'{ respond operacionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Operacion operacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        if (operacionInstance == null) {
            notFound()
            return
        }

        operacionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Operacion.label', default: 'Operacion'), operacionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def instancia = Operacion.get(params.idele)
        instancia.estatus = !instancia.estatus
        instancia.save flush:true
        
        redirect(action: 'index')
    }
    
     def buscar(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        try{
            def op = Operacion.createCriteria()
            def r2 = op.list {
                ilike("operacion", "%"+params.criterio+"%")
            }
            if(!r2){
                //System.out.println(params.criterio)
            }
            [operacionInstanceList:r2]
        }catch(Exception e){
            flash.message = e.message
        }
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'operacion.label', default: 'Operacion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
