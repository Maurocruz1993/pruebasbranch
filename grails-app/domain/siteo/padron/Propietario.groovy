package siteo.padron

import siteo.administracion.catalogos.TipoPersona
import siteo.administracion.catalogos.TipoPropietario

class Propietario {

    TipoPersona tipopersona
    String paterno
    String materno
    String nombre
    String razonsocial
    String rfc
    String curp
    TipoPropietario tipopropietario
    BigDecimal porcentaje
    String sexo
    String nacionalidad;
    String telefono
    String ycop
    
    static belongsTo = [datosdeclarados: DatosDeclarados]
    
    static constraints = {
        tipopersona (nullable:false)
        paterno (nullable:true, size:1..255, blank:true)
        materno (nullable:true, size:1..255, blank:true)
        nombre (nullable:true, size:1..255, blank:true)
        razonsocial (nullable:true, size:1..255, blank:true)
        rfc (nullable:true, size:1..15, blank:true)
        curp (nullable:true, size:1..18, blank:true)
        tipopropietario (nullable:false)
        porcentaje (nullable:true, scale:3, min: 0.000, max: 100.000)
        sexo(nullable:true,blank:true,size:1..3)
        telefono(nullable:true,blank:true,size:1..15)
        nacionalidad(nullable:true,blank:true,size:1..25)
        ycop (nullable:true, size:1..5, blank:true)
    }
    
    static mapping = {
        id generator: 'assigned'
    }

    Long guarda(prop){
        Boolean ok = false
        int error=0;
        while (!ok){
            Propietario.withTransaction{
                status->
                try{
                    Long id = Propietario.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    prop.id = id
                    prop.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
    
    String nombrecompleto(){
        String fullname = nombre
        if (paterno && !paterno.equals(""))
            fullname +=" "+paterno
        if (materno && !materno.equals(""))
            fullname +=" "+materno
            
        fullname
    }
    
    String nombrecompletoporapellidos(){
        String fullname = ""
        if (paterno && !paterno.equals(""))
            fullname += paterno
        if (materno && !materno.equals(""))
            fullname +=" "+materno
        if (nombre && !nombre.equals(""))
            fullname +=" "+nombre
            
        fullname
    }
}
