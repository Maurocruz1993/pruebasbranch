
<%@ page import="siteo.seguridad.CategoriaOp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'categoriaOp.label', default: 'CategoriaOp')}" />
		<title>Categorias</title>
	</head>
	<body>
		<a href="#list-categoriaOp" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">Principal</a></li>
				<li><g:link class="create" action="create">Nuevo</g:link></li>
			</ul>
		</div>
		<div id="list-categoriaOp" class="content scaffold-list" role="main">
			<h1>Categorias</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="categoria" title="${message(code: 'categoriaOp.categoria.label', default: 'Nombre Categoria')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'categoriaOp.estatus.label', default: 'Estatus')}" />
					
						<g:sortableColumn property="padre" title="${message(code: 'categoriaOp.padre.label', default: 'Padre')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${categoriaOpInstanceList}" status="i" var="categoriaOpInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${categoriaOpInstance.id}">${fieldValue(bean: categoriaOpInstance, field: "categoria")}</g:link></td>
					
						<td><g:if test="${categoriaOpInstance.estatus}">ACTIVO</g:if><g:else>BAJA</g:else>
                                                <a href="javascript: CambiaEstatus('${categoriaOpInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a>    
                                                </td>
					
						<!--<td>${fieldValue(bean: categoriaOpInstance, field: "padre")}</td>-->
                                                <td>
                                                    <g:if test="${categoriaOpInstance.padre == categoriaOpInstance.id}">
                                                        Categoria
                                                    </g:if>
                                                    <g:else>
                                                        Sub-categoria de ${CategoriaOp.get(categoriaOpInstance.padre)}
                                                    </g:else>
                                                </td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${categoriaOpInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
                        <g:form name="festatus" controller="CategoriaOp" action="cambiaEstatus">
                            <input id="idele" name="idele" type="hidden" value=""/>
                        </g:form>
		</div>
	</body>
</html>