package siteo.administracion.catalogos

class EstadoConservacion {

    String estadoConservacion
    BigDecimal factor
    Boolean estatus
    static constraints = {
        estadoConservacion(nullable:false, size:1..20, blank:false)
        factor(nullable:true)
    }
     static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        estadoConservacion
    }
}
