package siteo.administracion.catalogos

class TipoConstruccion {

    String tipoconstruccion
    Boolean estatus

    static constraints = {
        tipoconstruccion (nullable: false, size:1..25, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipoconstruccion
    }
}
