
<%@ page import="siteo.administracion.catalogos.EntidadesCat" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'entidadesCat.label', default: 'EntidadesCat')}" />
		<title><g:message code="Entidad Catastral" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-entidadesCat" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-entidadesCat" class="content scaffold-show" role="main">
			<h1><g:message code="Entidad catastral" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list entidadesCat">
			
				<g:if test="${entidadesCatInstance?.nombre}">
				<li class="fieldcontain">
					<span id="nombre-label" class="property-label"><g:message code="entidadesCat.nombre.label" default="Nombre" /></span>
					
						<span class="property-value" aria-labelledby="nombre-label"><g:fieldValue bean="${entidadesCatInstance}" field="nombre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${entidadesCatInstance?.clave}">
				<li class="fieldcontain">
					<span id="clave-label" class="property-label"><g:message code="entidadesCat.clave.label" default="Clave" /></span>
					
						<span class="property-value" aria-labelledby="clave-label"><g:fieldValue bean="${entidadesCatInstance}" field="clave"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${entidadesCatInstance?.abrev}">
				<li class="fieldcontain">
					<span id="abrev-label" class="property-label"><g:message code="entidadesCat.abrev.label" default="Abrev" /></span>
					
						<span class="property-value" aria-labelledby="abrev-label"><g:fieldValue bean="${entidadesCatInstance}" field="abrev"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:entidadesCatInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${entidadesCatInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
