<%@ page import="siteo.administracion.catalogos.*" %>
<%@ page import="siteo.seguridad.Delegacion" %>
<%@ page import="siteo.seguridad.Municipio" %>
<html>
    <head>
        <meta name="layout" content="main"/>  
        <asset:javascript src="functionsCapturaDatos.js"/> 
        <title>Captura Masiva</title>   
        <script type="text/javascript" language="javascript">
            comboAutocompletar("#lstMun");
            $(document).ready(function () {
                window.history.pushState(null, "Captura Masiva", '${request.contextPath}/solicitudMasiva/index');
                
                if ($("#lstMun").val() > 0) {
                    buscadelegacion($("#lstMun").val());
                }
                
                $("#selectMun input").autocomplete({
                    select:function( event, ui ) {
                        buscadelegacion($("#lstMun").val());
                    }
                });
                
                $("#guardardatos").click(function(){
                    var idmuni=$("#lstMun").val();
                    if(!idmuni || parseInt(idmuni)==0){
                        MostrarMensaje("Seleccione un municipio");
                        $("#guardarmasivo").submit(function(event){
                            event.preventDefault();
                        });
                        return;
                    }
                });
            });
            function buscadelegacion(municipio) {
                if (municipio != 0) {
                    var URL = "${createLink(controller:'SolicitudMasiva',action:'buscadelegacion')}"
                    $.ajax({
                        url: URL,
                        type: "GET",
                        data: {id: municipio},
                        success: function (respuesta, textStatus, jqXHR) {
                            console.log(respuesta)
                            $("#nombredelegacion").text(respuesta.clave + " - " + respuesta.nombre);
                            $("#auxdelegacion").val(respuesta.id);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {},
                        complete: function (jqXHR, textStatus) {}
                    });
                } else {
                    $("#nombredelegacion").text('');
                    $("#auxdelegacion").val('');
                }
            }
        </script>
        <style type="text/css">
            .ui-autocomplete {
            max-height: 155px;
            overflow-y: auto;
            overflow-x: hidden;
            }

            html .ui-autocomplete {
            height: 155px;
            }

            #lstmov .custom-combobox-input {
            width: 300px;
            }

            #lstdelegc .custom-combobox-input {
            width: 300px;
            }
            #lstmuni .custom-combobox-input {
            width: 380px;
            }
        </style>
    </head>
    <body> 
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            </ul>
        </div>
        <div id="list-solicitudMasiva" class="content scaffold-create" role="main">
            <g:if test="${flash.message}">
                <div class="message" role="status" ><span class="error">${flash.message}</span></div>
                </g:if>
                <!--<g:link action="descargarZip" controller="SolicitudMasiva">Descargar shape en zip</g:link>-->
            <h1>
                <g:message code="Carga masiva de datos" args="[entityName]" />
            </h1>
            <g:uploadForm action="guardamasivo" controller="SolicitudMasiva">
            <fieldset class="form">
                <div class="fieldcontain">            
                    <div id="lstmuni">
                        <div id="selectMun">
                            <label>Municipio</label>
                            <g:select from="${Municipio.findAllByIdIsNotNull([sort:'clave',order:'asc'])}" id="lstMun" name="municipio" value="" optionValue="${{it.nombreconclave()}}" optionKey="id" noSelection="['0':'- Seleccione un municipio -']" required="required"/>
                        </div>
                    </div>           
                </div>
                <div class="fieldcontain">
                    <label>Delegación</label>
                    <label id="nombredelegacion" size="30" style="text-align: left !important;"></label>
                    <g:hiddenField id="auxdelegacion" name="delegacion" value=""/>
                </div>
                <div class="fieldcontain">
                    <label>Localidad</label>
                    <input type="text" id="localidad" name="localidad" size="50"  maxlength="250" required="required" onblur="Mayusculas(this)"> 
                </div>
                <div class="fieldcontain">
                    <label>Núcleo Agrario</label>
                    <input type="text" name="nucleoagrario" size="50"  id="nucleoagrario" maxlength="250" onblur="Mayusculas(this)">
                </div>
                <div class="fieldcontain"> 
                    <label>Observaciones</label>
                    <textarea   rows="4" cols="52"  name="observaciones" maxlength="250" id="observaciones" onblur="Mayusculas(this)"></textarea>
                </div>
                <h1><g:message code="Carga de datos alfanuméricos(Excel)" /></h1>
                <div class="fieldcontain">
                    <label><g:message code="Seleccionar archivo excel" /></label>
                    <input type="file" name="excel" required="required" accept=".xls,.xlsx"/>
                </div>
                <h1><g:message code="Carga de datos cartográficos (Shape)"/></h1>
                <div class="fieldcontain">
                    <label><g:message code="Seleccionar archivo .zip" /></label>
                    <input type="file" name="zip" required="required" accept=".zip,.rar"/>
                </div>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="guardardatos" value="Guardar datos" class="save" id="guardardatos" />
                </fieldset>
            </g:uploadForm>
        </div>
    </body>
</html>