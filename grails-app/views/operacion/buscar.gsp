
<%@ page import="siteo.seguridad.Operacion" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'operacion.label', default: 'Operacion')}" />
		<title>Operaciones</title>
	</head>
	<body>
		<a href="#list-operacion" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">Principal</a></li>
                                <li><g:link class="list" action="index">Lista</g:link></li>
				<li><g:link class="create" action="create">Nuevo</g:link></li>
			</ul>
		</div>
		<div id="list-operacion" class="content scaffold-list" role="main">
			<h1>Operaciones</h1>
                        <g:form action="buscar">
                            Nombre de Operaci&oacute;n:
                            <g:textField name="criterio" required="" autofocus="" />
                            <g:submitButton name="Buscar" action="buscar" value="Buscar"/>
                        </g:form>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="operacion" title="${message(code: 'operacion.operacion.label', default: 'Nombre Operaci&oacute;n')}" />
					
						<g:sortableColumn property="controlador" title="${message(code: 'operacion.controlador.label', default: 'Controlador')}" />
					
						<g:sortableColumn property="accion" title="${message(code: 'operacion.accion.label', default: 'Accion')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'operacion.estatus.label', default: 'Estatus')}" />
					
						<th><g:message code="operacion.categoria.label" default="Categoria" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${operacionInstanceList}" status="i" var="operacionInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${operacionInstance.id}">${fieldValue(bean: operacionInstance, field: "operacion")}</g:link></td>
					
						<td>${fieldValue(bean: operacionInstance, field: "controlador")}</td>
					
						<td>${fieldValue(bean: operacionInstance, field: "accion")}</td>
					
						<td><g:if test="${operacionInstance.estatus}">ACTIVO</g:if><g:else>BAJA</g:else>
                                                    <a href="javascript: CambiaEstatus('${operacionInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a></td>
					
						<td>${fieldValue(bean: operacionInstance, field: "categoria")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${operacionInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
                        <g:form name="festatus" controller="Operacion" action="cambiaEstatus">
                            <input id="idele" name="idele" type="hidden" value=""/>
                        </g:form>
		</div>
	</body>
</html>
