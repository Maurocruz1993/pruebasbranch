<%@ page import="siteo.seguridad.Roles" %>
<%@ page import="siteo.seguridad.Operacion" %>



<div class="fieldcontain ${hasErrors(bean: rolesInstance, field: 'rol', 'error')} required">
	<label for="rol">
		<g:message code="roles.rol.label" default="Nombre del Rol:" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="rol" required="" value="${rolesInstance?.rol}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rolesInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="roles.estatus.label" default="Estatus:" />
		
	</label>
	<g:checkBox name="estatus" value="${rolesInstance?.estatus}" />

</div>

<div class="fieldcontain ${hasErrors(bean: rolesInstance, field: 'operacion', 'error')} ">
	<label for="operacion">
		<g:message code="roles.operacion.label" default="Operaci&oacute;n:" />
		
	</label>
	<!--<g:select name="operacion" from="${siteo.seguridad.Operacion.listOrderByOperacion()}" optionValue="${{it.operacion + " - " + it.categoria}}" multiple="multiple" optionKey="id" size="5" value="${rolesInstance?.operacion*.id}" class="many-to-many"/>
        -->
        <g:select name="operacion" from="${Operacion.findAllByEstatus(true,[sort:'operacion'])}" optionValue="${{it.operacion + " - " + it.categoria}}" multiple="multiple" optionKey="id" size="6" value="${rolesInstance?.operacion*.id}" class="many-to-many"/>

</div>

