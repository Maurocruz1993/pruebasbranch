package siteo.seguridad

import grails.transaction.Transactional
import siteo.administracion.catalogos.*
import siteo.seguridad.Roles

@Transactional
class UsuarioService {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def bitacoraService
    
    Boolean valida(params) {
        String us = params.usuario
        String pw = params.passwd

        def c = Usuario.createCriteria()

        def usExis = c.get {
            eq("usuario", us)
            eq("passwd", pw.encodeAsSHA1())
        }

        if (!usExis){
            false
        } else if (!usExis.estatus){
            false
        } else {
            true
        }
    }
    
    Usuario obtenerPorUsuarioYPass(params) {
        String us = params.usuario
        String pw = params.passwd

        def c = Usuario.createCriteria()

        def usuario = c.get {
            eq("usuario", us)
            eq("passwd", pw.encodeAsSHA1())
        }
    }
    
    def menu (Long idrol) {
        //def notario = Notario.get(idnot)
        def rol = Roles.get(idrol)
        Map menu = [:]
        def op = rol.operacion.sort{it.operacion}
        def ops = []
        def categorias = []
        def subcategorias = []
        def catsS = []
        def subcatsS = []
        
        op.each() {
            if(it.estatus){
                ops.add(it)
                if(it.categoria.estatus == true){
                    if (it.categoria.id==it.categoria.padre){
                        if (catsS.isEmpty()){
                            categorias.add(it.categoria)
                            catsS.add(it.categoria.categoria)
                        } else if (!catsS.contains(it.categoria.categoria)){
                            categorias.add(it.categoria)
                            catsS.add(it.categoria.categoria)
                        }
                    } else {
                        //obtener padre de subcategoria y agregar a categorias
                        //def padre = Categoria_operacion.get(it.categoria.padre)
                        def padre = CategoriaOp.get(it.categoria.padre)
                        if (catsS.isEmpty()){
                            categorias.add(padre)
                            catsS.add(padre.categoria)
                        } else if (!catsS.contains(padre.categoria)){
                            categorias.add(padre)
                            catsS.add(padre.categoria)
                        }
                        //agregar la subcategoria
                        if (subcatsS.isEmpty()){
                            subcategorias.add(it.categoria)
                            subcatsS.add(it.categoria.categoria)
                        } else if (!subcatsS.contains(it.categoria.categoria)) {
                            subcategorias.add(it.categoria)
                            subcatsS.add(it.categoria.categoria)
                        }
                    }
                }
            }
        }
        menu.put("categorias",categorias)
        menu.put("subcategorias",subcategorias)
        menu.put("operaciones",ops)
        menu
    }
    
    Integer totalRoles(Usuario user){
        def rol = user.roles.id
        def rRol = Roles.findAllByEstatusAndIdInList("true",rol)
        return rRol.size()
    }
    
    Long rolActivo(Usuario user){
        def rol = user.roles.id
        def rRol = Roles.findAllByEstatusAndIdInList("true",rol)
        return rRol[0].id
    }
    
    String nombreRol(Long idrol){
        def rol = Roles.findById(idrol)
        return rol.rol
    }
    
    Integer totalNotario(Usuario user){
        def not = user.notarias.id
        def rNot = Notario.findAllByEstatusAndIdInList("true",not)
        return rNot.size()
    }
    
    Long notarioActivo(Usuario user){
        def not = user.notarias.id
        def rNot = Notario.findAllByEstatusAndIdInList("true",not)
        return rNot[0].id
    }
    
    String nombreNotario(Long idnot){
        def not = Notario.findById(idnot)
        return not
    }
    
    @Transactional
    String cambiaPass(params, session){
        def usActual = Usuario.get(session.user)
        if(!usActual){
            //comprueba si existe el usuario actual
            return false
        }
        if(params.nueva1.encodeAsSHA1() != params.nueva2.encodeAsSHA1()){
            //comprueba si las contraseñas nuevas son diferentes
            return "La contraseña nueva debe ser igual"
        }
        def pwdActual = params.actual.encodeAsSHA1()
        if(usActual.passwd != pwdActual){
            //Comprueba si la contraseña enviada es igual a la que está guardada
            return "La contraseña enviada no es igual a la guardada"
        } 
        usActual.passwd = params.nueva1.encodeAsSHA1()
        usActual.save(flush:true)
        return "Contraseña modificada correctamente"
        
    }
    
}
