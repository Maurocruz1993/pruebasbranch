package siteo.administracion.catalogos
import siteo.seguridad.Operacion

class Notario {
    
    String notario
    Integer notaria
    Boolean estatus
    
    static constraints = {
        notario (nullable:false, size:1..80, blank:false)
        notaria (nullable:false, min:0, max:999, unique:true)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        notaria.toString()+" - "+notario
    }
}
