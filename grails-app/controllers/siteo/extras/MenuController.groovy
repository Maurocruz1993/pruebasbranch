package siteo.extras

class MenuController {
    def validaSesionService
    def index() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'login', action:'index')
        }
    }
}
