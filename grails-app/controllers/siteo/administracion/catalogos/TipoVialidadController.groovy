package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoVialidadController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond TipoVialidad.list(params), model:[tipoVialidadInstanceCount: TipoVialidad.count()]
    }

    def show(TipoVialidad tipoVialidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoVialidadInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoVialidad(params)
    }

    @Transactional
    def save(TipoVialidad tipoVialidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoVialidadInstance == null) {
            notFound()
            return
        }

        if (tipoVialidadInstance.hasErrors()) {
            respond tipoVialidadInstance.errors, view:'create'
            return
        }

        tipoVialidadInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoVialidad.label', default: 'TipoVialidad'), tipoVialidadInstance.id])
                redirect tipoVialidadInstance
            }
            '*' { respond tipoVialidadInstance, [status: CREATED] }
        }
    }

    def edit(TipoVialidad tipoVialidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoVialidadInstance
    }

    @Transactional
    def update(TipoVialidad tipoVialidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoVialidadInstance == null) {
            notFound()
            return
        }

        if (tipoVialidadInstance.hasErrors()) {
            respond tipoVialidadInstance.errors, view:'edit'
            return
        }

        tipoVialidadInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoVialidad.label', default: 'TipoVialidad'), tipoVialidadInstance.id])
                redirect tipoVialidadInstance
            }
            '*'{ respond tipoVialidadInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoVialidad tipoVialidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoVialidadInstance == null) {
            notFound()
            return
        }

        tipoVialidadInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoVialidad.label', default: 'TipoVialidad'), tipoVialidadInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoVial = TipoVialidad.get(params.idele)
        tipoVial.estatus = !tipoVial.estatus
        tipoVial.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoVialidad.label', default: 'TipoVialidad'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
