package siteo.tramites

import siteo.seguridad.Delegacion
import siteo.seguridad.Municipio
import groovy.sql.Sql
import siteo.administracion.catalogos.Notario

class LoteTramite {
    
    def dataSource;

    Municipio municipio;
    Delegacion delegacion;
    String localidad;
    String nucleoagrario;
    String observaciones;
    byte[] zip;
    Integer numerounico;
    Notario notario;
    Boolean enviado=false;
    Boolean cancelado=false;
    
    static hasMany=[solicitudMasiva:SolicitudMasiva];
    
    static constraints = {
        localidad(nullable:false,size:1..250,blank:false);
        nucleoagrario(nullable:true,size:1..250,blank:true);
        observaciones(nullable:true,size:1..500,blank:true);
        zip(maxSize:1073741824,nullable:true);
        numerounico(nullable:true,min:0);
        notario(nullable:true);
        enviado(nullable:true);
        cancelado(nullable:true);
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(lotetram){
        String sql="SELECT nextval('lotetramite_id') as id";
        Boolean ok = false
        int error=0;
        while (!ok){
            LoteTramite.withTransaction{
                status->
                try{
                    Long id = 0;
                    def consulta = new Sql(dataSource)
                    def retorno= consulta.firstRow(sql)
                    if(retorno){
                        id=retorno.id;
                    }
                    lotetram.id = id
                    lotetram.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                }catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
