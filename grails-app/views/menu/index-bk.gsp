<html>
    <head>
        <meta name="layout" content="main"/>
        <asset:stylesheet src="jmenu.css"/>
        <asset:javascript src="jMenu.jquery.min.js"/>
        <title>SITEO</title>
        <style type="text/css">
            label{
                    float:left;
                    width:65px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function(){
                // simple jMenu plugin called
                //$("#jMenu").jMenu();

                // more complex jMenu plugin called
                $("#jMenu").jMenu({
                  ulWidth : 200,
                  /*effects : {
                        effectSpeedOpen : 300,
                        effectTypeClose : 'slide'
                  },*/
                  animatedText : true
                });
            });
        </script>		
    </head>
    <body>
        <g:form name="frm" controller="master" action="index">
            <input id="vista" name="vista" type="hidden" value=""/>
            <input id="control" name="control" type="hidden" value=""/>
            <input id="pagina" name="pagina" type="hidden" value="menu"/>
            <input id="operacion" name="operacion" type="hidden" value=""/>
        </g:form>
        
        <g:if test="${session.user}">
            <g:set var="menu" value="${session["menu"]}" />
            <g:set var="cats" value="${menu["categorias"]}" />
            <g:set var="subcats" value="${menu["subcategorias"]}" />
            <g:set var="ops" value="${menu["operaciones"]}" />
            <br/>
            <div>
                <ul id="jMenu">
                    <g:each in="${cats}" var="cat">
                        <li><a>${cat.categoria}</a>
                            <ul>
                            <g:each in="${subcats}" var="subc">
                                <g:if test="${subc.padre==cat.id}">
                                    <li><a>${subc.categoria}</a>
                                        <ul>
                                        <g:each in="${ops}" var="op">
                                            <g:if test="${op.categoria==subc}">
                                                <li><g:link controller="${op.controlador}" action="${op.accion}">${op.operacion}</g:link><!--<a href="javascript: envia('{op.controlador}','{op.accion}','','')">{op.operacion}</a>--></li>
                                            </g:if>
                                        </g:each>
                                        </ul>
                                    </li>
                                </g:if>
                            </g:each>
                            <g:each in="${ops}" var="op">
                                <g:if test="${op.categoria.id==cat.id}">
                                    <li><g:link controller="${op.controlador}" action="${op.accion}">${op.operacion}</g:link><!--<a href="javascript: envia('{op.controlador}','{op.accion}','','')">{op.operacion}</a>--></li>
                                </g:if>
                            </g:each>
                            </ul>
                        </li>
                    </g:each>
                    <g:if test="${session.numRol > 1}">
                        <li><g:link controller="usuario" action="seleccionaRol" id="${session.user}">Cambiar Rol</g:link></li>
                    </g:if>
                    <li><g:link controller="usuario" action="passwd">Contraseña</g:link></li>
                  <li><g:link controller="usuario" action="logout">Salir</g:link></li>
                </ul>
            </div>
        </g:if>
        <g:if test="${flash.message}">
            <div class="message">
                    ${flash.message}
            </div>
        </g:if>
        <script>
            function envia(control, accion, pag, oper)
            {
                if (control == null || control == ''){
                    alert('La operación no está definida');
                    return;
                }                    
                vista = document.getElementById('vista');
                vista.value = pag;
                op = document.getElementById('operacion');
                op.value = oper;
                //document.frm.controller = control;
                //document.frm.action = accion;
                document.frm.submit();
            }
        </script>
    </body>
</html>