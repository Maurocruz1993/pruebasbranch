package siteo.seguridad

class Bitacora {

    String actividad
    Date fecha
    Long idafectado
    static constraints = {
        idafectado(nullable:true, blank:true)
    }
    static belongsTo = [usuario:Usuario]
    
    String toString(){
        actividad
    }
    
    static mapping = {
        id generator: 'increment'
    }
}
