package siteo.seguridad



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RolesController {

    def validaSesionService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort = "rol"
        params.order ="asc"
        respond Roles.list(params), model:[rolesInstanceCount: Roles.count()]
    }

    def show(Roles rolesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        respond rolesInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        respond new Roles(params)
    }

    @Transactional
    def save(Roles rolesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        if (rolesInstance == null) {
            notFound()
            return
        }

        if (rolesInstance.hasErrors()) {
            respond rolesInstance.errors, view:'create'
            return
        }

        rolesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'roles.label', default: 'Roles'), rolesInstance.id])
                redirect rolesInstance
            }
            '*' { respond rolesInstance, [status: CREATED] }
        }
    }

    def edit(Roles rolesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        respond rolesInstance
    }

    @Transactional
    def update(Roles rolesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        rolesInstance.operacion.clear()
        def auxInt = new Integer(params.numOperaciones)
        if(auxInt == 1){
            def opss = Operacion.findById(params.operacion)
            
            rolesInstance.addToOperacion(opss)
        }else if(auxInt > 1){
            if(params.operacion!= null){
                def contador = params.operacion.size()
                def arr = []
                for(def i = 0; i < contador ; i++){
                    arr.add(params.operacion[i])
                }
                rolesInstance.operacion = Operacion.findAllByIdInList(arr)
            }
        }
        
        if (rolesInstance == null) {
            notFound()
            return
        }

        if (rolesInstance.hasErrors()) {
            respond rolesInstance.errors, view:'edit'
            return
        }

        rolesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Roles.label', default: 'Roles'), rolesInstance.id])
                redirect rolesInstance
            }
            '*'{ respond rolesInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Roles rolesInstance) {

        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        if (rolesInstance == null) {
            notFound()
            return
        }

        rolesInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Roles.label', default: 'Roles'), rolesInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'roles.label', default: 'Roles'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        def instancia = Roles.get(params.idele)
        instancia.estatus = !instancia.estatus
        instancia.save flush:true
        
        redirect(action: 'index')
    }
}
