package siteo.administracion.catalogos

class NumeroNiveles {

    String nivel
    Boolean estatus
    static constraints = {
        nivel(nullable:false, size:1..40, blank:false)
    }
    static mapping = {
        id generator: 'increment'
    }
    String toString(){
        nivel
    }
}
