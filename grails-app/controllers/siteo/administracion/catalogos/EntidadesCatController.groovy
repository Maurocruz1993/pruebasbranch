package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EntidadesCatController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        respond EntidadesCat.list(params), model:[entidadesCatInstanceCount: EntidadesCat.count()]
    }

    def show(EntidadesCat entidadesCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond entidadesCatInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new EntidadesCat(params)
    }

    @Transactional
    def save(EntidadesCat entidadesCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (entidadesCatInstance == null) {
            notFound()
            return
        }

        if (entidadesCatInstance.hasErrors()) {
            respond entidadesCatInstance.errors, view:'create'
            return
        }

        entidadesCatInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'entidadesCat.label', default: 'EntidadesCat'), entidadesCatInstance.id])
                redirect entidadesCatInstance
            }
            '*' { respond entidadesCatInstance, [status: CREATED] }
        }
    }

    def edit(EntidadesCat entidadesCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond entidadesCatInstance
    }

    @Transactional
    def update(EntidadesCat entidadesCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (entidadesCatInstance == null) {
            notFound()
            return
        }

        if (entidadesCatInstance.hasErrors()) {
            respond entidadesCatInstance.errors, view:'edit'
            return
        }

        entidadesCatInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EntidadesCat.label', default: 'EntidadesCat'), entidadesCatInstance.id])
                redirect entidadesCatInstance
            }
            '*'{ respond entidadesCatInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EntidadesCat entidadesCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (entidadesCatInstance == null) {
            notFound()
            return
        }

        entidadesCatInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EntidadesCat.label', default: 'EntidadesCat'), entidadesCatInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'entidadesCat.label', default: 'EntidadesCat'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
