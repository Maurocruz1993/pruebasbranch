package siteo.administracion.catalogos

class TipoColindancia {

    String tipocolindancia
    Boolean estatus
    
    static constraints = {
        tipocolindancia(nullable: false, size: 1..10, blank: false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipocolindancia
    }
}
