
<%@ page import="siteo.administracion.catalogos.TipoPredio" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoPredio.label', default: 'TipoPredio')}" />
		<title><g:message code="Tipo de predio" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipoPredio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipoPredio" class="content scaffold-show" role="main">
			<h1><g:message code="Tipo de predio" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipoPredio">
			
				<g:if test="${tipoPredioInstance?.tipopredio}">
				<li class="fieldcontain">
					<span id="tipopredio-label" class="property-label"><g:message code="tipoPredio.tipopredio.label" default="Tipo de predio" /></span>
					
						<span class="property-value" aria-labelledby="tipopredio-label"><g:fieldValue bean="${tipoPredioInstance}" field="tipopredio"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipoPredioInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="tipoPredio.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${tipoPredioInstance?.estatus}" true="ACTIVO" false="BAJA" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tipoPredioInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tipoPredioInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
