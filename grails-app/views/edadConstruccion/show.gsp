
<%@ page import="siteo.administracion.catalogos.EdadConstruccion" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'edadConstruccion.label', default: 'EdadConstruccion')}" />
		<title><g:message code="Edad Construcción" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-edadConstruccion" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-edadConstruccion" class="content scaffold-show" role="main">
			<h1><g:message code="Edad construcción" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list edadConstruccion">
			
				<g:if test="${edadConstruccionInstance?.edadConstruccion}">
				<li class="fieldcontain">
					<span id="edadConstruccion-label" class="property-label"><g:message code="edadConstruccion.edadConstruccion.label" default="Edad de la Construccion" /></span>
					
						<span class="property-value" aria-labelledby="edadConstruccion-label"><g:fieldValue bean="${edadConstruccionInstance}" field="edadConstruccion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${edadConstruccionInstance?.factor}">
				<li class="fieldcontain">
					<span id="factor-label" class="property-label"><g:message code="edadConstruccion.factor.label" default="Factor" /></span>
					
						<span class="property-value" aria-labelledby="factor-label"><g:fieldValue bean="${edadConstruccionInstance}" field="factor"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${edadConstruccionInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="edadConstruccion.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${edadConstruccionInstance?.estatus}" true="ACTIVO" false="BAJA"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:edadConstruccionInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${edadConstruccionInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
