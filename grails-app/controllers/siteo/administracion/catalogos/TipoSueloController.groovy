package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoSueloController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond TipoSuelo.list(params), model:[tipoSueloInstanceCount: TipoSuelo.count()]
    }

    def show(TipoSuelo tipoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoSueloInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoSuelo(params)
    }

    @Transactional
    def save(TipoSuelo tipoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoSueloInstance == null) {
            notFound()
            return
        }

        if (tipoSueloInstance.hasErrors()) {
            respond tipoSueloInstance.errors, view:'create'
            return
        }

        tipoSueloInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoSuelo.label', default: 'TipoSuelo'), tipoSueloInstance.id])
                redirect tipoSueloInstance
            }
            '*' { respond tipoSueloInstance, [status: CREATED] }
        }
    }

    def edit(TipoSuelo tipoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoSueloInstance
    }

    @Transactional
    def update(TipoSuelo tipoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoSueloInstance == null) {
            notFound()
            return
        }

        if (tipoSueloInstance.hasErrors()) {
            respond tipoSueloInstance.errors, view:'edit'
            return
        }

        tipoSueloInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoSuelo.label', default: 'TipoSuelo'), tipoSueloInstance.id])
                redirect tipoSueloInstance
            }
            '*'{ respond tipoSueloInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoSuelo tipoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoSueloInstance == null) {
            notFound()
            return
        }

        tipoSueloInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoSuelo.label', default: 'TipoSuelo'), tipoSueloInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoSuelo = TipoSuelo.get(params.idele)
        tipoSuelo.estatus = !tipoSuelo.estatus
        tipoSuelo.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoSuelo.label', default: 'TipoSuelo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
