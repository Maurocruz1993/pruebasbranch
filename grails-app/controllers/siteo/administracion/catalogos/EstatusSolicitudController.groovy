package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EstatusSolicitudController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond EstatusSolicitud.list(params), model:[estatusSolicitudInstanceCount: EstatusSolicitud.count()]
    }

    def show(EstatusSolicitud estatusSolicitudInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond estatusSolicitudInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new EstatusSolicitud(params)
    }

    @Transactional
    def save(EstatusSolicitud estatusSolicitudInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (estatusSolicitudInstance == null) {
            notFound()
            return
        }

        if (estatusSolicitudInstance.hasErrors()) {
            respond estatusSolicitudInstance.errors, view:'create'
            return
        }

        estatusSolicitudInstance.save flush:true
   
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'estatusSolicitud.label', default: 'EstatusSolicitud'), estatusSolicitudInstance.id])
                redirect estatusSolicitudInstance
            }
            '*' { respond estatusSolicitudInstance, [status: CREATED] }
        }
    }

    def edit(EstatusSolicitud estatusSolicitudInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond estatusSolicitudInstance
    }

    @Transactional
    def update(EstatusSolicitud estatusSolicitudInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (estatusSolicitudInstance == null) {
            notFound()
            return
        }

        if (estatusSolicitudInstance.hasErrors()) {
            respond estatusSolicitudInstance.errors, view:'edit'
            return
        }

        estatusSolicitudInstance.save flush:true
     
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EstatusSolicitud.label', default: 'EstatusSolicitud'), estatusSolicitudInstance.id])
                redirect estatusSolicitudInstance
            }
            '*'{ respond estatusSolicitudInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EstatusSolicitud estatusSolicitudInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }

        if (estatusSolicitudInstance == null) {
            notFound()
            return
        }

        estatusSolicitudInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EstatusSolicitud.label', default: 'EstatusSolicitud'), estatusSolicitudInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'estatusSolicitud.label', default: 'EstatusSolicitud'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def mov = EstatusSolicitud.get(params.idele)
        mov.estatus = !mov.estatus
        mov.save flush:true
        
        redirect(action: 'index')
    }
}
