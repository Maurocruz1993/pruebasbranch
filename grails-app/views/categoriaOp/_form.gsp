<%@ page import="siteo.seguridad.CategoriaOp" %>


<!--
<div class="fieldcontain ${hasErrors(bean: categoriaOpInstance, field: 'padre', 'error')} ">
	<label for="padre">
		<g:message code="categoriaOp.padre.label" default="Padre" />
		
	</label>
	<g:field name="padre" type="number" value="${categoriaOpInstance.padre}"/>

</div>-->

<div class="fieldcontain ${hasErrors(bean: categoriaOpInstance, field: 'categoria', 'error')} required">
	<label for="categoria">
		<g:message code="categoriaOp.categoria.label" default="Categoria" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="categoria" required="" value="${categoriaOpInstance?.categoria}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: categoriaOpInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="categoriaOp.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${categoriaOpInstance?.estatus}" />

</div>

<div class="fieldcontain ${hasErrors(bean: categoriaOpInstance, field: 'operaciones', 'error')} "> 
    <label for="categoria">
            <g:message code="categoriaOp.padre.label" default="¿Es Categoria?" />
            <span class="required-indicator">*</span>
    </label>
    <g:checkBox name="sub" value="2" checked="true" />
</div>
<!---
<div class="fieldcontain ${hasErrors(bean: categoriaOpInstance, field: 'operaciones', 'error')} ">
	<label for="operaciones">
		<g:message code="categoriaOp.operaciones.label" default="Operaciones" />
		
	</label>

<ul class="one-to-many">
    
<g:each in="${categoriaOpInstance?.operaciones?}" var="o">
    <li><g:link controller="operacion" action="show" id="${o.id}">${o?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="operacion" action="create" params="['categoriaOp.id': categoriaOpInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'operacion.label', default: 'Operacion')])}</g:link>
</li>
</ul>


</div>
-->
