package siteo.administracion.catalogos

class TipoLocalidad {

    String tipolocalidad
    Boolean estatus
    
    static constraints = {
        tipolocalidad (nullable:false, size:1..100, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipolocalidad
    }
}
