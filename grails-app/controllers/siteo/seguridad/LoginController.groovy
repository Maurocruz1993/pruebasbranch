package siteo.seguridad

class LoginController {

    def validaSesionService
    
    def index() { 
        if (validaSesionService.sesionActiva(session)){
            redirect(controller:'menu', action:'index')
        }
    }
}
