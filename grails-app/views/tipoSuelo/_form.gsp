<%@ page import="siteo.administracion.catalogos.TipoSuelo" %>



<div class="fieldcontain ${hasErrors(bean: tipoSueloInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="tipoSuelo.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${tipoSueloInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoSueloInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoSuelo.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoSueloInstance?.estatus}" />

</div>

