
<%@ page import="siteo.administracion.catalogos.UnidadMedida" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'unidadMedida.label', default: 'UnidadMedida')}" />
		<title><g:message code="Unidad de medida" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-unidadMedida" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-unidadMedida" class="content scaffold-show" role="main">
			<h1><g:message code="Unidad de medida" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list unidadMedida">
			
				<g:if test="${unidadMedidaInstance?.unidad}">
				<li class="fieldcontain">
					<span id="unidad-label" class="property-label"><g:message code="unidadMedida.unidad.label" default="Unidad" /></span>
					
						<span class="property-value" aria-labelledby="unidad-label"><g:fieldValue bean="${unidadMedidaInstance}" field="unidad"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${unidadMedidaInstance?.abreviacion}">
				<li class="fieldcontain">
					<span id="abreviacion-label" class="property-label"><g:message code="unidadMedida.abreviacion.label" default="Abreviacion" /></span>
					
						<span class="property-value" aria-labelledby="abreviacion-label"><g:fieldValue bean="${unidadMedidaInstance}" field="abreviacion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${unidadMedidaInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="unidadMedida.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${unidadMedidaInstance?.estatus}" true="ACTIVO" false="BAJA" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:unidadMedidaInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${unidadMedidaInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
