package siteo.seguridad



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DelegacionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        respond Delegacion.list(params), model:[delegacionInstanceCount: Delegacion.count()]
    }

    def show(Delegacion delegacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond delegacionInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new Delegacion(params)
    }

    @Transactional
    def save(Delegacion delegacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (delegacionInstance == null) {
            notFound()
            return
        }

        if (delegacionInstance.hasErrors()) {
            respond delegacionInstance.errors, view:'create'
            return
        }

        delegacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'delegacion.label', default: 'Delegacion'), delegacionInstance.id])
                redirect delegacionInstance
            }
            '*' { respond delegacionInstance, [status: CREATED] }
        }
    }

    def edit(Delegacion delegacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond delegacionInstance
    }

    @Transactional
    def update(Delegacion delegacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (delegacionInstance == null) {
            notFound()
            return
        }

        if (delegacionInstance.hasErrors()) {
            respond delegacionInstance.errors, view:'edit'
            return
        }

        delegacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Delegacion.label', default: 'Delegacion'), delegacionInstance.id])
                redirect delegacionInstance
            }
            '*'{ respond delegacionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Delegacion delegacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }

        if (delegacionInstance == null) {
            notFound()
            return
        }

        delegacionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Delegacion.label', default: 'Delegacion'), delegacionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'delegacion.label', default: 'Delegacion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
