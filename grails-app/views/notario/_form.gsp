<%@ page import="siteo.administracion.catalogos.Notario" %>
<%@ page import="siteo.seguridad.Operacion" %>


<div class="fieldcontain ${hasErrors(bean: notarioInstance, field: 'notario', 'error')} required">
	<label for="notario">
		<g:message code="notario.notario.label" default="Notario" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="notario" maxlength="80" required="" value="${notarioInstance?.notario}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: notarioInstance, field: 'notaria', 'error')} required">
	<label for="notaria">
		<g:message code="notario.notaria.label" default="Notaria" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="notaria" type="number" min="0" max="999" value="${notarioInstance.notaria}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: notarioInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="notario.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${notarioInstance?.estatus}" />

</div>

