package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UbicacionManzanaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond UbicacionManzana.list(params), model:[ubicacionManzanaInstanceCount: UbicacionManzana.count()]
    }

    def show(UbicacionManzana ubicacionManzanaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond ubicacionManzanaInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new UbicacionManzana(params)
    }

    @Transactional
    def save(UbicacionManzana ubicacionManzanaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (ubicacionManzanaInstance == null) {
            notFound()
            return
        }

        if (ubicacionManzanaInstance.hasErrors()) {
            respond ubicacionManzanaInstance.errors, view:'create'
            return
        }

        ubicacionManzanaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'ubicacionManzana.label', default: 'UbicacionManzana'), ubicacionManzanaInstance.id])
                redirect ubicacionManzanaInstance
            }
            '*' { respond ubicacionManzanaInstance, [status: CREATED] }
        }
    }

    def edit(UbicacionManzana ubicacionManzanaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond ubicacionManzanaInstance
    }

    @Transactional
    def update(UbicacionManzana ubicacionManzanaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (ubicacionManzanaInstance == null) {
            notFound()
            return
        }

        if (ubicacionManzanaInstance.hasErrors()) {
            respond ubicacionManzanaInstance.errors, view:'edit'
            return
        }

        ubicacionManzanaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UbicacionManzana.label', default: 'UbicacionManzana'), ubicacionManzanaInstance.id])
                redirect ubicacionManzanaInstance
            }
            '*'{ respond ubicacionManzanaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UbicacionManzana ubicacionManzanaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (ubicacionManzanaInstance == null) {
            notFound()
            return
        }

        ubicacionManzanaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UbicacionManzana.label', default: 'UbicacionManzana'), ubicacionManzanaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def ubMnza = UbicacionManzana.get(params.idele)
        ubMnza.estatus = !ubMnza.estatus
        ubMnza.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'ubicacionManzana.label', default: 'UbicacionManzana'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
