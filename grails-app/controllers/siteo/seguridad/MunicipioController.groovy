package siteo.seguridad



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MunicipioController {

    def validaSesionService;
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        params.max = Math.min(max ?: 10, 100)
        respond Municipio.list(params), model:[municipioInstanceCount: Municipio.count()]
    }

    def show(Municipio municipioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        respond municipioInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        respond new Municipio(params)
    }

    @Transactional
    def save(Municipio municipioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        if (municipioInstance == null) {
            notFound()
            return
        }

        if (municipioInstance.hasErrors()) {
            respond municipioInstance.errors, view:'create'
            return
        }

        municipioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'municipio.label', default: 'Municipio'), municipioInstance.id])
                redirect municipioInstance
            }
            '*' { respond municipioInstance, [status: CREATED] }
        }
    }

    def edit(Municipio municipioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        respond municipioInstance
    }

    @Transactional
    def update(Municipio municipioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        if (municipioInstance == null) {
            notFound()
            return
        }

        if (municipioInstance.hasErrors()) {
            respond municipioInstance.errors, view:'edit'
            return
        }

        municipioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Municipio.label', default: 'Municipio'), municipioInstance.id])
                redirect municipioInstance
            }
            '*'{ respond municipioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Municipio municipioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        if (municipioInstance == null) {
            notFound()
            return
        }

        municipioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Municipio.label', default: 'Municipio'), municipioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'municipio.label', default: 'Municipio'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
