package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoConstruccionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort="id"
        params.order="asc"
        respond TipoConstruccion.list(params), model:[tipoConstruccionInstanceCount: TipoConstruccion.count()]
    }

    def show(TipoConstruccion tipoConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoConstruccionInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoConstruccion(params)
    }

    @Transactional
    def save(TipoConstruccion tipoConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoConstruccionInstance == null) {
            notFound()
            return
        }

        if (tipoConstruccionInstance.hasErrors()) {
            respond tipoConstruccionInstance.errors, view:'create'
            return
        }

        tipoConstruccionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoConstruccion.label', default: 'TipoConstruccion'), tipoConstruccionInstance.id])
                redirect tipoConstruccionInstance
            }
            '*' { respond tipoConstruccionInstance, [status: CREATED] }
        }
    }

    def edit(TipoConstruccion tipoConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoConstruccionInstance
    }

    @Transactional
    def update(TipoConstruccion tipoConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoConstruccionInstance == null) {
            notFound()
            return
        }

        if (tipoConstruccionInstance.hasErrors()) {
            respond tipoConstruccionInstance.errors, view:'edit'
            return
        }

        tipoConstruccionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoConstruccion.label', default: 'TipoConstruccion'), tipoConstruccionInstance.id])
                redirect tipoConstruccionInstance
            }
            '*'{ respond tipoConstruccionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoConstruccion tipoConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoConstruccionInstance == null) {
            notFound()
            return
        }

        tipoConstruccionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoConstruccion.label', default: 'TipoConstruccion'), tipoConstruccionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoConst = TipoConstruccion.get(params.idele)
        tipoConst.estatus = !tipoConst.estatus
        tipoConst.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoConstruccion.label', default: 'TipoConstruccion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
