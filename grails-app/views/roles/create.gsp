<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'roles.label', default: 'Roles')}" />
		<title>Nuevo</title>
	</head>
	<body>
		<a href="#create-roles" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">Principal</a></li>
				<li><g:link class="list" action="index">Lista</g:link></li>
			</ul>
		</div>
		<div id="create-roles" class="content scaffold-create" role="main">
			<h1>Nuevo Rol</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${rolesInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${rolesInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:rolesInstance, action:'save']" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="Crear" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
