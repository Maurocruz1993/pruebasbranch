<%@ page import="siteo.administracion.catalogos.UbicacionManzana" %>



<div class="fieldcontain ${hasErrors(bean: ubicacionManzanaInstance, field: 'ubicacionManzana', 'error')} required">
	<label for="ubicacionManzana">
		<g:message code="ubicacionManzana.ubicacionManzana.label" default="Ubicacion de manzana" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="ubicacionManzana" maxlength="20" required="" value="${ubicacionManzanaInstance?.ubicacionManzana}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: ubicacionManzanaInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="ubicacionManzana.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${ubicacionManzanaInstance?.estatus}" />

</div>

