package siteo.tramites



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import siteo.seguridad.Delegacion
import siteo.seguridad.Municipio
import java.io.InputStream
import siteo.administracion.catalogos.Notario
import grails.converters.JSON
import utilerias.UtilJasper
import grails.converters.*
import jxl.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.poi.ss.usermodel.WorkbookFactory

@Transactional(readOnly = true)
class SolicitudMasivaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    
    def validaSesionService    
    def solicitudMasivaService 

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
    }
   
    // 
    // estas son las pruebas de cambios hechos en la rama 1 y ver el comportamiento para poder hacer el merge
    // estas son las pruebas de cambios hechos en la rama 1 y ver el comportamiento para poder hacer el merge
    // estas son las pruebas de cambios hechos en la rama 1 y ver el comportamiento para poder hacer el merge
    // estas son las pruebas de cambios hechos en la rama 1 y ver el comportamiento para poder hacer el merge
    def buscadelegacion(int id) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def municipio = Municipio.findById(id) 
        def delegacion
        def iddel
        if(municipio!=null){
            delegacion =Delegacion.get(municipio.delegaciones.id)
            
        }else{
            System.out.println("No entro al if")
        }
        render delegacion as JSON
    }
    
    // la rama en prueba 2 para ver el comportamiento a la hora de unir los branch
    // la rama en prueba 2 para ver el comportamiento a la hora de unir los branch
    // la rama en prueba 2 para ver el comportamiento a la hora de unir los branch
    // la rama en prueba 2 para ver el comportamiento a la hora de unir los branch
    
    @Transactional
    def guardamasivo(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        int numlote=0;
        long idlote=0;
        def auxnot=Notario.get((long)session.auxidnot);
        def cantidad=LoteTramite.createCriteria().list{
            eq("notario",auxnot)
            projections {
                max("numerounico")
            }
        };
        if(cantidad[0]){
            numlote=cantidad[0]+1;
        }else{
            numlote=1;
        }
        def municipio=Municipio.get(params.municipio);
        def delegacion=Delegacion.get(params.delegacion);
        String[] ruta="";
        String mensaje="",dirzip="",dir="";
        //----def lotesol=LoteTramite.get(1);
        LoteTramite lotesol=new LoteTramite();
        lotesol.delegacion=delegacion;
        lotesol.municipio=municipio;
        lotesol.localidad=params.localidad;
        lotesol.nucleoagrario=params.nucleoagrario;
        lotesol.observaciones=params.observaciones;
        //---declaración archivo zip
        def zip = request.getFile('zip')//---obtenemos el archivo
        if(!zip.getOriginalFilename().contains(".zip")){//---validamos que sea un archivo zip
            mensaje="Solo archivos zip";
            redirect(action:"index",params:[mensaje:mensaje]);
            return;
        }
        
        String webRootDir = servletContext.getRealPath("/");
        if(webRootDir[0].equals("/")){//---linux
            ruta=webRootDir.split("/");
            for(int c=0;c<(ruta.length-3);c++){
                dir+=ruta[c]+"/";
            }
            dirzip="shape/";
        }else{//---windows
            ruta=webRootDir.split("\\'");
            for(int c=0;c<(ruta.length-3);c++){
                dir+=ruta[c]+"\\";
            }
            dir = "c:\\"
            dirzip="shape\\";
        }
        File directorio=new File(dir+dirzip);
        File convFile = new File(directorio,""+zip.getOriginalFilename());
        FileOutputStream auxfile = new FileOutputStream(convFile); 
        auxfile.write(zip.getBytes());
        auxfile.close();
        InputStream is = new FileInputStream(convFile);
        byte[] buffer = new byte[(int) convFile.length()];
        int readers = is.read(buffer);
        
        lotesol.zip=buffer;
        lotesol.numerounico=numlote;
        lotesol.notario=auxnot;
        idlote=lotesol.guarda(lotesol)
        
        def xls = request.getFile('excel')//---obtenemos el archivo   
        InputStream inp=xls.getInputStream(); 
        
        if (xls.getOriginalFilename().endsWith(".xls")|| xls.getOriginalFilename().endsWith(".XLS") ) {                      
            System.out.println("es un archivo .xls")
        } else 
        if (  xls.getOriginalFilename().endsWith(".xlsx") || xls.getOriginalFilename().endsWith(".XLSX")) {
            System.out.println("es un archivo .xlsx")   
            XSSFWorkbook wb = new XSSFWorkbook(inp)
            solicitudMasivaService.guardaExcel(wb,lotesol,(int)session.user);
        }else{
            mensaje="Solo archivos xls";
            redirect(action:"index",params:[mensaje:mensaje]);
            return;
        } 
        
        redirect(controller:"SolicitudMasiva",action:"datoslote",params:[id:idlote]);
    }
    
    def datoslote(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        def lote=LoteTramite.get(params.id);
        def solicitudes=SolicitudMasiva.findAllByLotetramite(lote);
        flash.message="Resultado de la carga, datos del lote "+lote.numerounico;
        render(view:"datoslote",model:[solicitudes:solicitudes,lote:lote]);
    }
    
    def descargarZip(){
        def lotesol=LoteTramite.get(1);
        response.setContentType("APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition", "Attachment;Filename=\"shape\"");
        def outputStream = response.getOutputStream();
        outputStream << lotesol.zip;
        outputStream.flush();
        outputStream.close();
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'solicitudMasiva.label', default: 'SolicitudMasiva'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
