package siteo.administracion.catalogos

class TipoPersona {

    String tipopersona
    Boolean estatus
    
    static constraints = {
        tipopersona (nullable: false, size:1..10, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipopersona
    }
}
