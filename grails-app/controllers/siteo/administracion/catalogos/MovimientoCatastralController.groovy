package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MovimientoCatastralController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort="clave"
        params.order="asc"
        respond MovimientoCatastral.list(params), model:[movimientoCatastralInstanceCount: MovimientoCatastral.count()]
    }

    def show(MovimientoCatastral movimientoCatastralInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond movimientoCatastralInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new MovimientoCatastral(params)
    }

    @Transactional
    def save(MovimientoCatastral movimientoCatastralInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (movimientoCatastralInstance == null) {
            notFound()
            return
        }

        if (movimientoCatastralInstance.hasErrors()) {
            respond movimientoCatastralInstance.errors, view:'create'
            return
        }

        movimientoCatastralInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'movimientoCatastral.label', default: 'MovimientoCatastral'), movimientoCatastralInstance.id])
                redirect movimientoCatastralInstance
            }
            '*' { respond movimientoCatastralInstance, [status: CREATED] }
        }
    }

    def edit(MovimientoCatastral movimientoCatastralInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond movimientoCatastralInstance
    }

    @Transactional
    def update(MovimientoCatastral movimientoCatastralInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (movimientoCatastralInstance == null) {
            notFound()
            return
        }

        if (movimientoCatastralInstance.hasErrors()) {
            respond movimientoCatastralInstance.errors, view:'edit'
            return
        }

        movimientoCatastralInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'MovimientoCatastral.label', default: 'MovimientoCatastral'), movimientoCatastralInstance.id])
                redirect movimientoCatastralInstance
            }
            '*'{ respond movimientoCatastralInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(MovimientoCatastral movimientoCatastralInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (movimientoCatastralInstance == null) {
            notFound()
            return
        }

        movimientoCatastralInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'MovimientoCatastral.label', default: 'MovimientoCatastral'), movimientoCatastralInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def mov = MovimientoCatastral.get(params.idele)
        mov.estatus = !mov.estatus
        mov.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'movimientoCatastral.label', default: 'MovimientoCatastral'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
