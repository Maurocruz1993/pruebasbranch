package siteo.seguridad

class CategoriaOp {

    String categoria
    Long padre
    Boolean estatus
    static hasMany = [operaciones:Operacion]
    static constraints = {
        padre(nullable:true, blank:true)
        categoria(unique:true)
    }
    String toString(){
        categoria
    }
    static mapping = {
        id generator: 'increment'
    }
}
