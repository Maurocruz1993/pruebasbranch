package siteo.administracion.catalogos

class TipoSuelo {

    String nombre
    Boolean estatus = Boolean.FALSE
    
    static constraints = {
        nombre(nullable:false, blank:false)
    }
    static mapping = {
        id generator: 'increment'
    }
    String toString(){
        return nombre
    }
}
