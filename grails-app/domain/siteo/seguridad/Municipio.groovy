package siteo.seguridad

class Municipio {

    String nombre
    Integer clave
    static belongsTo = [delegaciones:Delegacion]
    
    static constraints = {
        nombre(blank:true, nullable:false,size:1..150)
        clave(nullable:false, min:0, max:9999)
    }
    
    String toString (){
        delegaciones.nombre +  "-" + nombre
    }
    
    String nombreconclave(){
        clave.toString().padLeft(3,'0')+" - "+nombre
    }    
}
