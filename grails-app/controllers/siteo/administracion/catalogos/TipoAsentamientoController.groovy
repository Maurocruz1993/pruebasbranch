package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoAsentamientoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort="id"
        params.order="asc"
        respond TipoAsentamiento.list(params), model:[tipoAsentamientoInstanceCount: TipoAsentamiento.count()]
    }

    def show(TipoAsentamiento tipoAsentamientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoAsentamientoInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoAsentamiento(params)
    }

    @Transactional
    def save(TipoAsentamiento tipoAsentamientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoAsentamientoInstance == null) {
            notFound()
            return
        }

        if (tipoAsentamientoInstance.hasErrors()) {
            respond tipoAsentamientoInstance.errors, view:'create'
            return
        }

        tipoAsentamientoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoAsentamiento.label', default: 'TipoAsentamiento'), tipoAsentamientoInstance.id])
                redirect tipoAsentamientoInstance
            }
            '*' { respond tipoAsentamientoInstance, [status: CREATED] }
        }
    }

    def edit(TipoAsentamiento tipoAsentamientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoAsentamientoInstance
    }

    @Transactional
    def update(TipoAsentamiento tipoAsentamientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoAsentamientoInstance == null) {
            notFound()
            return
        }

        if (tipoAsentamientoInstance.hasErrors()) {
            respond tipoAsentamientoInstance.errors, view:'edit'
            return
        }

        tipoAsentamientoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoAsentamiento.label', default: 'TipoAsentamiento'), tipoAsentamientoInstance.id])
                redirect tipoAsentamientoInstance
            }
            '*'{ respond tipoAsentamientoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoAsentamiento tipoAsentamientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoAsentamientoInstance == null) {
            notFound()
            return
        }

        tipoAsentamientoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoAsentamiento.label', default: 'TipoAsentamiento'), tipoAsentamientoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoAs = TipoAsentamiento.get(params.idele)
        tipoAs.estatus = !tipoAs.estatus
        tipoAs.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoAsentamiento.label', default: 'TipoAsentamiento'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
