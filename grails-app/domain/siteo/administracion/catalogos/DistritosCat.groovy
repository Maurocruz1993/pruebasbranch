package siteo.administracion.catalogos

class DistritosCat {

    String distrito
    String clave
    
    static constraints = {
        distrito(nullable:false, size:1..45, blank:true)
        clave(nullable:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        clave.toString().padLeft(2,'0')+" - "+distrito
    }  
    
}
