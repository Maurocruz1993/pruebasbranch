package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class NumeroNivelesController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort="id"
        params.order="asc"
        respond NumeroNiveles.list(params), model:[numeroNivelesInstanceCount: NumeroNiveles.count()]
    }

    def show(NumeroNiveles numeroNivelesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond numeroNivelesInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new NumeroNiveles(params)
    }

    @Transactional
    def save(NumeroNiveles numeroNivelesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (numeroNivelesInstance == null) {
            notFound()
            return
        }

        if (numeroNivelesInstance.hasErrors()) {
            respond numeroNivelesInstance.errors, view:'create'
            return
        }

        numeroNivelesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'numeroNiveles.label', default: 'NumeroNiveles'), numeroNivelesInstance.id])
                redirect numeroNivelesInstance
            }
            '*' { respond numeroNivelesInstance, [status: CREATED] }
        }
    }

    def edit(NumeroNiveles numeroNivelesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond numeroNivelesInstance
    }

    @Transactional
    def update(NumeroNiveles numeroNivelesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (numeroNivelesInstance == null) {
            notFound()
            return
        }

        if (numeroNivelesInstance.hasErrors()) {
            respond numeroNivelesInstance.errors, view:'edit'
            return
        }

        numeroNivelesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'NumeroNiveles.label', default: 'NumeroNiveles'), numeroNivelesInstance.id])
                redirect numeroNivelesInstance
            }
            '*'{ respond numeroNivelesInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(NumeroNiveles numeroNivelesInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (numeroNivelesInstance == null) {
            notFound()
            return
        }

        numeroNivelesInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'NumeroNiveles.label', default: 'NumeroNiveles'), numeroNivelesInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def nv = NumeroNiveles.get(params.idele)
        nv.estatus = !nv.estatus
        nv.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'numeroNiveles.label', default: 'NumeroNiveles'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
