package siteo.administracion.catalogos

class TipoAsentamiento {

    String tipoasentamiento
    Boolean estatus

    static constraints = {
        tipoasentamiento (nullable: false, size:1..25, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipoasentamiento
    }
}
