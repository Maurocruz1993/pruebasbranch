
<%@ page import="siteo.seguridad.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
		<title>Modificar contraseña</title>
	</head>
	<body>
		<a href="#list-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">Principal</a></li>
			</ul>
		</div>
		<br/>
                <g:if test="${flash.message}">
                    <div class="message">
                            ${flash.message}
                    </div>
                </g:if>
                <h1>Modifcar Contraseña</h1>
                <g:form action="modificapwd">
                    <label for="actual">
                        <g:message code="usuario.passwd.label" default="Contraseña Actual" />
                        <span class="required-indicator">*</span>
                    </label>
                    <g:field type="password" name="actual" required=""/>
                    <br/>
                    <br/>
                    
                    <label for="nueva1">
                        <g:message code="usuario.passwd.label" default="Contraseña nueva" />
                        <span class="required-indicator">*</span>
                    </label>
                    <g:field type="password" name="nueva1" required=""/>
                    <br/><br/>
                    
                    <label for="nueva2">
                        <g:message code="usuario.passwd.label" default="Repita Contraseña" />
                        <span class="required-indicator">*</span>
                    </label>
                    <g:field type="password" name="nueva2" required=""/>
                    <br/><br/>
                    <fieldset class="buttons">
                        <g:submitButton class="save" name="enviar" action="modificapwd" value="Modificar"/>
                    </fieldset>
                    
                </g:form>
	</body>
</html>
