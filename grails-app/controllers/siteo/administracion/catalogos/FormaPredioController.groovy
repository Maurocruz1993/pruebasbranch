package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FormaPredioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort = "id"
        params.order ="asc"
        respond FormaPredio.list(params), model:[formaPredioInstanceCount: FormaPredio.count()]
    }

    def show(FormaPredio formaPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond formaPredioInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new FormaPredio(params)
    }

    @Transactional
    def save(FormaPredio formaPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (formaPredioInstance == null) {
            notFound()
            return
        }

        if (formaPredioInstance.hasErrors()) {
            respond formaPredioInstance.errors, view:'create'
            return
        }

        formaPredioInstance.save flush:true
      
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'formaPredio.label', default: 'FormaPredio'), formaPredioInstance.id])
                redirect formaPredioInstance
            }
            '*' { respond formaPredioInstance, [status: CREATED] }
        }
    }

    def edit(FormaPredio formaPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond formaPredioInstance
    }

    @Transactional
    def update(FormaPredio formaPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (formaPredioInstance == null) {
            notFound()
            return
        }

        if (formaPredioInstance.hasErrors()) {
            respond formaPredioInstance.errors, view:'edit'
            return
        }

        formaPredioInstance.save flush:true
        
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'FormaPredio.label', default: 'FormaPredio'), formaPredioInstance.id])
                redirect formaPredioInstance
            }
            '*'{ respond formaPredioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(FormaPredio formaPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (formaPredioInstance == null) {
            notFound()
            return
        }

        formaPredioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'FormaPredio.label', default: 'FormaPredio'), formaPredioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def fp = FormaPredio.get(params.idele)
        fp.estatus = !fp.estatus
        fp.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'formaPredio.label', default: 'FormaPredio'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
