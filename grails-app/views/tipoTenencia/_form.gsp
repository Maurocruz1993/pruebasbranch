<%@ page import="siteo.administracion.catalogos.TipoTenencia" %>



<div class="fieldcontain ${hasErrors(bean: tipoTenenciaInstance, field: 'tipotenencia', 'error')} required">
	<label for="tipotenencia">
		<g:message code="tipoTenencia.tipotenencia.label" default="Tipo de tenencia" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipotenencia" maxlength="7" required="" value="${tipoTenenciaInstance?.tipotenencia}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoTenenciaInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoTenencia.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoTenenciaInstance?.estatus}" />

</div>

