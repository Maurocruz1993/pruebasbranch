package siteo.administracion.catalogos

class FormaPredio {

    String formaPredio
    Boolean estatus
    
    static constraints = {
        formaPredio(nullable:false, size:1..20, blank:false)
    }
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        formaPredio
    }
}
