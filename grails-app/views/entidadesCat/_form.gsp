<%@ page import="siteo.administracion.catalogos.EntidadesCat" %>



<div class="fieldcontain ${hasErrors(bean: entidadesCatInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="entidadesCat.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" maxlength="45" required="" value="${entidadesCatInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: entidadesCatInstance, field: 'clave', 'error')} required">
	<label for="clave">
		<g:message code="entidadesCat.clave.label" default="Clave" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="clave" type="number" value="${entidadesCatInstance.clave}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: entidadesCatInstance, field: 'abrev', 'error')} ">
	<label for="abrev">
		<g:message code="entidadesCat.abrev.label" default="Abrev" />
		
	</label>
	<g:textField name="abrev" maxlength="5" value="${entidadesCatInstance?.abrev}"/>

</div>

