<%@ page import="siteo.administracion.catalogos.DistritosCat" %>



<div class="fieldcontain ${hasErrors(bean: distritosCatInstance, field: 'distrito', 'error')} required">
	<label for="distrito">
		<g:message code="distritosCat.distrito.label" default="Distrito" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="distrito" maxlength="45" required="" value="${distritosCatInstance?.distrito}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: distritosCatInstance, field: 'clave', 'error')} required">
	<label for="clave">
		<g:message code="distritosCat.clave.label" default="Clave" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="clave" required="" value="${distritosCatInstance?.clave}"/>

</div>

