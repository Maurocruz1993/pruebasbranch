<%@ page import="siteo.administracion.catalogos.TipoPredio" %>



<div class="fieldcontain ${hasErrors(bean: tipoPredioInstance, field: 'tipopredio', 'error')} required">
	<label for="tipopredio">
		<g:message code="tipoPredio.tipopredio.label" default="Tipo de predio" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipopredio" maxlength="10" required="" value="${tipoPredioInstance?.tipopredio}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoPredioInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoPredio.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoPredioInstance?.estatus}" />

</div>

