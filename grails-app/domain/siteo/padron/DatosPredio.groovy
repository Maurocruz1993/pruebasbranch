package siteo.padron

import siteo.administracion.catalogos.DistritosCat
import siteo.administracion.catalogos.TipoAsentamiento
import siteo.administracion.catalogos.TipoLocalidad
import siteo.administracion.catalogos.TipoVialidad

class DatosPredio {
    
    TipoVialidad tipovialidad
    String otrotipovialidad
    String vialidad
    String numext
    String numint
    TipoAsentamiento tipoasentamiento
    String otrotipoasentamiento
    String asentamiento
    TipoLocalidad tipolocalidad
    String otrotipolocalidad
    String descripcion
    String lote
    String zona
    String codigopostal
    String cond_cve
    String descripcionEdificio
    String manzana
    String distrito;
    DistritosCat distritocat
    
    static belongsTo = [datosdeclarados: DatosDeclarados]

    static constraints = {
        vialidad (nullable:true, size:1..255, blank:true)
        otrotipovialidad(nullable:true,blank:true,size:1..100)
        numext (nullable:true, size:1..10, blank:true)
        numint (nullable:true, size:1..6, blank:true)
        asentamiento (nullable:true, size:1..255, blank:true)
        otrotipoasentamiento(nullable:true,blank:true,size:1..100)
        otrotipolocalidad(nullable:true,blank:true,size:1..100)
        descripcion(nullable:true, size:1..255, blank:true)
        descripcionEdificio(nullable:true, size:1..255, blank:true)
        codigopostal(nullable:true, size:1..6, blank:true)
        cond_cve(nullable:true, size:1..10, blank:true)
        lote(nullable:true, size:1..45, blank:true)
        manzana(nullable:true, size:1..45, blank:true)
        distrito(nullable:true,size:1..100, blank:true)
        distritocat(nullable:true)
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(dp){
        Boolean ok = false
        int error=0;
        while (!ok){
            DatosPredio.withTransaction{
                status->
                try{
                    Long id = DatosPredio.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    dp.id = id
                    dp.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
