package siteo.seguridad
import siteo.administracion.catalogos.Notario

class Usuario {
    String titulo
    String nombre
    String usuario
    String ubicacion
    String passwd
    Boolean estatus
    Boolean firma
    
    static hasMany = [notarias:Notario, roles:Roles]

    static constraints = {
        ubicacion(nullable:true, blank:true)
        usuario(unique:true, minSize:6)
        titulo(blank:true, nullable:true)
        firma(nullable:true)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        String usuario = nombre
        if (titulo)
            usuario = titulo+" "+nombre
        usuario
    }
}
