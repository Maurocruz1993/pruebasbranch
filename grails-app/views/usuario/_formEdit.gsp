<%@ page import="siteo.seguridad.Usuario" %>



<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'titulo', 'error')} ">
	<label for="titulo">
		<g:message code="usuario.titulo.label" default="Titulo" />
		
	</label>
	<g:textField name="titulo" value="${usuarioInstance?.titulo}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="usuario.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" required="" value="${usuarioInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'usuario', 'error')} required">
	<label for="usuario">
		<g:message code="usuario.usuario.label" default="Usuario" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="usuario" required="" value="${usuarioInstance?.usuario}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'ubicacion', 'error')} ">
	<label for="ubicacion">
		<g:message code="usuario.ubicacion.label" default="Ubicacion" />
		
	</label>
	<g:textField name="ubicacion" value="${usuarioInstance?.ubicacion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="usuario.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${usuarioInstance?.estatus}" />

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'roles', 'error')} ">
	<label for="roles">
		<g:message code="usuario.roles.label" default="Roles" />
		
	</label>
	<g:select name="roles" from="${siteo.seguridad.Roles.list(sort: "rol", order: "asc")}" multiple="multiple" optionKey="id" size="5" value="${usuarioInstance?.roles*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'notarias', 'error')} ">
	<label for="notarias">
		<g:message code="usuario.notarias.label" default="Notarias" />
		
	</label>
	<g:select name="notarias" from="${siteo.administracion.catalogos.Notario.list(sort: "notaria", order: "asc")}" multiple="multiple" optionKey="id" size="5" value="${usuarioInstance?.notarias*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: usuarioInstance, field: 'firma', 'error')} ">
	<label for="firma">
		<g:message code="usuario.firma.label" default="Firma" />
		
	</label>
	<g:checkBox name="firma" value="${usuarioInstance?.firma}" />
</div>