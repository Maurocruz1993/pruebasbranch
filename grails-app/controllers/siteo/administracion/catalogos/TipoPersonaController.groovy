package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoPersonaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort="id"
        params.order="asc"
        respond TipoPersona.list(params), model:[tipoPersonaInstanceCount: TipoPersona.count()]
    }

    def show(TipoPersona tipoPersonaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoPersonaInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoPersona(params)
    }

    @Transactional
    def save(TipoPersona tipoPersonaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPersonaInstance == null) {
            notFound()
            return
        }

        if (tipoPersonaInstance.hasErrors()) {
            respond tipoPersonaInstance.errors, view:'create'
            return
        }

        tipoPersonaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoPersona.label', default: 'TipoPersona'), tipoPersonaInstance.id])
                redirect tipoPersonaInstance
            }
            '*' { respond tipoPersonaInstance, [status: CREATED] }
        }
    }

    def edit(TipoPersona tipoPersonaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoPersonaInstance
    }

    @Transactional
    def update(TipoPersona tipoPersonaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPersonaInstance == null) {
            notFound()
            return
        }

        if (tipoPersonaInstance.hasErrors()) {
            respond tipoPersonaInstance.errors, view:'edit'
            return
        }

        tipoPersonaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoPersona.label', default: 'TipoPersona'), tipoPersonaInstance.id])
                redirect tipoPersonaInstance
            }
            '*'{ respond tipoPersonaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoPersona tipoPersonaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPersonaInstance == null) {
            notFound()
            return
        }

        tipoPersonaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoPersona.label', default: 'TipoPersona'), tipoPersonaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoPer = TipoPersona.get(params.idele)
        tipoPer.estatus = !tipoPer.estatus
        tipoPer.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoPersona.label', default: 'TipoPersona'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
