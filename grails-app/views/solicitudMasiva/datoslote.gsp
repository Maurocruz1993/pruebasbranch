<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        <meta name="layout" content="main"/> 
        <title>Lote</title>
        <asset:stylesheet src="jquery.dataTables.css"/>
        <asset:javascript src="jquery.dataTables.min.js"/>
        <script type="text/javascript" language="javascript">
            $.extend( true, $.fn.dataTable.defaults, {
                "searching": false
            } );
            $(document).ready(function () {
                //---window.history.pushState(null, "Lote", '${request.contextPath}/solicitudMasiva/datoslote');
                depliegamensaje();
                var tablasol=$("#tbl-tramites").DataTable( {
                    "language": {
                           "pagingType": "full_numbers",
                           "search": "Buscar:",
                           "lengthChange": false,
                           "lengthMenu": "Mostrando _MENU_ registros por página",
                           "zeroRecords": "Nada encontrado ",
                           "info": "Mostrando página _PAGE_ de _PAGES_",
                           "infoEmpty": "No hay registros",
                           "infoFiltered": "(filtered from _MAX_ total records)",
                           "paginate": {
                               "first":      "Inicio",
                               "last":       "Fin",
                               "next":       "Siguiente",
                               "previous":   "Anterior"
                           }
                       },"order": [[ 1, "desc" ]]
                });
            });
            
            $(function() {
                $( "#tabstram" ).tabs();
            });
        </script>
    </head>
    <body>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            </ul>
        </div>
        <div id="list-lotesol" class="content scaffold-list" role="main">
            <g:if test="${flash.message}">
                <div id="mensajes" style="display: none;" class="exito">${flash.message}</div>
            </g:if>
            <g:hiddenField id="txtidlote" name="txtidlote" value="${lote?.id}" />
            <table>
                <tbody>
                    <tr>
                        <th><g:message code="Municipio"/></th>
                        <th><g:message code="Delegación"/></th>
                        <th><g:message code="Localidad"/></th>
                    </tr>
                    <tr>
                        <td>${lote?.municipio?.nombreconclave()}</td>
                        <td>${lote?.delegacion?.toString()}</td>
                        <td>${lote?.localidad}</td>
                    </tr>
                    <tr>
                        <th><g:message code="Núcleo agrario"/></th>
                        <th><g:message code="Observaciones"/></th>
                    </tr>
                    <tr>
                        <td>${lote?.nucleoagrario}</td>
                        <td>${lote?.observaciones}</td>
                    </tr>
                </tbody>
            </table>
            <div id="tabstram">
                <ul>
                    <li><a href="#validos">Tramites válidos</a></li>
                    <li><a href="#correccion">Requieren corrección</a></li>
                </ul>
                <div id="validos">
                    <table id="tbl-tramites">
                        <thead>
                            <tr>
                                <th><g:message code="Num. Folio" /></th>
                                <th><g:message code="Nombre" /></th>
                                <th><g:message code="Lote" /></th>
                                <th><g:message code="Zona" /></th>
                                <th><g:message code="Superficie" /></th>
                                <th><g:message code="Enviado" /></th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${solicitudes}" status="i" var="solicitud">
                                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                    <td>${solicitud?.foliounico}</td>
                                    <td>${solicitud?.nombrepropietario}</td>
                                    <td>${solicitud?.lote}</td>
                                    <td>${solicitud?.zona}</td>
                                    <td>${solicitud?.superficiepred}</td>
                                    <td></td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
                </div>
                <div id="correccion">
                    <span>corrección</span>
                </div>
            </div>
        </div>
    </body>
</html>
