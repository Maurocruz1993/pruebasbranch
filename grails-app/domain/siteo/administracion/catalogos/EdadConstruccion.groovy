package siteo.administracion.catalogos

class EdadConstruccion {
    String edadConstruccion
    BigDecimal factor
    Boolean estatus
    static constraints = {
        edadConstruccion(nullable:false, size:1..20, blank:false)
        factor(nullable:true)
    }
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        edadConstruccion
    }
}
