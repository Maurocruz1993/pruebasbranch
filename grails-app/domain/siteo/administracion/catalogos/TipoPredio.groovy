package siteo.administracion.catalogos

class TipoPredio {

    String tipopredio
    Boolean estatus
    
    static constraints = {
        tipopredio (nullable:false, size: 1..10, blank: false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipopredio
    }
}
