package siteo.padron

import siteo.administracion.catalogos.EntidadesCat
import siteo.administracion.catalogos.TipoAsentamiento
import siteo.administracion.catalogos.TipoLocalidad
import siteo.administracion.catalogos.TipoVialidad
import siteo.seguridad.Municipio

class DomicilioFiscal {

    TipoVialidad tipovialidad
    String otrotipovialidad
    String nombrevialidad
    String numext
    String numint
    TipoAsentamiento tipoasentamiento
    String otrotipoasentamiento
    TipoLocalidad tipolocalidad
    String otrotipolocalidad
    EntidadesCat entidadescat
    Municipio municipio
    String otromun;
    String nombreasentamiento
    String nombrelocalidad
    
    static belongsTo = [datosdeclarados: DatosDeclarados]
    
    static constraints = {
        nombrevialidad(nullable:true,blank:true,size:1..100)
        otrotipovialidad(nullable:true,blank:true,size:1..100)
        numext (nullable:true, size:1..10, blank:true)
        numint (nullable:true, size:1..10, blank:true)
        nombreasentamiento(nullable:true, size:1..100, blank:true)
        otrotipoasentamiento(nullable:true,blank:true,size:1..100)
        nombrelocalidad(nullable:true, size:1..100, blank:true)
        otrotipolocalidad(nullable:true,blank:true,size:1..100)
        otromun(nullable:true, size:1..150, blank:true)
        municipio(nullable:true)
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(dom){
        Boolean ok = false
        int error=0;
        while (!ok){
            DomicilioFiscal.withTransaction{
                status->
                try{
                    Long id = DomicilioFiscal.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    dom.id = id
                    dom.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
