<%@ page import="siteo.administracion.catalogos.EstadoConservacion" %>



<div class="fieldcontain ${hasErrors(bean: estadoConservacionInstance, field: 'estadoConservacion', 'error')} required">
	<label for="estadoConservacion">
		<g:message code="estadoConservacion.estadoConservacion.label" default="Estado de Conservacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="estadoConservacion" maxlength="20" required="" value="${estadoConservacionInstance?.estadoConservacion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: estadoConservacionInstance, field: 'factor', 'error')} ">
	<label for="factor">
		<g:message code="estadoConservacion.factor.label" default="Factor" />
		
	</label>
	<g:field name="factor" value="${fieldValue(bean: estadoConservacionInstance, field: 'factor')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: estadoConservacionInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="estadoConservacion.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${estadoConservacionInstance?.estatus}" />

</div>

