<%@ page import="siteo.administracion.catalogos.TipoConstruccion" %>



<div class="fieldcontain ${hasErrors(bean: tipoConstruccionInstance, field: 'tipoconstruccion', 'error')} required">
	<label for="tipoconstruccion">
		<g:message code="tipoConstruccion.tipoconstruccion.label" default="Tipoconstruccion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipoconstruccion" maxlength="25" required="" value="${tipoConstruccionInstance?.tipoconstruccion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoConstruccionInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoConstruccion.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoConstruccionInstance?.estatus}" />

</div>

