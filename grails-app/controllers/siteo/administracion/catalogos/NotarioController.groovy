package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class NotarioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort = "notaria"
        params.order ="asc"
        respond Notario.list(params), model:[notarioInstanceCount: Notario.count()]
    }

    def show(Notario notarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond notarioInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new Notario(params)
    }

    @Transactional
    def save(Notario notarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (notarioInstance == null) {
            notFound()
            return
        }

        if (notarioInstance.hasErrors()) {
            respond notarioInstance.errors, view:'create'
            return
        }

        notarioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'notario.label', default: 'Notario'), notarioInstance.id])
                redirect notarioInstance
            }
            '*' { respond notarioInstance, [status: CREATED] }
        }
    }

    def edit(Notario notarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond notarioInstance
    }

    @Transactional
    def update(Notario notarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (notarioInstance == null) {
            notFound()
            return
        }

        if (notarioInstance.hasErrors()) {
            respond notarioInstance.errors, view:'edit'
            return
        }

        notarioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Notario.label', default: 'Notario'), notarioInstance.id])
                redirect notarioInstance
            }
            '*'{ respond notarioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Notario notarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (notarioInstance == null) {
            notFound()
            return
        }

        notarioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Notario.label', default: 'Notario'), notarioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def not = Notario.get(params.idele)
        not.estatus = !not.estatus
        not.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'notario.label', default: 'Notario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
