<%@ page import="siteo.administracion.catalogos.Viento" %>



<div class="fieldcontain ${hasErrors(bean: vientoInstance, field: 'viento', 'error')} required">
	<label for="viento">
		<g:message code="viento.viento.label" default="Viento" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="viento" maxlength="9" required="" value="${vientoInstance?.viento}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: vientoInstance, field: 'abreviacion', 'error')} required">
	<label for="abreviacion">
		<g:message code="viento.abreviacion.label" default="Abreviacion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="abreviacion" maxlength="3" required="" value="${vientoInstance?.abreviacion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: vientoInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="viento.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${vientoInstance?.estatus}" />

</div>

