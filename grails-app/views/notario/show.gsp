
<%@ page import="siteo.administracion.catalogos.Notario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'notario.label', default: 'Notario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-notario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-notario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list notario">
			
				<g:if test="${notarioInstance?.notario}">
				<li class="fieldcontain">
					<span id="notario-label" class="property-label"><g:message code="notario.notario.label" default="Notario" /></span>
					
						<span class="property-value" aria-labelledby="notario-label"><g:fieldValue bean="${notarioInstance}" field="notario"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${notarioInstance?.notaria}">
				<li class="fieldcontain">
					<span id="notaria-label" class="property-label"><g:message code="notario.notaria.label" default="Notaria" /></span>
					
						<span class="property-value" aria-labelledby="notaria-label"><g:fieldValue bean="${notarioInstance}" field="notaria"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${notarioInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="notario.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${notarioInstance?.estatus}" true="ACTIVO" false="BAJA"/></span>
					
				</li>
				</g:if>
			</ol>
			<g:form url="[resource:notarioInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${notarioInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
