
<%@ page import="siteo.administracion.catalogos.UsoSuelo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usoSuelo.label', default: 'UsoSuelo')}" />
		<title><g:message code="Lista" args="[entityName]" /></title>
	</head>
	<body>
            <g:form name="festatus" controller="UsoSuelo" action="cambiaEstatus">
                <input id="idele" name="idele" type="hidden" value=""/>
            </g:form>
		<a href="#list-usoSuelo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-usoSuelo" class="content scaffold-list" role="main">
			<h1><g:message code="Lista" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="usosuelo" title="${message(code: 'usoSuelo.usosuelo.label', default: 'Uso suelo')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'usoSuelo.estatus.label', default: 'Estatus')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${usoSueloInstanceList}" status="i" var="usoSueloInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${usoSueloInstance.id}">${fieldValue(bean: usoSueloInstance, field: "usosuelo")}</g:link></td>
					
						<td><g:formatBoolean boolean="${usoSueloInstance.estatus}" true="ACTIVO" false="BAJA"/>
                                                    <a href="javascript: CambiaEstatus('${usoSueloInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${usoSueloInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
		</div>
	</body>
</html>
