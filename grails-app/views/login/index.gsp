<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Captura Masiva</title>
		<style type="text/css">
			label{
				float:left;
				width:65px;
			}
		</style>
		
        <script>        
        //BOTONES
        $(function() {
            $( "#btnIniciar" ).button({
                icons: {
                    primary: "ui-icon-key"
		}
            });
        });
        </script>
		
	</head>
	<body>
            <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
            </g:if>
                
                <g:if test="${usuarioError}">
                <ul class="errors" role="alert">
                    <li><g:message error="${usuarioError}"/></li>
                </ul>
                </g:if>
		<br/>
		<div style="width:300px" align="center">
			<span>Ingrese sus datos de acceso:</span>
		</div>
		<br/>
		<g:form controller="usuario" action="login" style="padding-left:300px">
			<div style="width:300px" align="center">
				<label>Usuario:</label><input class="text" type="text" name="usuario" required autofocus/>
			</div>
			<br/>
			<div style="width:300px" align="center">
				<label>Contrase&ntilde;a:</label><input class="text" type="password" name="passwd" required/>
			</div>
			<br/>
			<div style="width:300px" align="right">
				<label>&nbsp;</label><input type="submit" value="Entrar" id="btnIniciar"/>
			</div>
		</g:form>
	</body>
</html>