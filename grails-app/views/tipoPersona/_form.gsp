<%@ page import="siteo.administracion.catalogos.TipoPersona" %>



<div class="fieldcontain ${hasErrors(bean: tipoPersonaInstance, field: 'tipopersona', 'error')} required">
	<label for="tipopersona">
		<g:message code="tipoPersona.tipopersona.label" default="Tipo de persona" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipopersona" maxlength="10" required="" value="${tipoPersonaInstance?.tipopersona}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoPersonaInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoPersona.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoPersonaInstance?.estatus}" />

</div>

