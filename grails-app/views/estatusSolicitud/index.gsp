
<%@ page import="siteo.administracion.catalogos.EstatusSolicitud" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'estatusSolicitud.label', default: 'Estatus Solicitud')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
            <g:form name="festatus" controller="EstatusSolicitud" action="cambiaEstatus">
                <input id="idele" name="idele" type="hidden" value=""/>
            </g:form>
		<a href="#list-estatusSolicitud" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-estatusSolicitud" class="content scaffold-list" role="main">
			<h1><g:message code="Lista" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="estatussolicitud" title="${message(code: 'estatusSolicitud.estatussolicitud.label', default: 'Estatus solicitud')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'estatusSolicitud.estatus.label', default: 'Estatus')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${estatusSolicitudInstanceList}" status="i" var="estatusSolicitudInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${estatusSolicitudInstance.id}">${fieldValue(bean: estatusSolicitudInstance, field: "estatussolicitud")}</g:link></td>
					
                                                <td>
                                                    <g:if test="${estatusSolicitudInstance.estatus}">ACTIVO</g:if><g:else>BAJA</g:else>
                                                    <a href="javascript: CambiaEstatus('${estatusSolicitudInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a>
                                                </td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${estatusSolicitudInstanceCount ?: 0}" />
			</div>
		</div>
                <script>
                    function CambiaEstatus(id){
                    var idele = document.getElementById('idele');
                    idele.value = id;
                    var frm = document.getElementById('festatus');
                    frm.submit();
                    }
                </script>
	</body>
</html>
