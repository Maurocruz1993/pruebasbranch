package siteo.seguridad

import grails.transaction.Transactional

@Transactional
class BitacoraService {

    def registraActividad(Usuario us, String cadena, Long idrecibido) {
        def fechaactual = new Date()
        def accion = new Bitacora(actividad:cadena, usuario:us, fecha:fechaactual, idafectado:idrecibido).save(flush:true)
        if(!accion){
            System.out.println("error")
        }
    }
}
