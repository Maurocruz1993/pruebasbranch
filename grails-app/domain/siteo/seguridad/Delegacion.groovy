package siteo.seguridad

class Delegacion {

    String clave
    String nombre
    static hasMany = [municipios:Municipio]
    static constraints = {
        clave(nullable:true,size:1..15)
        nombre(blank:true, nullable:false,size:1..150)
    }
    
    String toString(){
        clave + " - " + nombre
    }
}
