package siteo.tramites

class SolicitudMasiva {
    
    String nombrepropietario;
    String lote;
    String mnza;
    String zona;
    String parcela;
    BigDecimal superficiepred;
    String colinda;
    Integer foliounico;
    
    static belongsTo=[lotetramite:LoteTramite]

    static constraints = {
        nombrepropietario(nullable:false, size:1..255, blank:true);
        lote(nullable:false, size:1..5, blank:true);
        mnza(nullable:false, size:1..3, blank:true);
        zona(nullable:false, size:1..2, blank:true);
        foliounico(nullable:false);
        superficiepred(nullable: true, scale:2, min: 0.000)
        colinda(nullable:false,size:1..50000,blank:true)
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(solic){
        Boolean ok = false
        int error=0;
        while (!ok){
            SolicitudMasiva.withTransaction{
                status->
                try{
                    Long id = SolicitudMasiva.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    solic.id = id
                    solic.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
