package siteo.administracion.catalogos

class UbicacionManzana {

    String ubicacionManzana
    Boolean estatus
    
    static constraints = {
        ubicacionManzana(nullable:false, size:1..20, blank:false)
    }
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        ubicacionManzana
    }
}
