package siteo.administracion.catalogos

class TipoPropietario {
    
    String tipopropietario
    Boolean estatus

    static constraints = {
        tipopropietario(nullable: false, size:1..15, blank: false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipopropietario
    }
}
