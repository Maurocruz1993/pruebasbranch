package siteo.seguridad

class Operacion {

    String operacion
    String controlador
    String accion
    Boolean estatus
    String parametros
    static belongsTo = [categoria:CategoriaOp]
    static constraints = {
        operacion (nullable:false, maxSize:50, blank:false)
        controlador (nullable:false, maxSize:100, blank:false)
        accion (nullable:false, maxSize:100, blank:false)
        parametros (nullable:true, maxSize:100, blank:true)
    }
    
    String toString(){
        operacion
    }
    static mapping = {
        id generator: 'increment'
    }
}
