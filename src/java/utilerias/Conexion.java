/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilerias;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.postgresql.Driver;

/**
 *
 * @author temoc
 */
public class Conexion {
    public Conexion(){
        
    }
    
    public Connection conectarpruebas(){
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://10.173.155.240:5432/siteonotpruebas";
            try {
                con = DriverManager.getConnection(url, "postgres","s1t30");
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
    
    public Connection conectar(){
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://10.173.155.240:5432/siteonotarios";
            try {
                con = DriverManager.getConnection(url, "postgres","s1t30");
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }
    
}
