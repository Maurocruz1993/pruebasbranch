
<%@ page import="siteo.administracion.catalogos.TipoTramite" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoTramite.label', default: 'TipoTramite')}" />
		<title><g:message code="Tipo de trámite" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipoTramite" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipoTramite" class="content scaffold-show" role="main">
			<h1><g:message code="Tipo de trámite" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipoTramite">
			
				<g:if test="${tipoTramiteInstance?.clave}">
				<li class="fieldcontain">
					<span id="clave-label" class="property-label"><g:message code="tipoTramite.clave.label" default="Clave" /></span>
					
						<span class="property-value" aria-labelledby="clave-label"><g:fieldValue bean="${tipoTramiteInstance}" field="clave"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipoTramiteInstance?.tipotramite}">
				<li class="fieldcontain">
					<span id="tipotramite-label" class="property-label"><g:message code="tipoTramite.tipotramite.label" default="Tipo de trámite" /></span>
					
						<span class="property-value" aria-labelledby="tipotramite-label"><g:fieldValue bean="${tipoTramiteInstance}" field="tipotramite"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipoTramiteInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="tipoTramite.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${tipoTramiteInstance?.estatus}" true="ACTIVO" false="BAJA" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tipoTramiteInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tipoTramiteInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
