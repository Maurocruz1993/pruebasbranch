package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoPropietarioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond TipoPropietario.list(params), model:[tipoPropietarioInstanceCount: TipoPropietario.count()]
    }

    def show(TipoPropietario tipoPropietarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoPropietarioInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoPropietario(params)
    }

    @Transactional
    def save(TipoPropietario tipoPropietarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPropietarioInstance == null) {
            notFound()
            return
        }

        if (tipoPropietarioInstance.hasErrors()) {
            respond tipoPropietarioInstance.errors, view:'create'
            return
        }

        tipoPropietarioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoPropietario.label', default: 'TipoPropietario'), tipoPropietarioInstance.id])
                redirect tipoPropietarioInstance
            }
            '*' { respond tipoPropietarioInstance, [status: CREATED] }
        }
    }

    def edit(TipoPropietario tipoPropietarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoPropietarioInstance
    }

    @Transactional
    def update(TipoPropietario tipoPropietarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPropietarioInstance == null) {
            notFound()
            return
        }

        if (tipoPropietarioInstance.hasErrors()) {
            respond tipoPropietarioInstance.errors, view:'edit'
            return
        }

        tipoPropietarioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoPropietario.label', default: 'TipoPropietario'), tipoPropietarioInstance.id])
                redirect tipoPropietarioInstance
            }
            '*'{ respond tipoPropietarioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoPropietario tipoPropietarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPropietarioInstance == null) {
            notFound()
            return
        }

        tipoPropietarioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoPropietario.label', default: 'TipoPropietario'), tipoPropietarioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoProp = TipoPropietario.get(params.idele)
        tipoProp.estatus = !tipoProp.estatus
        tipoProp.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoPropietario.label', default: 'TipoPropietario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
