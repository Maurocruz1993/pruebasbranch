<%@ page import="siteo.administracion.catalogos.TipoLocalidad" %>



<div class="fieldcontain ${hasErrors(bean: tipoLocalidadInstance, field: 'tipolocalidad', 'error')} required">
	<label for="tipolocalidad">
		<g:message code="tipoLocalidad.tipolocalidad.label" default="Tipo de localidad" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipolocalidad" maxlength="100" required="" value="${tipoLocalidadInstance?.tipolocalidad}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoLocalidadInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoLocalidad.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoLocalidadInstance?.estatus}" />

</div>

