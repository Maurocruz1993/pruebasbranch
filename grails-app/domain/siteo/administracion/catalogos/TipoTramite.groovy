package siteo.administracion.catalogos

class TipoTramite {

    Integer clave
    String tipotramite
    Boolean estatus
    
    static constraints = {
        clave (nullable:false)
        tipotramite (nullable:false, size:1..25, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        clave.toString()+" - "+tipotramite
    }
}
