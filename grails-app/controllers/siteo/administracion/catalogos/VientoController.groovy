package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class VientoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond Viento.list(params), model:[vientoInstanceCount: Viento.count()]
    }

    def show(Viento vientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond vientoInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new Viento(params)
    }

    @Transactional
    def save(Viento vientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (vientoInstance == null) {
            notFound()
            return
        }

        if (vientoInstance.hasErrors()) {
            respond vientoInstance.errors, view:'create'
            return
        }

        vientoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'viento.label', default: 'Viento'), vientoInstance.id])
                redirect vientoInstance
            }
            '*' { respond vientoInstance, [status: CREATED] }
        }
    }

    def edit(Viento vientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond vientoInstance
    }

    @Transactional
    def update(Viento vientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (vientoInstance == null) {
            notFound()
            return
        }

        if (vientoInstance.hasErrors()) {
            respond vientoInstance.errors, view:'edit'
            return
        }

        vientoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Viento.label', default: 'Viento'), vientoInstance.id])
                redirect vientoInstance
            }
            '*'{ respond vientoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Viento vientoInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (vientoInstance == null) {
            notFound()
            return
        }

        vientoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Viento.label', default: 'Viento'), vientoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def viento = Viento.get(params.idele)
        viento.estatus = !viento.estatus
        viento.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'viento.label', default: 'Viento'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
