<%@ page import="siteo.administracion.catalogos.NumeroNiveles" %>



<div class="fieldcontain ${hasErrors(bean: numeroNivelesInstance, field: 'nivel', 'error')} required">
	<label for="nivel">
		<g:message code="numeroNiveles.nivel.label" default="Nivel" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nivel" maxlength="40" required="" value="${numeroNivelesInstance?.nivel}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: numeroNivelesInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="numeroNiveles.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${numeroNivelesInstance?.estatus}" />

</div>

