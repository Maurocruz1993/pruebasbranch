
<%@ page import="siteo.administracion.catalogos.MovimientoCatastral" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'movimientoCatastral.label', default: 'MovimientoCatastral')}" />
		<title><g:message code="Movimiento catastral" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-movimientoCatastral" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-movimientoCatastral" class="content scaffold-show" role="main">
			<h1><g:message code="Movimiento Catastral" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list movimientoCatastral">
			
				<g:if test="${movimientoCatastralInstance?.clave}">
				<li class="fieldcontain">
					<span id="clave-label" class="property-label"><g:message code="movimientoCatastral.clave.label" default="Clave" /></span>
					
						<span class="property-value" aria-labelledby="clave-label"><g:fieldValue bean="${movimientoCatastralInstance}" field="clave"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.movimiento}">
				<li class="fieldcontain">
					<span id="movimiento-label" class="property-label"><g:message code="movimientoCatastral.movimiento.label" default="Movimiento" /></span>
					
						<span class="property-value" aria-labelledby="movimiento-label"><g:fieldValue bean="${movimientoCatastralInstance}" field="movimiento"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.cuentaopcional}">
				<li class="fieldcontain">
					<span id="cuentaopcional-label" class="property-label"><g:message code="movimientoCatastral.cuentaopcional.label" default="Cuentaopcional" /></span>
					
						<span class="property-value" aria-labelledby="cuentaopcional-label"><g:formatBoolean boolean="${movimientoCatastralInstance?.cuentaopcional}" true="SÍ" false="NO"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.cuentarequerida}">
				<li class="fieldcontain">
					<span id="cuentarequerida-label" class="property-label"><g:message code="movimientoCatastral.cuentarequerida.label" default="Cuentarequerida" /></span>
					
						<span class="property-value" aria-labelledby="cuentarequerida-label"><g:formatBoolean boolean="${movimientoCatastralInstance?.cuentarequerida}" true="SÍ" false="NO"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.esparcial}">
				<li class="fieldcontain">
					<span id="esparcial-label" class="property-label"><g:message code="movimientoCatastral.esparcial.label" default="Esparcial" /></span>
					
						<span class="property-value" aria-labelledby="esparcial-label"><g:formatBoolean boolean="${movimientoCatastralInstance?.esparcial}" true="SÍ" false="NO"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="movimientoCatastral.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${movimientoCatastralInstance?.estatus}" true="ACTIVO" false="BAJA"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.estotal}">
				<li class="fieldcontain">
					<span id="estotal-label" class="property-label"><g:message code="movimientoCatastral.estotal.label" default="Estotal" /></span>
					
						<span class="property-value" aria-labelledby="estotal-label"><g:formatBoolean boolean="${movimientoCatastralInstance?.estotal}" true="SÍ" false="NO" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.generacuenta}">
				<li class="fieldcontain">
					<span id="generacuenta-label" class="property-label"><g:message code="movimientoCatastral.generacuenta.label" default="Generacuenta" /></span>
					
						<span class="property-value" aria-labelledby="generacuenta-label"><g:formatBoolean boolean="${movimientoCatastralInstance?.generacuenta}" true="SÍ" false="NO"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.pagorequerido}">
				<li class="fieldcontain">
					<span id="pagorequerido-label" class="property-label"><g:message code="movimientoCatastral.pagorequerido.label" default="Pagorequerido" /></span>
					
						<span class="property-value" aria-labelledby="pagorequerido-label"><g:formatBoolean boolean="${movimientoCatastralInstance?.pagorequerido}" true="SÍ" false="NO"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${movimientoCatastralInstance?.tipotramite}">
				<li class="fieldcontain">
					<span id="tipotramite-label" class="property-label"><g:message code="movimientoCatastral.tipotramite.label" default="Tipotramite" /></span>
					
						<span class="property-value" aria-labelledby="tipotramite-label"><g:link controller="tipoTramite" action="show" id="${movimientoCatastralInstance?.tipotramite?.id}">${movimientoCatastralInstance?.tipotramite?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:movimientoCatastralInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${movimientoCatastralInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
