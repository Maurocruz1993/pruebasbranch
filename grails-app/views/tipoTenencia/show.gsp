
<%@ page import="siteo.administracion.catalogos.TipoTenencia" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoTenencia.label', default: 'TipoTenencia')}" />
		<title><g:message code="Tipo de tenencia" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipoTenencia" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipoTenencia" class="content scaffold-show" role="main">
			<h1><g:message code="Tipo de tenencia" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipoTenencia">
			
				<g:if test="${tipoTenenciaInstance?.tipotenencia}">
				<li class="fieldcontain">
					<span id="tipotenencia-label" class="property-label"><g:message code="tipoTenencia.tipotenencia.label" default="Tipo de tenencia" /></span>
					
						<span class="property-value" aria-labelledby="tipotenencia-label"><g:fieldValue bean="${tipoTenenciaInstance}" field="tipotenencia"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipoTenenciaInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="tipoTenencia.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${tipoTenenciaInstance?.estatus}" true="ACTIVO" false="BAJA" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tipoTenenciaInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tipoTenenciaInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
