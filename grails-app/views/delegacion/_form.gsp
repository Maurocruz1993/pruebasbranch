<%@ page import="siteo.seguridad.Delegacion" %>



<div class="fieldcontain ${hasErrors(bean: delegacionInstance, field: 'clave', 'error')} ">
	<label for="clave">
		<g:message code="delegacion.clave.label" default="Clave" />
		
	</label>
	<g:textField name="clave" maxlength="15" value="${delegacionInstance?.clave}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: delegacionInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="delegacion.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" maxlength="150" required="" value="${delegacionInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: delegacionInstance, field: 'municipios', 'error')} ">
	<label for="municipios">
		<g:message code="delegacion.municipios.label" default="Municipios" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${delegacionInstance?.municipios?}" var="m">
    <li><g:link controller="municipio" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="municipio" action="create" params="['delegacion.id': delegacionInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'municipio.label', default: 'Municipio')])}</g:link>
</li>
</ul>


</div>

