
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
		<title>Login</title>
	</head>
	<body>
		<a href="#list-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		
		<div id="list-usuario" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
                            <br>
                            <br>
                            <br>
                            <g:form controller="usuario" action="login">	
                                <span class='nameClean'><label for="usuario">Login</label></span>
                                        <td valign="top" class="value ${hasErrors(bean:usuarioInstance, field:'usuario', 'errors')}">
                                                <input type="text" id="usuario" name="usuario" value="${usuarioInstance?.usuario}"/>
                                        </td>
                                        <br>
                                <span class='nameClean'><label for="passwd">Password</label></span>
                                        <td valign="top" class="value ${hasErrors(bean:usuarioInstance, field:'passwd', 'errors')}">
                                                <input type="password" id="pass" name="passwd" value="${usuarioInstance?.passwd}"/>
                                        </td>
                                <div class="buttons">
                                        <span class="button"><input class="save" type="submit" value="login"/></span>
                                </div>
                            </g:form>
		</div>
	</body>
</html>
