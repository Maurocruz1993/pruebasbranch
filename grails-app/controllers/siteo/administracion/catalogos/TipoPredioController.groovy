package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoPredioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond TipoPredio.list(params), model:[tipoPredioInstanceCount: TipoPredio.count()]
    }

    def show(TipoPredio tipoPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoPredioInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoPredio(params)
    }

    @Transactional
    def save(TipoPredio tipoPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPredioInstance == null) {
            notFound()
            return
        }

        if (tipoPredioInstance.hasErrors()) {
            respond tipoPredioInstance.errors, view:'create'
            return
        }

        tipoPredioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoPredio.label', default: 'TipoPredio'), tipoPredioInstance.id])
                redirect tipoPredioInstance
            }
            '*' { respond tipoPredioInstance, [status: CREATED] }
        }
    }

    def edit(TipoPredio tipoPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoPredioInstance
    }

    @Transactional
    def update(TipoPredio tipoPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPredioInstance == null) {
            notFound()
            return
        }

        if (tipoPredioInstance.hasErrors()) {
            respond tipoPredioInstance.errors, view:'edit'
            return
        }

        tipoPredioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoPredio.label', default: 'TipoPredio'), tipoPredioInstance.id])
                redirect tipoPredioInstance
            }
            '*'{ respond tipoPredioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoPredio tipoPredioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoPredioInstance == null) {
            notFound()
            return
        }

        tipoPredioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoPredio.label', default: 'TipoPredio'), tipoPredioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoPred = TipoPredio.get(params.idele)
        tipoPred.estatus = !tipoPred.estatus
        tipoPred.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoPredio.label', default: 'TipoPredio'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
