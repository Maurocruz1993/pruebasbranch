<%@ page import="siteo.administracion.catalogos.TipoColindancia" %>



<div class="fieldcontain ${hasErrors(bean: tipoColindanciaInstance, field: 'tipocolindancia', 'error')} required">
	<label for="tipocolindancia">
		<g:message code="tipoColindancia.tipocolindancia.label" default="Tipo de colindancia" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipocolindancia" maxlength="10" required="" value="${tipoColindanciaInstance?.tipocolindancia}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoColindanciaInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoColindancia.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoColindanciaInstance?.estatus}" />

</div>

