package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EstadoConservacionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort = "id"
        params.order ="asc"
        params.max = Math.min(max ?: 10, 100)
        respond EstadoConservacion.list(params), model:[estadoConservacionInstanceCount: EstadoConservacion.count()]
    }

    def show(EstadoConservacion estadoConservacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond estadoConservacionInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new EstadoConservacion(params)
    }

    @Transactional
    def save(EstadoConservacion estadoConservacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (estadoConservacionInstance == null) {
            notFound()
            return
        }

        if (estadoConservacionInstance.hasErrors()) {
            respond estadoConservacionInstance.errors, view:'create'
            return
        }

        estadoConservacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'estadoConservacion.label', default: 'EstadoConservacion'), estadoConservacionInstance.id])
                redirect estadoConservacionInstance
            }
            '*' { respond estadoConservacionInstance, [status: CREATED] }
        }
    }

    def edit(EstadoConservacion estadoConservacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond estadoConservacionInstance
    }

    @Transactional
    def update(EstadoConservacion estadoConservacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (estadoConservacionInstance == null) {
            notFound()
            return
        }

        if (estadoConservacionInstance.hasErrors()) {
            respond estadoConservacionInstance.errors, view:'edit'
            return
        }

        estadoConservacionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EstadoConservacion.label', default: 'EstadoConservacion'), estadoConservacionInstance.id])
                redirect estadoConservacionInstance
            }
            '*'{ respond estadoConservacionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EstadoConservacion estadoConservacionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (estadoConservacionInstance == null) {
            notFound()
            return
        }

        estadoConservacionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EstadoConservacion.label', default: 'EstadoConservacion'), estadoConservacionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def estadoc = EstadoConservacion.get(params.idele)
        estadoc.estatus = !estadoc.estatus
        estadoc.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'estadoConservacion.label', default: 'EstadoConservacion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
