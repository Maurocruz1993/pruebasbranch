package siteo.padron

import siteo.administracion.catalogos.FormaPredio
import siteo.administracion.catalogos.UbicacionManzana

class DatosTerreno {

    String zonaValor;
    UbicacionManzana ubicacionMza;
    FormaPredio forma;
    BigDecimal indiviso;
    
    static belongsTo=[datosDeclarados:DatosDeclarados]
    
    static constraints = {
        ubicacionMza(nullable:true)
        forma(nullable:true)
        zonaValor(nullable: true, size:1..25,blank:true)
        indiviso(nullable: true, scale:2, min: 0.000)
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(dt){
        Boolean ok = false
        int error=0;
        while (!ok){
            DatosTerreno.withTransaction{
                status->
                try{
                    Long id = DatosTerreno.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    dt.id = id
                    dt.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
