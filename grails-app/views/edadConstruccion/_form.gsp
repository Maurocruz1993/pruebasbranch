<%@ page import="siteo.administracion.catalogos.EdadConstruccion" %>



<div class="fieldcontain ${hasErrors(bean: edadConstruccionInstance, field: 'edadConstruccion', 'error')} required">
	<label for="edadConstruccion">
		<g:message code="edadConstruccion.edadConstruccion.label" default="Edad de la Construccion" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="edadConstruccion" maxlength="20" required="" value="${edadConstruccionInstance?.edadConstruccion}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: edadConstruccionInstance, field: 'factor', 'error')} ">
	<label for="factor">
		<g:message code="edadConstruccion.factor.label" default="Factor" />
		
	</label>
	<g:field name="factor" value="${fieldValue(bean: edadConstruccionInstance, field: 'factor')}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: edadConstruccionInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="edadConstruccion.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${edadConstruccionInstance?.estatus}" />

</div>

