package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoColindanciaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort="id"
        params.order="asc"
        respond TipoColindancia.list(params), model:[tipoColindanciaInstanceCount: TipoColindancia.count()]
    }

    def show(TipoColindancia tipoColindanciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoColindanciaInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoColindancia(params)
    }

    @Transactional
    def save(TipoColindancia tipoColindanciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoColindanciaInstance == null) {
            notFound()
            return
        }

        if (tipoColindanciaInstance.hasErrors()) {
            respond tipoColindanciaInstance.errors, view:'create'
            return
        }

        tipoColindanciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoColindancia.label', default: 'TipoColindancia'), tipoColindanciaInstance.id])
                redirect tipoColindanciaInstance
            }
            '*' { respond tipoColindanciaInstance, [status: CREATED] }
        }
    }

    def edit(TipoColindancia tipoColindanciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoColindanciaInstance
    }

    @Transactional
    def update(TipoColindancia tipoColindanciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoColindanciaInstance == null) {
            notFound()
            return
        }

        if (tipoColindanciaInstance.hasErrors()) {
            respond tipoColindanciaInstance.errors, view:'edit'
            return
        }

        tipoColindanciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoColindancia.label', default: 'TipoColindancia'), tipoColindanciaInstance.id])
                redirect tipoColindanciaInstance
            }
            '*'{ respond tipoColindanciaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoColindancia tipoColindanciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoColindanciaInstance == null) {
            notFound()
            return
        }

        tipoColindanciaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoColindancia.label', default: 'TipoColindancia'), tipoColindanciaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoCol = TipoColindancia.get(params.idele)
        tipoCol.estatus = !tipoCol.estatus
        tipoCol.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoColindancia.label', default: 'TipoColindancia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
