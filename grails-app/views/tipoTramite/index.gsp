
<%@ page import="siteo.administracion.catalogos.TipoTramite" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoTramite.label', default: 'TipoTramite')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
            <g:form name="festatus" controller="TipoTramite" action="cambiaEstatus">
                <input id="idele" name="idele" type="hidden" value=""/>
            </g:form>
		<a href="#list-tipoTramite" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-tipoTramite" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="clave" title="${message(code: 'tipoTramite.clave.label', default: 'Clave')}" />
					
						<g:sortableColumn property="tipotramite" title="${message(code: 'tipoTramite.tipotramite.label', default: 'Tipo de tramite')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'tipoTramite.estatus.label', default: 'Estatus')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${tipoTramiteInstanceList}" status="i" var="tipoTramiteInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${tipoTramiteInstance.id}">${fieldValue(bean: tipoTramiteInstance, field: "clave")}</g:link></td>
					
						<td>${fieldValue(bean: tipoTramiteInstance, field: "tipotramite")}</td>
					
						<td><g:formatBoolean boolean="${tipoTramiteInstance.estatus}" true="ACTIVO" false="BAJA" />
                                                    <a href="javascript: CambiaEstatus('${tipoTramiteInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${tipoTramiteInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
		</div>
	</body>
</html>
