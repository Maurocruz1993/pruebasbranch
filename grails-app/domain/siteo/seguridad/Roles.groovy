package siteo.seguridad

class Roles {
    String rol
    Boolean estatus
    
    static hasMany = [operacion:Operacion]
    static constraints = {
        rol(unique:true)
    }
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        rol
    }
    
}
