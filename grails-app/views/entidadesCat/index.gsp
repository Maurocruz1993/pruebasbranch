
<%@ page import="siteo.administracion.catalogos.EntidadesCat" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'entidadesCat.label', default: 'EntidadesCat')}" />
		<title><g:message code="Lista" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-entidadesCat" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-entidadesCat" class="content scaffold-list" role="main">
			<h1><g:message code="Lista" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
                                                <g:sortableColumn property="clave" title="${message(code: 'entidadesCat.clave.label', default: 'Clave')}" />
                                                
						<g:sortableColumn property="nombre" title="${message(code: 'entidadesCat.nombre.label', default: 'Nombre')}" />
					
						<g:sortableColumn property="abrev" title="${message(code: 'entidadesCat.abrev.label', default: 'Abrev')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${entidadesCatInstanceList}" status="i" var="entidadesCatInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                                                <td><g:link action="edit" id="${entidadesCatInstance.id}">${fieldValue(bean: entidadesCatInstance, field: "clave")}</g:link></td>
                                                
						<td>${fieldValue(bean: entidadesCatInstance, field: "nombre")}</td>
					
                                                <td>${fieldValue(bean: entidadesCatInstance, field: "abrev")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${entidadesCatInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
