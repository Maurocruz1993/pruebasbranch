
<%@ page import="siteo.seguridad.CategoriaOp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'categoriaOp.label', default: 'CategoriaOp')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-categoriaOp" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-categoriaOp" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list categoriaOp">
			
				<g:if test="${categoriaOpInstance?.padre}">
				<li class="fieldcontain">
					<span id="padre-label" class="property-label"><g:message code="categoriaOp.padre.label" default="Padre" /></span>
					
						<span class="property-value" aria-labelledby="padre-label"><g:fieldValue bean="${categoriaOpInstance}" field="padre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${categoriaOpInstance?.categoria}">
				<li class="fieldcontain">
					<span id="categoria-label" class="property-label"><g:message code="categoriaOp.categoria.label" default="Categoria" /></span>
					
						<span class="property-value" aria-labelledby="categoria-label"><g:fieldValue bean="${categoriaOpInstance}" field="categoria"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${categoriaOpInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="categoriaOp.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${categoriaOpInstance?.estatus}" true="Activo" false="Baja" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${categoriaOpInstance?.operaciones}">
				<li class="fieldcontain">
					<span id="operaciones-label" class="property-label"><g:message code="categoriaOp.operaciones.label" default="Operaciones" /></span>
					
						<g:each in="${categoriaOpInstance.operaciones}" var="o">
						<span class="property-value" aria-labelledby="operaciones-label"><g:link controller="operacion" action="show" id="${o.id}">${o?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:categoriaOpInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${categoriaOpInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
