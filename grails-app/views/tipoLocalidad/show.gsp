
<%@ page import="siteo.administracion.catalogos.TipoLocalidad" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoLocalidad.label', default: 'TipoLocalidad')}" />
		<title><g:message code="Tipo de localidad" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipoLocalidad" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipoLocalidad" class="content scaffold-show" role="main">
			<h1><g:message code="Tipo de localidad" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipoLocalidad">
			
				<g:if test="${tipoLocalidadInstance?.tipolocalidad}">
				<li class="fieldcontain">
					<span id="tipolocalidad-label" class="property-label"><g:message code="tipoLocalidad.tipolocalidad.label" default="Tipo de localidad" /></span>
					
						<span class="property-value" aria-labelledby="tipolocalidad-label"><g:fieldValue bean="${tipoLocalidadInstance}" field="tipolocalidad"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipoLocalidadInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="tipoLocalidad.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${tipoLocalidadInstance?.estatus}" true="ACTIVO" false="BAJA" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tipoLocalidadInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tipoLocalidadInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
