package siteo.administracion.catalogos

class TipoVialidad {

    String tipovialidad
    Boolean estatus
    
    static constraints = {
        tipovialidad (nullable:false, size:1..20, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipovialidad
    }
}
