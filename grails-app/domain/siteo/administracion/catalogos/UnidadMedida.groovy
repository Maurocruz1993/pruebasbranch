package siteo.administracion.catalogos

class UnidadMedida {

    String unidad
    String abreviacion
    Boolean estatus
    
    static constraints = {
        unidad(nullable:false, size:1..20, blank:false)
        abreviacion(nullable:false, size:1..5, blank:false)
    }
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        abreviacion
    }
}
