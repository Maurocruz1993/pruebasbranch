package siteo.seguridad



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CategoriaOpController {

    def validaSesionService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort = "categoria"
        params.order ="asc"
        respond CategoriaOp.list(params), model:[categoriaOpInstanceCount: CategoriaOp.count()]
    }

    def show(CategoriaOp categoriaOpInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        respond categoriaOpInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        respond new CategoriaOp(params)
    }

    @Transactional
    def save(CategoriaOp categoriaOpInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        if (categoriaOpInstance == null) {
            notFound()
            return
        }

        if (categoriaOpInstance.hasErrors()) {
            respond categoriaOpInstance.errors, view:'create'
            return
        }

        categoriaOpInstance.padre = new Long(Integer.parseInt(params.catenviada))
        categoriaOpInstance.save flush:true
        if(params.sub){
            categoriaOpInstance.padre = categoriaOpInstance.id 
            categoriaOpInstance.save flush:true
        }
        redirect(action:'index')
        /*request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'categoriaOp.label', default: 'CategoriaOp'), categoriaOpInstance.id])
                redirect categoriaOpInstance
            }
            '*' { respond categoriaOpInstance, [status: CREATED] }
        }*/
    }

    def edit(CategoriaOp categoriaOpInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        respond categoriaOpInstance
    }

    @Transactional
    def update(CategoriaOp categoriaOpInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        if (categoriaOpInstance == null) {
            notFound()
            return
        }

        if (categoriaOpInstance.hasErrors()) {
            respond categoriaOpInstance.errors, view:'edit'
            return
        }
        categoriaOpInstance.padre = new Long(Integer.parseInt(params.catenviada))
        categoriaOpInstance.save flush:true
        if(params.sub){
            categoriaOpInstance.padre = categoriaOpInstance.id 
            categoriaOpInstance.save flush:true
        }
        redirect(action:'index')
        
        /*request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'CategoriaOp.label', default: 'CategoriaOp'), categoriaOpInstance.id])
                redirect categoriaOpInstance
            }
            '*'{ respond categoriaOpInstance, [status: OK] }
        }*/
    }

    @Transactional
    def delete(CategoriaOp categoriaOpInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        
        if (categoriaOpInstance == null) {
            notFound()
            return
        }

        categoriaOpInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CategoriaOp.label', default: 'CategoriaOp'), categoriaOpInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout');
            return;
        }
        def instancia = CategoriaOp.get(params.idele)
        instancia.estatus = !instancia.estatus
        instancia.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'categoriaOp.label', default: 'CategoriaOp'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
