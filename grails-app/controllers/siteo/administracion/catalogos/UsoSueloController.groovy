package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UsoSueloController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond UsoSuelo.list(params), model:[usoSueloInstanceCount: UsoSuelo.count()]
    }

    def show(UsoSuelo usoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond usoSueloInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new UsoSuelo(params)
    }

    @Transactional
    def save(UsoSuelo usoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (usoSueloInstance == null) {
            notFound()
            return
        }

        if (usoSueloInstance.hasErrors()) {
            respond usoSueloInstance.errors, view:'create'
            return
        }

        usoSueloInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usoSuelo.label', default: 'UsoSuelo'), usoSueloInstance.id])
                redirect usoSueloInstance
            }
            '*' { respond usoSueloInstance, [status: CREATED] }
        }
    }

    def edit(UsoSuelo usoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond usoSueloInstance
    }

    @Transactional
    def update(UsoSuelo usoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (usoSueloInstance == null) {
            notFound()
            return
        }

        if (usoSueloInstance.hasErrors()) {
            respond usoSueloInstance.errors, view:'edit'
            return
        }

        usoSueloInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UsoSuelo.label', default: 'UsoSuelo'), usoSueloInstance.id])
                redirect usoSueloInstance
            }
            '*'{ respond usoSueloInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UsoSuelo usoSueloInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (usoSueloInstance == null) {
            notFound()
            return
        }

        usoSueloInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UsoSuelo.label', default: 'UsoSuelo'), usoSueloInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def usoSuelo = UsoSuelo.get(params.idele)
        usoSuelo.estatus = !usoSuelo.estatus
        usoSuelo.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usoSuelo.label', default: 'UsoSuelo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
