
<%@ page import="siteo.seguridad.Usuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-usuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-usuario" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
                        <table>
                            <tr>
                                <th><g:form action="buscar">
                                    Usuario:
                                    <g:textField name="criterio" required="" autofocus="" />
                                    <g:submitButton name="Buscar" action="buscar" value="Buscar"/>
                                    </g:form>
                                </th>
                            </tr>
                        </table>
			<table>
			<thead>
					<tr>
						<g:sortableColumn property="nombre" title="${message(code: 'usuario.nombre.label', default: 'Nombre')}" />
                                                
                                                <g:sortableColumn property="usuario" title="${message(code: 'usuario.usuario.label', default: 'Usuario')}" />
					
						<g:sortableColumn property="estatus" title="${message(code: 'usuario.estatus.label', default: 'Estatus')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${usuarioInstanceList}" status="i" var="usuarioInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${usuarioInstance.id}">${fieldValue(bean: usuarioInstance, field: "nombre")}</g:link></td>
					
						<td>${fieldValue(bean: usuarioInstance, field: "usuario")}</td>
					
						<td><g:formatBoolean true="ACTIVO" false="BAJA" boolean="${usuarioInstance.estatus}" />
                                                    <a href="javascript: CambiaEstatus('${usuarioInstance.id}')" title="Cambiar Estatus">
                                                        <img src="${assetPath(src: '/skin/switch.png')}" width="20" height="20"/>
                                                    </a>
                                                </td>
                                                
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${usuarioInstanceCount ?: 0}" />
			</div>
                        <script>
                            function CambiaEstatus(id){
                                var idele = document.getElementById('idele');
                                idele.value = id;
                                var frm = document.getElementById('festatus');
                                frm.submit();
                            }
                        </script>
                        <g:form name="festatus" controller="Usuario" action="cambiaEstatus">
                            <input id="idele" name="idele" type="hidden" value=""/>
                        </g:form>
		</div>
	</body>
</html>
