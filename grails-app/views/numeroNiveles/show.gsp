
<%@ page import="siteo.administracion.catalogos.NumeroNiveles" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'numeroNiveles.label', default: 'NumeroNiveles')}" />
		<title><g:message code="Número del nivel" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-numeroNiveles" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-numeroNiveles" class="content scaffold-show" role="main">
			<h1><g:message code="Número del nivel" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list numeroNiveles">
			
				<g:if test="${numeroNivelesInstance?.nivel}">
				<li class="fieldcontain">
					<span id="nivel-label" class="property-label"><g:message code="numeroNiveles.nivel.label" default="Nivel" /></span>
					
						<span class="property-value" aria-labelledby="nivel-label"><g:fieldValue bean="${numeroNivelesInstance}" field="nivel"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${numeroNivelesInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="numeroNiveles.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${numeroNivelesInstance?.estatus}" true="ACTIVO" false="BAJA"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:numeroNivelesInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${numeroNivelesInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
