package siteo.administracion.catalogos

class MovimientoCatastral {

    Integer clave
    String movimiento
    Boolean estatus
    TipoTramite tipotramite
    Boolean cuentarequerida
    Boolean cuentaopcional
    Boolean pagorequerido
    Boolean estotal
    Boolean esparcial
    Boolean generacuenta
    
    static constraints = {
        clave (nullable:false)
        movimiento (nullable:false, size:1..150, blank:false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        clave.toString()+" - "+movimiento
    }
}
