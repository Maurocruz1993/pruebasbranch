package siteo.administracion.catalogos

class TipoTenencia {

    String tipotenencia
    Boolean estatus
    
    static constraints = {
        tipotenencia (nullable:false, size:1..7, blank: false)
    }

    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        tipotenencia
    }
}
