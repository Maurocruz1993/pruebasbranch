package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoTramiteController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond TipoTramite.list(params), model:[tipoTramiteInstanceCount: TipoTramite.count()]
    }

    def show(TipoTramite tipoTramiteInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoTramiteInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoTramite(params)
    }

    @Transactional
    def save(TipoTramite tipoTramiteInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoTramiteInstance == null) {
            notFound()
            return
        }

        if (tipoTramiteInstance.hasErrors()) {
            respond tipoTramiteInstance.errors, view:'create'
            return
        }

        tipoTramiteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoTramite.label', default: 'TipoTramite'), tipoTramiteInstance.id])
                redirect tipoTramiteInstance
            }
            '*' { respond tipoTramiteInstance, [status: CREATED] }
        }
    }

    def edit(TipoTramite tipoTramiteInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoTramiteInstance
    }

    @Transactional
    def update(TipoTramite tipoTramiteInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoTramiteInstance == null) {
            notFound()
            return
        }

        if (tipoTramiteInstance.hasErrors()) {
            respond tipoTramiteInstance.errors, view:'edit'
            return
        }

        tipoTramiteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoTramite.label', default: 'TipoTramite'), tipoTramiteInstance.id])
                redirect tipoTramiteInstance
            }
            '*'{ respond tipoTramiteInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoTramite tipoTramiteInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoTramiteInstance == null) {
            notFound()
            return
        }

        tipoTramiteInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoTramite.label', default: 'TipoTramite'), tipoTramiteInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoTram = TipoTramite.get(params.idele)
        tipoTram.estatus = !tipoTram.estatus
        tipoTram.save flush:true
        
        redirect(action: 'index')
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoTramite.label', default: 'TipoTramite'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
