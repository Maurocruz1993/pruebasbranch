package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DistritosCatController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        respond DistritosCat.list(params), model:[distritosCatInstanceCount: DistritosCat.count()]
    }

    def show(DistritosCat distritosCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond distritosCatInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new DistritosCat(params)
    }

    @Transactional
    def save(DistritosCat distritosCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (distritosCatInstance == null) {
            notFound()
            return
        }

        if (distritosCatInstance.hasErrors()) {
            respond distritosCatInstance.errors, view:'create'
            return
        }

        distritosCatInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'distritosCat.label', default: 'DistritosCat'), distritosCatInstance.id])
                redirect distritosCatInstance
            }
            '*' { respond distritosCatInstance, [status: CREATED] }
        }
    }

    def edit(DistritosCat distritosCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond distritosCatInstance
    }

    @Transactional
    def update(DistritosCat distritosCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (distritosCatInstance == null) {
            notFound()
            return
        }

        if (distritosCatInstance.hasErrors()) {
            respond distritosCatInstance.errors, view:'edit'
            return
        }

        distritosCatInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DistritosCat.label', default: 'DistritosCat'), distritosCatInstance.id])
                redirect distritosCatInstance
            }
            '*'{ respond distritosCatInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DistritosCat distritosCatInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }

        if (distritosCatInstance == null) {
            notFound()
            return
        }

        distritosCatInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DistritosCat.label', default: 'DistritosCat'), distritosCatInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'distritosCat.label', default: 'DistritosCat'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
