package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EdadConstruccionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort = "id"
        params.order ="asc"
        respond EdadConstruccion.list(params), model:[edadConstruccionInstanceCount: EdadConstruccion.count()]
    }

    def show(EdadConstruccion edadConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond edadConstruccionInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new EdadConstruccion(params)
    }

    @Transactional
    def save(EdadConstruccion edadConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (edadConstruccionInstance == null) {
            notFound()
            return
        }

        if (edadConstruccionInstance.hasErrors()) {
            respond edadConstruccionInstance.errors, view:'create'
            return
        }

        edadConstruccionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'edadConstruccion.label', default: 'EdadConstruccion'), edadConstruccionInstance.id])
                redirect edadConstruccionInstance
            }
            '*' { respond edadConstruccionInstance, [status: CREATED] }
        }
    }

    def edit(EdadConstruccion edadConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond edadConstruccionInstance
    }

    @Transactional
    def update(EdadConstruccion edadConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (edadConstruccionInstance == null) {
            notFound()
            return
        }

        if (edadConstruccionInstance.hasErrors()) {
            respond edadConstruccionInstance.errors, view:'edit'
            return
        }

        edadConstruccionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EdadConstruccion.label', default: 'EdadConstruccion'), edadConstruccionInstance.id])
                redirect edadConstruccionInstance
            }
            '*'{ respond edadConstruccionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EdadConstruccion edadConstruccionInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (edadConstruccionInstance == null) {
            notFound()
            return
        }

        edadConstruccionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EdadConstruccion.label', default: 'EdadConstruccion'), edadConstruccionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def edadc = EdadConstruccion.get(params.idele)
        edadc.estatus = !edadc.estatus
        edadc.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'edadConstruccion.label', default: 'EdadConstruccion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
