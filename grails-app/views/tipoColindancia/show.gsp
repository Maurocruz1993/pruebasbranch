
<%@ page import="siteo.administracion.catalogos.TipoColindancia" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoColindancia.label', default: 'TipoColindancia')}" />
		<title><g:message code="Tipo colindancia" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipoColindancia" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipoColindancia" class="content scaffold-show" role="main">
			<h1><g:message code="Tipo colindancia" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipoColindancia">
			
				<g:if test="${tipoColindanciaInstance?.tipocolindancia}">
				<li class="fieldcontain">
					<span id="tipocolindancia-label" class="property-label"><g:message code="tipoColindancia.tipocolindancia.label" default="Tipo de colindancia" /></span>
					
						<span class="property-value" aria-labelledby="tipocolindancia-label"><g:fieldValue bean="${tipoColindanciaInstance}" field="tipocolindancia"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipoColindanciaInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="tipoColindancia.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${tipoColindanciaInstance?.estatus}" true="ACTIVO" false="BAJA" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tipoColindanciaInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tipoColindanciaInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
