package siteo.padron

import siteo.administracion.catalogos.TipoPredio
import siteo.administracion.catalogos.TipoSuelo
import siteo.administracion.catalogos.UnidadMedida
import siteo.administracion.catalogos.UsoSuelo
import siteo.seguridad.Delegacion
import siteo.seguridad.Municipio

class DatosDeclarados {

    Date fechacelebracion;
    Delegacion delegacion
    Municipio municipio
    String age_cve
    String sector_cve
    String manzana_cve
    String lote_cve
    String escritura
    String tomo
    TipoPredio tipopredio
    TipoSuelo tiposuelo
    UsoSuelo usosuelo
    BigDecimal valorDeclarado
    BigDecimal basecatAnt
    BigDecimal superficieTerreno = 0.00
    BigDecimal superficieComunTerreno = 0.00
    BigDecimal superficieConstruccion  = 0.00
    BigDecimal superficieComunConstruccion = 0.00
    UnidadMedida unidad
    Integer totalpropietarios
    Boolean peritoTerreno=false;
    Boolean peritoConstruccion=false;
    Boolean pasaquinta;
    
    static constraints = {
        delegacion(nullable:true, blank:true)
        municipio(nullable:true, blank:true)
        fechacelebracion(nullable:true)
        age_cve(blank:true, nullable:false,size:1..5)
        sector_cve(blank:true, nullable:false,size:1..5)
        manzana_cve(blank:true, nullable:false,size:1..5)
        lote_cve(blank:true, nullable:false,size:1..5)
        escritura(nullable:true,blank:true,size:1..45)
        tomo(nullable:true,blank:true,size:1..45)
        valorDeclarado(nullable: true, scale:2, min: 0.000)
        basecatAnt(nullable: true, scale:2, min: 0.000)
        superficieTerreno(nullable: true, scale:2, min: 0.000)
        superficieComunTerreno(nullable: true, scale:2, min: 0.000)
        superficieConstruccion(nullable: true, scale:2, min: 0.000)
        superficieComunConstruccion(nullable: true, scale:2, min: 0.000)
        totalpropietarios(nullable:true,min:0)
        tipopredio(nullable:true)
        tiposuelo(nullable:true)
        unidad(nullable:true)
        usosuelo(nullable:true)
        peritoTerreno(nullable:true);
        peritoConstruccion(nullable:true);
        pasaquinta(nullable:true);
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    Long guarda(datleg){
        Boolean ok = false
        int error=0;
        while (!ok){
            DatosDeclarados.withTransaction{
                status->
                try{
                    Long id = DatosDeclarados.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    datleg.id = id
                    datleg.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
