<%@ page import="siteo.seguridad.Roles" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'roles.label', default: 'Roles')}" />
		<title>Actualizar</title>
	</head>
	<body>
            <script language="javascript">
                    $(document).ready(function(){
                        $("#save").click(function(){
                            $("#numOperaciones").val($("#operacion option:selected").length)
                        });
                    });
                </script>
		<a href="#edit-roles" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}">Principal</a></li>
				<li><g:link class="list" action="index">Lista</g:link></li>
				<li><g:link class="create" action="create">Nuevo</g:link></li>
			</ul>
		</div>
		<div id="edit-roles" class="content scaffold-edit" role="main">
			<h1>Actualizar Rol</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${rolesInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${rolesInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:rolesInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${rolesInstance?.version}" />
				<fieldset class="form">
                                        <g:hiddenField name="numOperaciones" />
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit id="save" class="save" action="update" value="Actualizar" />
                                        
				</fieldset>
			</g:form>
                
		</div>
	</body>
</html>
