<%@ page import="siteo.administracion.catalogos.EstatusSolicitud" %>



<div class="fieldcontain ${hasErrors(bean: estatusSolicitudInstance, field: 'estatussolicitud', 'error')} required">
	<label for="estatussolicitud">
		<g:message code="estatusSolicitud.estatussolicitud.label" default="Estatus Solicitud" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="estatussolicitud" maxlength="45" required="" value="${estatusSolicitudInstance?.estatussolicitud}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: estatusSolicitudInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="estatusSolicitud.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${estatusSolicitudInstance?.estatus}" />

</div>

