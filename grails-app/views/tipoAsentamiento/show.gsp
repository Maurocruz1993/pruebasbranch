
<%@ page import="siteo.administracion.catalogos.TipoAsentamiento" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tipoAsentamiento.label', default: 'TipoAsentamiento')}" />
		<title><g:message code="Tipo de asentamiento" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-tipoAsentamiento" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="Lista" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="Nuevo" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-tipoAsentamiento" class="content scaffold-show" role="main">
			<h1><g:message code="Tipo de asentamiento" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list tipoAsentamiento">
			
				<g:if test="${tipoAsentamientoInstance?.tipoasentamiento}">
				<li class="fieldcontain">
					<span id="tipoasentamiento-label" class="property-label"><g:message code="tipoAsentamiento.tipoasentamiento.label" default="Tipoasentamiento" /></span>
					
						<span class="property-value" aria-labelledby="tipoasentamiento-label"><g:fieldValue bean="${tipoAsentamientoInstance}" field="tipoasentamiento"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${tipoAsentamientoInstance?.estatus}">
				<li class="fieldcontain">
					<span id="estatus-label" class="property-label"><g:message code="tipoAsentamiento.estatus.label" default="Estatus" /></span>
					
						<span class="property-value" aria-labelledby="estatus-label"><g:formatBoolean boolean="${tipoAsentamientoInstance?.estatus}" true="ACTIVO" false="BAJA"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:tipoAsentamientoInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tipoAsentamientoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
