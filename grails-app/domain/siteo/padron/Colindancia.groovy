package siteo.padron

import siteo.administracion.catalogos.TipoColindancia
import siteo.administracion.catalogos.Viento

class Colindancia {

    TipoColindancia tipocolindancia
    Viento viento
    BigDecimal medida = 0.000
    String descripcion
    String otroviento
    
    static belongsTo = [datosdeclarados: DatosDeclarados]
    
    static constraints = {
        tipocolindancia (nullable:false)
        viento (nullable:true)
        medida(nullable:true, scale:3, min: 0.0000)
        descripcion (nullable:false, blank:true,size:1..50000)
        otroviento (nullable:true, size:1..5, blank:true)
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(colin){
        Boolean ok = false
        int error=0;
        while (!ok){
            Colindancia.withTransaction{
                status->
                try{
                    Long id = Colindancia.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    colin.id = id
                    colin.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
