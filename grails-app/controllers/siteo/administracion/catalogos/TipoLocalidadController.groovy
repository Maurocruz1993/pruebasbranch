package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoLocalidadController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort="id"
        params.order="asc"
        respond TipoLocalidad.list(params), model:[tipoLocalidadInstanceCount: TipoLocalidad.count()]
    }

    def show(TipoLocalidad tipoLocalidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoLocalidadInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoLocalidad(params)
    }

    @Transactional
    def save(TipoLocalidad tipoLocalidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoLocalidadInstance == null) {
            notFound()
            return
        }

        if (tipoLocalidadInstance.hasErrors()) {
            respond tipoLocalidadInstance.errors, view:'create'
            return
        }

        tipoLocalidadInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoLocalidad.label', default: 'TipoLocalidad'), tipoLocalidadInstance.id])
                redirect tipoLocalidadInstance
            }
            '*' { respond tipoLocalidadInstance, [status: CREATED] }
        }
    }

    def edit(TipoLocalidad tipoLocalidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoLocalidadInstance
    }

    @Transactional
    def update(TipoLocalidad tipoLocalidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoLocalidadInstance == null) {
            notFound()
            return
        }

        if (tipoLocalidadInstance.hasErrors()) {
            respond tipoLocalidadInstance.errors, view:'edit'
            return
        }

        tipoLocalidadInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoLocalidad.label', default: 'TipoLocalidad'), tipoLocalidadInstance.id])
                redirect tipoLocalidadInstance
            }
            '*'{ respond tipoLocalidadInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoLocalidad tipoLocalidadInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoLocalidadInstance == null) {
            notFound()
            return
        }

        tipoLocalidadInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoLocalidad.label', default: 'TipoLocalidad'), tipoLocalidadInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoLoc = TipoLocalidad.get(params.idele)
        tipoLoc.estatus = !tipoLoc.estatus
        tipoLoc.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoLocalidad.label', default: 'TipoLocalidad'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
