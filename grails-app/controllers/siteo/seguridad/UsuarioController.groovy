package siteo.seguridad



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import siteo.administracion.catalogos.Notario

@Transactional(readOnly = true)
class UsuarioController {
    
    def usuarioService
    def validaSesionService
    def bitacoraService
    
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Transactional
    def login(){
        session.user = null
        session.random = new Random()
        if(params){
            
            if (!usuarioService.valida(params)){
                flash.message = "Usuario no existe o está dado de baja"
                redirect(controller:'login', action:'index')
                return;
            }else{
                def us = usuarioService.obtenerPorUsuarioYPass(params);  //Consulta el usuario mediante el login y contraseña enviados
                def totRol = usuarioService.totalRoles(us)
                def totNot = usuarioService.totalNotario(us);    //Consulta el total de roles activos que tiene el usuario
                session.setAttribute("usuario", us);
                session.user = us.id
                if(!us.titulo){
                    session.nombrecompleto = us.nombre
                }else{
                    session.nombrecompleto = us.titulo + " " + us.nombre
                }
                session.nombrenot = null;
                session.nombrerol = null;
                session.setAttribute("numNot",totNot);
                session.setAttribute("numRol",totRol)
                bitacoraService.registraActividad(us, "ACCESO AL SISTEMA", null);
                if(totRol ==1){
                    def r = usuarioService.rolActivo(us)
                    def rolActual = usuarioService.nombreRol(r)
                    if(rolActual == "ADMINISTRADOR"){
                        session.nombrerol = rolActual
                        session.setAttribute("menu", usuarioService.menu(r))
                        session.setAttribute("idrol", r)
                        redirect(controller:'menu', action:'index')
                    }                        
                    else if(totNot ==1){
                        def notActual = usuarioService.notarioActivo(us);
                        def nomNot = usuarioService.nombreNotario(notActual);
                        session.nombrerol = rolActual
                        session.nombrenot = nomNot
                        session.auxidnot = notActual
                        session.setAttribute("menu", usuarioService.menu(r))
                        session.setAttribute("idrol", r)
                        redirect(controller:'menu', action:'index')
                    }else if (totNot >1){
                        def nomRol = usuarioService.nombreRol(r)
                        session.nombrerol = nomRol
                        session.setAttribute("idrol", r)
                        redirect(controller:'usuario', action:'seleccionaNot',id:us.id)
                        
                    }
                }else if(totRol > 1){
                    flash.message= "Seleccione el rol a utilizar"
                    redirect(controller:'usuario', action:'seleccionaRol', id:us.id)
                }else{
                    flash.message="No tiene asociados roles"
                    redirect(controller:'usuario',action:'logout')
                }
            }
        }else{
            redirect(controller:"login",action:"index");
        }
    }
    
    def logout = {
        session.invalidate()
        redirect(controller:'login', action: "index")
    }
    
    def seleccionaRol(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        session.nombrerol = null
        session.nombrenot = null;
        def user = Usuario.findById(params.id)
        def rol = user.roles.id
        def rRol = Roles.findAllByEstatusAndIdInList("true",rol, [sort:'rol'])
        [user:user,roles:rRol]
    }
    
    def rolSelec(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        def r = Roles.findById(params.rol)
        session.idrol = r.id
        def nomRol = usuarioService.nombreRol(r.id)
        def us = Usuario.findById(session.user)
        def totNot = usuarioService.totalNotario(us); 
        session.nombrerol = nomRol
        if(nomRol == "ADMINISTRADOR"){
            session.setAttribute("menu", usuarioService.menu(r.id))
            redirect(controller:'menu', action:'index')
        }else{
            if(totNot==1){
                def notActual = usuarioService.notarioActivo(us);
                def nomNot = usuarioService.nombreNotario(notActual);
                session.nombrenot = nomNot
                session.auxidnot=notActual
                session.setAttribute("menu", usuarioService.menu(r.id))
                redirect(controller:'menu', action:'index')
            }else if(totNot > 1){
                redirect(controller:'usuario', action:'seleccionaNot', id:us.id)
            }
        }
    }
    def seleccionaNot(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        session.nombrenot = null
        def user = Usuario.findById(params.id)
        def not = user.notarias.id
        def rNot = Notario.findAllByEstatusAndIdInList("true",not, [sort:'notaria'])
        [user:user,roles:rNot]
    }
    
    def notSelec(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return
        }
        
        def n = Notario.findById(params.nota)
        def nomNot = usuarioService.nombreNotario(n.id)
        session.nombrenot = nomNot
        session.auxidnot = n.id
        session.setAttribute("menu", usuarioService.menu(session.idrol))
        redirect(controller:'menu', action:'index')
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        def instancia = Usuario.get(params.idele)
        instancia.estatus = !instancia.estatus
        instancia.save flush:true
        
        redirect(action: 'index')
    }
    
    def passwd(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
    }
    @Transactional
    def modificapwd(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        def x = usuarioService.cambiaPass(params, session)
        flash.message= x
        if(x == "Contraseña modificada correctamente"){
            def usActual = Usuario.findById(session.user)
            bitacoraService.registraActividad(usActual, "MODIFICA CONTRASEÑA", session.user)
        }
        redirect(action:"passwd")
    }
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        params.max = Math.min(max ?: 10, 100)
        params.sort = "nombre"
        params.order ="asc"
        respond Usuario.list(params), model:[usuarioInstanceCount: Usuario.count()]
    }

    def show(Usuario usuarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        respond usuarioInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        respond new Usuario(params)
    }

    @Transactional
    def save(Usuario usuarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        if (usuarioInstance == null) {
            notFound()
            return
        }

        if (usuarioInstance.hasErrors()) {
            respond usuarioInstance.errors, view:'create'
            return
        }

        usuarioInstance.passwd = usuarioInstance.passwd.encodeAsSHA1()
        usuarioInstance.save flush:true
        
        def usActual = Usuario.findById(session.user)
        bitacoraService.registraActividad(usActual, "CREA USUARIO", usuarioInstance.id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'usuario.label', default: 'Usuario'), usuarioInstance.id])
                redirect usuarioInstance
            }
            '*' { respond usuarioInstance, [status: CREATED] }
        }
    }

    def edit(Usuario usuarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        respond usuarioInstance
    }

    @Transactional
    def update(Usuario usuarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        if (usuarioInstance == null) {
            notFound()
            return
        }

        if (usuarioInstance.hasErrors()) {
            respond usuarioInstance.errors, view:'edit'
            return
        }
        
        usuarioInstance.save flush:true
        
        def usActual = Usuario.findById(session.user)
        bitacoraService.registraActividad(usActual, "MODIFICA USUARIO", usuarioInstance.id)
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Usuario.label', default: 'Usuario'), usuarioInstance.id])
                redirect usuarioInstance
            }
            '*'{ respond usuarioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Usuario usuarioInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        if (usuarioInstance == null) {
            notFound()
            return
        }

        usuarioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Usuario.label', default: 'Usuario'), usuarioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def buscar(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        try{
            def u = Usuario.createCriteria()
            def r2 = u.list {
                ilike("nombre", params.criterio+"%")
            }
            if(!r2){
                //System.out.println(params.criterio)
            }
            [usuarioInstanceList:r2]
        }catch(Exception e){
            flash.message = e.message
        }
    }
    @Transactional
    def resetPass(){
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
        }
        if(!validaSesionService.compruebaRol(session,"ADMINISTRADOR")){
            redirect(controller:'menu', action:'index')
        }
        def usu = Usuario.findById(params.usuario)
        def pass = params.pass.encodeAsSHA1()
        usu.passwd = pass
        usu.save(flush:true)
        return true
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'usuario.label', default: 'Usuario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
