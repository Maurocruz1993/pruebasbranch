package siteo.administracion.catalogos



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TipoTenenciaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def validaSesionService;
    
    def index(Integer max) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        params.sort="id"
        params.order="asc"
        params.max = Math.min(max ?: 10, 100)
        respond TipoTenencia.list(params), model:[tipoTenenciaInstanceCount: TipoTenencia.count()]
    }

    def show(TipoTenencia tipoTenenciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoTenenciaInstance
    }

    def create() {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond new TipoTenencia(params)
    }

    @Transactional
    def save(TipoTenencia tipoTenenciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoTenenciaInstance == null) {
            notFound()
            return
        }

        if (tipoTenenciaInstance.hasErrors()) {
            respond tipoTenenciaInstance.errors, view:'create'
            return
        }

        tipoTenenciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoTenencia.label', default: 'TipoTenencia'), tipoTenenciaInstance.id])
                redirect tipoTenenciaInstance
            }
            '*' { respond tipoTenenciaInstance, [status: CREATED] }
        }
    }

    def edit(TipoTenencia tipoTenenciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        respond tipoTenenciaInstance
    }

    @Transactional
    def update(TipoTenencia tipoTenenciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoTenenciaInstance == null) {
            notFound()
            return
        }

        if (tipoTenenciaInstance.hasErrors()) {
            respond tipoTenenciaInstance.errors, view:'edit'
            return
        }

        tipoTenenciaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TipoTenencia.label', default: 'TipoTenencia'), tipoTenenciaInstance.id])
                redirect tipoTenenciaInstance
            }
            '*'{ respond tipoTenenciaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TipoTenencia tipoTenenciaInstance) {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        if (tipoTenenciaInstance == null) {
            notFound()
            return
        }

        tipoTenenciaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TipoTenencia.label', default: 'TipoTenencia'), tipoTenenciaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
    
    def cambiaEstatus = {
        if (!validaSesionService.sesionActiva(session)){
            redirect(controller:'Usuario', action:'logout')
            return;
        }
        def tipoTen = TipoTenencia.get(params.idele)
        tipoTen.estatus = !tipoTen.estatus
        tipoTen.save flush:true
        
        redirect(action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoTenencia.label', default: 'TipoTenencia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
