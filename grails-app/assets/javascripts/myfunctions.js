/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Bloquea(baseUrl,mensaje){
    $.blockUI({message: '<h1><img src="'+baseUrl+'/assets/loading.gif" width="30"/> '+mensaje+'...</h1>'});
}

function Desbloquea(){
    $.unblockUI();
}

function Mayusculas(objeto){
    objeto.value = objeto.value.toUpperCase();
    objeto.value = objeto.value.trim();
}

function ValidaNums(e){
    var key = window.event ? e.keyCode : e.which;
    var keychar = String.fromCharCode(key);
    reg = /[\d]/;
    return reg.test(keychar);
}

function ValidaCuenta(e, valor){
    var key = window.event ? e.keyCode : e.which;
    var keychar = String.fromCharCode(key);
    
    if(e.keyCode==8)
        return true;
    
    if (valor.length == 0 && keychar == '*')
        return false;
    
    var asteris = valor.indexOf('*');
    if (asteris!=-1)
        return false;
    
    reg = /[\d*]/;
    return reg.test(keychar);    
}

function marcatotalparcial(baseUrl,idmov){
    $.ajax({
        url:baseUrl+"/solicitudNotario/validatotalparcial",
        type:"GET",
        data:"idmov="+idmov,
        success: function (respuesta, textStatus, jqXHR) {
            marcaradio(respuesta);
        },complete: function (jqXHR, textStatus) {}
    });
}

function soloNumeros(event){
    
    if (event.keyCode == 46 || event.keyCode == 8 || (event.keyCode>=37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode==13){
        
    }else {
        if (event.keyCode < 95) {

            if (event.keyCode < 48 || event.keyCode > 57 ) {
                event.preventDefault();
            }
        } 
        else {
            if (event.keyCode < 96 || event.keyCode > 105) {
                event.preventDefault();
            }
        }
    }
}

function soloNumerosDec(event) {
    if (event.keyCode === 190 || event.keyCode == 46 || event.keyCode === 110 || event.keyCode == 8 || (event.keyCode >= 37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode == 13) {

    } else {
        if (event.keyCode < 95) {
            if (event.shiftKey == 1) {//---si se presiona la tecla shift
                if (event.keyCode > 48 || event.keyCode < 57) {
                    event.preventDefault();
                }
            } else {
                if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
                }
            }
        }
        else {
            if (event.keyCode < 96 || event.keyCode > 105) {
                event.preventDefault();
            }
        }
    }
}

function combinacionTeclas(event,opcion){
    switch(parseInt(opcion)){
        case 1://---solo letras y números
            if(event.keyCode >=65 && event.keyCode<=90){
                
            }else{
                if((event.keyCode>=96 && event.keyCode<=105) || (event.keyCode>=48 && event.keyCode<=57)){
                    
                }else{
                    if (event.keyCode == 190 || event.keyCode == 46  || event.keyCode == 8 || (event.keyCode>=37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode==13 || event.keyCode == 192){
                        
                    }else {
                        event.preventDefault();
                    }
                }
            }
        break;
        case 2://---solo letras,números y guión
            if(event.keyCode==13 || (event.keyCode >=65 && event.keyCode<=90)||(event.keyCode>=48 && event.keyCode<=57 || event.keyCode == 192)|| event.keyCode==189){
                if(event.shiftKey==1){
                    if(event.keyCode>=48 && event.keyCode<=57){
                        event.preventDefault();
                    }
                }
            }else{
                if(event.keyCode>=96 && event.keyCode<=105){

                }else{
                    if (event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || (event.keyCode>=37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode==173 || event.keyCode==109 || event.keyCode == 192){
                        
                    }else {
                        event.preventDefault();
                    }
                }
            }
        break;
        case 3://---letras, números, simbolos(' , - " % & *)
            if(event.keyCode >=65 && event.keyCode<=90){

            }else{
                if(event.keyCode>=96 && event.keyCode<=105){

                }else{
                    if (event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || (event.keyCode>=37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode==13  || event.keyCode==109 || event.keyCode==188 || event.keyCode==32 || event.keyCode==222 || event.keyCode == 192){

                    }else {
                        if(event.shiftKey==1){//---si se presiona la tecla shift
                            if(event.keyCode==50 || event.keyCode==53 || event.keyCode==54 || event.keyCode==55 || event.keyCode==56 || event.keyCode==57 ){

                            }else{
                                event.preventDefault();
                            }
                        }else{
                            if(event.keyCode==111 || event.keyCode==106 || event.keyCode==109 || event.keyCode==110 || event.keyCode==190 || event.keyCode==173 || (event.keyCode >=48 && event.keyCode<=57)){

                            }else{
                                event.preventDefault();
                            }
                        }
                    }
                }
            }
        break;
        case 4://---solo letras y espacio
            if((event.keyCode >=65 && event.keyCode<=90) || event.keyCode==32 || event.keyCode==8 || event.keyCode == 9 || event.keyCode==46 || (event.keyCode>=37 && event.keyCode<=40) || event.keyCode==13 || event.keyCode == 192){

            }else{
                event.preventDefault();
            }
        break;
        case 5://---letras y diagonal
            if (event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 111 || event.keyCode == 8 || (event.keyCode>=37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode==13 || event.keyCode == 192){

            }else {
                if (event.keyCode < 95) {

                    if (event.keyCode < 48 || event.keyCode > 57 ) {
                        event.preventDefault();
                    }
                } 
                else {
                    if (event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            }
        break;
        case 6://---números y guion
            if (event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 109 || event.keyCode == 8 || (event.keyCode>=37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode==13){
                
            }else {
                if (event.keyCode < 95) {

                    if (event.keyCode < 48 || event.keyCode > 57 ) {
                        event.preventDefault();
                    }
                } 
                else {
                    if (event.keyCode < 96 || event.keyCode > 105) {
                        if(event.keyCode==173){
                            if(event.shiftKey==1){
                                event.preventDefault();
                            }
                        }else{
                            event.preventDefault();
                        }
                    }
                }
            }
        break;
        case 7://---números,letras,símbolos (* - .)
            if(event.keyCode >=65 && event.keyCode<=90){

            }else{
                if((event.keyCode>=48 && event.keyCode<=57)||(event.keyCode>=96 && event.keyCode<=105)){

                }else{
                    if (event.keyCode == 46 || event.keyCode == 8 || (event.keyCode>=37 && event.keyCode <= 40) || event.keyCode == 9 || event.keyCode==109 || event.keyCode==32 || event.keyCode==106 || event.keyCode==110 || event.keyCode == 192){
                        
                    }else {
                        if(event.shiftKey==1){//---si se presiona la tecla shift
                            if(event.keyCode==54 || event.keyCode==171 ){
                    
                            }else{
                                event.preventDefault();
                            }
                        }else{
                            if(event.keyCode==190 || event.keyCode==173){
                    
                            }else{
                                event.preventDefault();
                            }
                        }
                    }
                }
            }
        break;
        case 8://---solo letrás y espacio
            if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || (event.keyCode >= 65 && event.keyCode <= 90)) {

            } else {
                event.preventDefault();
            }
            break
        case 9://---solo letras y espacio
            if ((event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode == 43 || event.keyCode == 60 || (event.keyCode >= 219 && event.keyCode <= 222) || (event.keyCode >= 188 && event.keyCode <= 191) || event.keyCode == 226 || event.keyCode == 111) {
                event.preventDefault();
            } else {
                if (event.keyCode == 106 || event.key == 'Ñ' || event.key == 'ñ' || event.keyCode == 192 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 32 || event.keyCode == 16 || (event.keyCode >= 65 && event.keyCode <= 90)) {
                } else {
                    if (event.shiftKey == 1) {//---si se presiona la tecla shift

                        if (event.keyCode == 187) {

                        } else {
                            event.preventDefault();
                        }
                    } else {
                        event.preventDefault();
                    }

                    //event.preventDefault();
                }
            }
            break;
        default:break;
    }
}

function comboAutocompletar(objeto){
    (function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Mostrar todos los registros" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", "Registro no encontrado")
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.autocomplete( "instance" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );
 
  $(function() {
    $( objeto ).combobox();
  });
}

function observaciones(baseUrl,id){
    Bloquea(baseUrl,"Espere un momento...");
    $.ajax({
        url:baseUrl+"/solicitudNotario/verObservaciones",
        type:"GET",
        data:"id="+id,
        success: function (respuesta, textStatus, jqXHR) {
            $("#muestra-observaciones").html(respuesta).show();
            $("#formObservaciones").dialog("open");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            MostrarMensaje("Error: "+errorThrown);
        },
        complete: function (jqXHR, textStatus) {
            Desbloquea();
        }
    });
}

function viento(baseUrl,id_dat,nuevo,idsol){
    var auxiddat="0",auxidsol="0";
    if(id_dat){
        auxiddat=id_dat;
    }
    if(idsol){
        auxidsol=idsol;
    }
    Bloquea(baseUrl,"Espere un momento..");
    $.get(baseUrl+"/solicitudNotario/viento",{idDat:auxiddat,nuevo:nuevo,idsol:auxidsol},function(respuesta){
        $("#agrega-medidas").html(respuesta).show();
        $("#formViento").dialog("open");
    }).always(function(){
        Desbloquea();
    });
}

function coprop(baseUrl,idsol,id_dc,idprop){
    Bloquea(baseUrl,"Espere un momento...");
    var datos={idsol:idsol,id_dc:id_dc,idprop:idprop};
    $.ajax({
        url:baseUrl+"/solicitudNotario/vercoprop",
        type:"GET",
        data:datos,
        success: function (respuesta, textStatus, jqXHR) {
            $("#agrega-copropietario").html(respuesta).show();
            $("#datosProp").dialog("open");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            MostrarMensaje("Error: "+errorThrown);
        },
        complete: function (jqXHR, textStatus) {
            Desbloquea();
        }
    });
}

function verenaje(baseUrl, idsol, id_dc, idenaje) {
    Bloquea(baseUrl, "Espere un momento...");
    var datos = {idsol: idsol, id_dc: id_dc, idenaje: idenaje};
    $.ajax({
        url: baseUrl + "/solicitudNotario/verenajenante",
        type: "GET",
        data: datos,
        success: function(respuesta, textStatus, jqXHR) {
            $("#agrega-enajenante").html(respuesta).show();
            $("#datosEnajenante").dialog("open");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            MostrarMensaje("Error: " + errorThrown);
        },
        complete: function(jqXHR, textStatus) {
            Desbloquea();
        }
    });
}
function editaCopro(baseUrl,id,id_dat,auxidp,idprop){
    Bloquea(baseUrl,"Espere un momento");
    $.get(baseUrl+"/solicitudNotario/editCop",{idcop:id,idDat:id_dat,auxidp:auxidp,idprop:idprop},function(respuesta){
       $("#edita-cop").html(respuesta).show();
       $("#editCop").dialog("open");
    }).error(function(jqXHR, textStatus,errorThrown) {
        MostrarMensaje("Error: "+errorThrown);
    }).always(function(){
        Desbloquea();
    });
}

function editaEnajenante(baseUrl, id, id_dat, auxidp, idenajes) {
    Bloquea(baseUrl, "Espere un momento");
    $.get(baseUrl + "/solicitudNotario/editEna", {idenaje: id, idDat: id_dat, auxidp: auxidp, idena: idenajes}, function(respuesta) {
        $("#edita-enaje").html(respuesta).show();
        $("#editEna").dialog("open");
    }).error(function(jqXHR, textStatus, errorThrown) {
        MostrarMensaje("Error: " + errorThrown);
    }).always(function() {
        Desbloquea();
    });
}
function validarDomicilioNotificacion() {
    if ($("#lstTipoVialnot").val() === "0" || $("#lstTipoVialnot").val() === "") {
        MostrarMensaje("Elige un tipo de vialidad");
        return false;
    }
    if ($("#txtNomDomnot").val() === "") {
        MostrarMensaje("Escriba el nombre de la vialidad");
        $("#txtNomDomnot").focus();
        return false;
    }
    if ($("#lstTipoAsentnot").val() === "0" || $("#lstTipoAsentnot").val() === "") {
        MostrarMensaje("Elige el tipo de asentamiento");
        return false;
    }
    if ($("#txtDescripcionnot").val() === "") {
        MostrarMensaje("Escriba la descripcion del asentamiento");
        $("#txtDescripcionnot").focus();
        return false;
    }
    if ($("#lstTipoLocnot").val() === "0" || $("#lstTipoLocnot").val() === "") {
        MostrarMensaje("Elige el tipo de localidad");
        return false;
    }
    if ($("#txtDescripcionLocnot").val() === "") {
        MostrarMensaje("Escriba la descripcion de la localidad");
        $("#txtDescripcionLocnot").focus();
        return false;
    }
     if ($("#txtNoExtnot").val() === "") {
        MostrarMensaje("El numero exterior es requerido");
        $("#txtNoExtnot").focus();
        return false;
    }
    if ($("#lstEstadonot").val() === "0" || $("#lstEstadonot").val() === "") {
        MostrarMensaje("Elige un estado");
        if ($("#lstEstadonot").val() === "20") {
            if ($("#lstMunicipioDfnot").val() === "0" || $("#lstMunicipioDfnot").val() === "") {
                MostrarMensaje("Elige un minicipio");
                return false;
            }
        } else if ($("#lstEstadonot").val() !== "20") {
            if ($("#txtOtroMunnot").val() === "") {
                MostrarMensaje("Escribe el nombre del municipio");
                $("#txtOtroMunnot").focus();
                return false;
            }
        }
    }
    return true;
}
function guardaDomNot(baseUrl, idsol, idDat, idDomf, idDomnot) {
    var auxiddom = "0", auxiddat = "0", auxiddomn = "0";
    if (idDomf) {
        auxiddom = idDomf;
    }
    if (idDat) {
        auxiddat = idDat;
    }
    if (idDomnot) {
        auxiddomn = idDomnot;
    }
    var vial = $("#lstTipoVialnot").val();
    var otnvial= $("#txtNotOtroVialidad").val();
    var nombde = $("#txtNomDomnot").val();
    var ext = $("#txtNoExtnot").val();
    var inte = $("#txtNoIntnot").val();
    var asent = $("#lstTipoAsentnot").val();
    var otnasent=$("#txtNotOtroAsentamiento").val();
    var desc = $("#txtDescripcionnot").val();
    var tiploc = $("#lstTipoLocnot").val();
    var otnloc=$("#txtNotOtraLocalidad").val();
    var descLoc = $("#txtDescripcionLocnot").val();
    var estado = $("#lstEstadonot").val();
    var otromun = $("#txtOtroMunnot").val();
    var mun = $("#lstMunicipioDfnot").val();
    var datos = {iddom: auxiddom, idsolicitud: idsol, idDat: auxiddat, vial: vial,otnvial:otnvial,otnasent:otnasent,otnloc:otnloc, nombde: nombde, ext: ext, inte: inte, asent: asent, desc: desc, tiploc: tiploc,
        descLoc: descLoc, estado: estado, otromun: otromun, mun: mun, idDomn: auxiddomn};
    if (validarDomicilioNotificacion()) {
        Bloquea(baseUrl, "Guadando datos..");
        $.ajax({
            url: baseUrl + "/solicitudNotario/updateDomNoti",
            type: "POST",
            data: datos,
            success: function(respuesta, textStatus, jqXHR) {
                //---window.location.href=baseUrl+"/solicitudNotario/capturaDatos/"+respuesta+"?tab=3";
                $(function() {
                    $("#tabs").tabs({
                        active: '3'
                    });
                });
                MostrarMensaje("Datos guardados correctamente");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                MostrarMensaje("Error: " + errorThrown);
            },
            complete: function(jqXHR, textStatus) {
                Desbloquea();
                bloqueaDomNot();
                muestraBotones();
            }
        });
    }
}

function bloqueaDomNot(){
    var auxtipvnot=$("#lstTipoVialnot").val();
    var auxtipvnot1=$("#lsttipvialnot input").val();
    var auxnombdenot=$("#txtNomDomnot").val();
    var auxnextdfnot=$("#txtNoExtnot").val();
    var auxnintdfnot=$("#txtNoIntnot").val();
    var auxtipasdfnot=$("#lstTipoAsentnot").val();
    var auxtipasdfnot1=$("#lsttipastnot input").val();
    var descdfnot=$("#txtDescripcionnot").val();
    var auxtiplocdfnot=$("#lstTipoLoc").val();
    var auxtiplocdfnot1=$("#lstlocnot input").val();
    var deslocdfnot=$("#txtDescripcionLocnot").val();
    var entnot=$("#lstEstadonot").val();
    var entnot1=$("#lstentnot input").val();
    $("#btnCancelaDnot").hide();
    $("#btnGuardaDnot").hide();
    $("#btnEditaDnot").show();
    $("#auxtipvialnot").show();
    $("#lsttipvialnot").hide();
    $("#lstTipoVialnot").val(auxtipvnot);
    $("#lsttipvialnot input").val(auxtipvnot1);
    $("#txtNomDomnot").attr('readonly',true);
    $("#txtNomDomnot").val(auxnombdenot);
    $("#txtNoExtnot").attr('readonly',true);
    $("#txtNoExtnot").val(auxnextdfnot);
    $("#txtNoIntnot").attr('readonly',true);
    $("#txtNoIntnot").val(auxnintdfnot);
    $("#auxtipastnot").show();
    $("#lsttipastnot").hide();
    $("#lstTipoAsentnot").val(auxtipasdfnot);
    $("#lsttipastnot input").val(auxtipasdfnot1);
    $("#txtDescripcionnot").attr('readonly',true);
    $("#txtDescripcionnot").val(descdfnot);
    $("#tiplocnot").show();
    $("#lstlocnot").hide();
    $("#lstTipoLocnot").val(auxtiplocdfnot);
    $("#lstlocnot input").val(auxtiplocdfnot1);
    $("#txtDescripcionLocnot").attr('readonly',true);
    $("#txtDescripcionLocnot").val(deslocdfnot);
    $("#entnot").show();
    $("#lstentnot").hide();
    $("#lstEstadonot").val(entnot);
    $("#lstentnot input").val(entnot1);
    validaEntidadNot(entnot);
    $("#muninot").show();
    $("#lstmunidfnot").hide();
    $("#txtOtroMunnot").hide();
}

function escribeId(idesc,objeto){
    if(idesc){
        $(objeto).val(idesc);
    }
}

function obtieneTotParc(baseUrl,idmov,inicio,fracc){
    $.ajax({
        url:baseUrl+"/solicitudNotario/getTotalParcial",
        type:"GET",
        data:"idmov="+idmov,
        success:function(respuesta){
            actualizaTotalParcial(respuesta,inicio,fracc);
        },error: function (jqXHR, textStatus, errorThrown) {
            MostrarMensaje("Error: "+errorThrown);
        },complete: function (jqXHR, textStatus) {}
    });
}

function actualizaTotalParcial(data,inicio,fracc){
    if(data.id){
        if(Boolean(data.esparcial)){
            $("#opcionTipoMov").val("2");
            $("#lsttipomov2 input").val("PARCIAL");
            $("#esTotPar").text("PARCIAL");
            $("#lsttipomov2").hide();
            $("#lsttipomov1").show();
        }
        if(Boolean(data.estotal)){
            $("#opcionTipoMov").val("1");
            $("#lsttipomov2 input").val("TOTAL");
            $("#esTotPar").text("TOTAL");
            $("#lsttipomov2").hide();
            $("#lsttipomov1").show();
        }

        if(!Boolean(data.esparcial) && !Boolean(data.estotal)){
            $("#lsttipomov1").hide();
            $("#lsttipomov2").show();
        }

        if(Boolean(data.cuentarequerida) && !Boolean(data.cuentaopcional)){
            if(!fracc){
                $("#txtCuenta").removeAttr("readonly",false);
                if($("#txtCuenta").val()=="SIN CUENTA"){
                    $("#txtCuenta").val("")
                }
            }
        }

        if(Boolean(data.cuentaopcional)){
            if(!fracc){
                $("#txtCuenta").removeAttr("readonly",false);
                if(!$("#txtCuenta").val()){
                    $("#txtCuenta").val("SIN CUENTA");
                }
            }
        }

        if(!Boolean(data.cuentarequerida) && !Boolean(data.cuentaopcional)){
            $("#txtCuenta").attr("readonly",true);
            $("#txtCuenta").val("SIN CUENTA");
        }

        if(inicio){
            $("#lsttipomov1").show();
            $("#lsttipomov2").hide();
        }
    }
}

function simulaTab(objetos){
    objetos.keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var nextInput = objetos.get(objetos.index(this) + 1);
            
            if (nextInput) {
                nextInput.focus();
            }
        }
   });
}

function depliegamensaje(){
    setTimeout(function(){ $('#mensajes').fadeIn(500).fadeOut(500).fadeIn(500);}, 500);
}

function otromensaje(div){
    setTimeout(function(){ $(div).fadeIn(500).fadeOut(500).fadeIn(500);}, 500);
}

function muestradetalle(baseUrl,idsol){
    Bloquea("Espere un momento");
    $.ajax({
        url:baseUrl+"/solicitudNotario/detallesolicitud",
        type: "GET",
        data:"id="+idsol,
        success: function (respuesta, textStatus, jqXHR) {
            $("#detallesolnot").html(respuesta).show();
            $("#detalle-sol").dialog("open");
        },error: function (jqXHR, textStatus, errorThrown) {
            MostrarMensaje("Error: "+errorThrown);
        },complete: function (jqXHR, textStatus) {
            Desbloquea();
        }
    });
}