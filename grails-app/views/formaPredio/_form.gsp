<%@ page import="siteo.administracion.catalogos.FormaPredio" %>



<div class="fieldcontain ${hasErrors(bean: formaPredioInstance, field: 'formaPredio', 'error')} required">
	<label for="formaPredio">
		<g:message code="formaPredio.formaPredio.label" default="Forma Predio" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="formaPredio" maxlength="20" required="" value="${formaPredioInstance?.formaPredio}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: formaPredioInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="formaPredio.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${formaPredioInstance?.estatus}" />

</div>

