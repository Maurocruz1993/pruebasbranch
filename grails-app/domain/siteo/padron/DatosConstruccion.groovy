package siteo.padron

import siteo.administracion.catalogos.EdadConstruccion
import siteo.administracion.catalogos.EstadoConservacion
import siteo.administracion.catalogos.NumeroNiveles
import siteo.administracion.catalogos.TipoConstruccion

class DatosConstruccion {
    
    BigDecimal superficieconstruccion
    TipoConstruccion tipoconstruccion
    NumeroNiveles nivel
    Integer subniveles
    String actividad
    EstadoConservacion conservacion
    EdadConstruccion edad
    BigDecimal indiviso
    
    static belongsTo=[datosDeclarados:DatosDeclarados]

    static constraints = {
        subniveles(nullable: true, size:1..100,blank:true)
        actividad(nullable:true, size:1..100, blank:true)
        superficieconstruccion(nullable: true, scale:2, min: 0.000)
        indiviso(nullable: true, scale:2, min: 0.000)
        edad(nullable:true)
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(dc){
        Boolean ok = false
        int error=0;
        while (!ok){
            DatosConstruccion.withTransaction{
                status->
                try{
                    Long id = DatosConstruccion.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    dc.id = id
                    dc.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
}
