package siteo.padron

import siteo.administracion.catalogos.TipoPersona

class Enajenante {

    TipoPersona tipopersona;
    String nombre;
    String paterno
    String materno
    String razonsocial
    String rfc;
    String ycop
    Boolean principal=false
    
    static belongsTo = [datosdeclarados: DatosDeclarados]
    
    static constraints = {
        rfc (nullable:true, size:1..15, blank:true);
        nombre(nullable:true, size:1..150, blank:true);
        paterno(nullable:true, size:1..150, blank:true)
        materno(nullable:true, size:1..150, blank:true)
        razonsocial(nullable:true, size:1..250, blank:true)
        tipopersona(nullable:true);
        ycop(nullable:true, size:1..5, blank:true)
        principal(nullable:true)
    }
    
    static mapping = {
        id generator: 'assigned'
    }
    
    Long guarda(enajenante){
        Boolean ok = false
        int error=0;
        while (!ok){
            Enajenante.withTransaction{
                status->
                try{
                    Long id = Enajenante.createCriteria().get{projections {max "id"}}
                    if(!id){
                        id=0;
                    }
                    id++
                    enajenante.id = id
                    enajenante.save(flush:true, failOnError: true)
                    ok = true
                    error=0;
                } catch (Exception e){
                    println "Error:--->${e.getMessage()}"
                    status.setRollbackOnly()
                    error=1;
                }
            }
            if(error==1){
                break;
            }
        }
        id
    }
    
    String toString(){
        return nombre;
    }
    
     String nombrecompletoporapellidos(){
        String fullname = ""
        if (paterno && !paterno.equals(""))
        fullname += paterno
        if (materno && !materno.equals(""))
        fullname +=" "+materno
        if (nombre && !nombre.equals(""))
        fullname +=" "+nombre
        
        fullname
    }
}
