<%@ page import="siteo.administracion.catalogos.MovimientoCatastral" %>



<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'clave', 'error')} required">
	<label for="clave">
		<g:message code="movimientoCatastral.clave.label" default="Clave" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="clave" type="number" value="${movimientoCatastralInstance.clave}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'movimiento', 'error')} required">
	<label for="movimiento">
		<g:message code="movimientoCatastral.movimiento.label" default="Movimiento" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="movimiento" maxlength="150" required="" value="${movimientoCatastralInstance?.movimiento}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'cuentaopcional', 'error')} ">
	<label for="cuentaopcional">
		<g:message code="movimientoCatastral.cuentaopcional.label" default="Cuentaopcional" />
		
	</label>
	<g:checkBox name="cuentaopcional" value="${movimientoCatastralInstance?.cuentaopcional}" />

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'cuentarequerida', 'error')} ">
	<label for="cuentarequerida">
		<g:message code="movimientoCatastral.cuentarequerida.label" default="Cuentarequerida" />
		
	</label>
	<g:checkBox name="cuentarequerida" value="${movimientoCatastralInstance?.cuentarequerida}" />

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'esparcial', 'error')} ">
	<label for="esparcial">
		<g:message code="movimientoCatastral.esparcial.label" default="Esparcial" />
		
	</label>
	<g:checkBox name="esparcial" value="${movimientoCatastralInstance?.esparcial}" />

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="movimientoCatastral.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${movimientoCatastralInstance?.estatus}" />

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'estotal', 'error')} ">
	<label for="estotal">
		<g:message code="movimientoCatastral.estotal.label" default="Estotal" />
		
	</label>
	<g:checkBox name="estotal" value="${movimientoCatastralInstance?.estotal}" />

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'generacuenta', 'error')} ">
	<label for="generacuenta">
		<g:message code="movimientoCatastral.generacuenta.label" default="Generacuenta" />
		
	</label>
	<g:checkBox name="generacuenta" value="${movimientoCatastralInstance?.generacuenta}" />

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'pagorequerido', 'error')} ">
	<label for="pagorequerido">
		<g:message code="movimientoCatastral.pagorequerido.label" default="Pagorequerido" />
		
	</label>
	<g:checkBox name="pagorequerido" value="${movimientoCatastralInstance?.pagorequerido}" />

</div>

<div class="fieldcontain ${hasErrors(bean: movimientoCatastralInstance, field: 'tipotramite', 'error')} required">
	<label for="tipotramite">
		<g:message code="movimientoCatastral.tipotramite.label" default="Tipotramite" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tipotramite" name="tipotramite.id" from="${siteo.administracion.catalogos.TipoTramite.list()}" optionKey="id" required="" value="${movimientoCatastralInstance?.tipotramite?.id}" class="many-to-one"/>

</div>

