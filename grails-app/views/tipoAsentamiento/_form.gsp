<%@ page import="siteo.administracion.catalogos.TipoAsentamiento" %>



<div class="fieldcontain ${hasErrors(bean: tipoAsentamientoInstance, field: 'tipoasentamiento', 'error')} required">
	<label for="tipoasentamiento">
		<g:message code="tipoAsentamiento.tipoasentamiento.label" default="Tipoasentamiento" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipoasentamiento" maxlength="25" required="" value="${tipoAsentamientoInstance?.tipoasentamiento}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoAsentamientoInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoAsentamiento.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoAsentamientoInstance?.estatus}" />

</div>

