<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="SITEONOT"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                
                <asset:stylesheet src="main.css"/>
                <asset:stylesheet src="mobile.css"/>
                <asset:stylesheet src="mystyles.css"/>
                <asset:stylesheet src="jquery-ui-custom.css"/>
                
                <asset:javascript src="jquery-1.9.1.js"/>                
                <asset:javascript src="jquery-ui.js"/> 
                <asset:javascript src="myfunctions.js"/>
                <asset:javascript src="jquery.blockUI.js"/> 
                <style type="text/css">
                .custom-combobox {
                  position: relative;
                  display: inline-block;
                }
                .custom-combobox-toggle {
                  position: absolute;
                  top: 0;
                  bottom: 0;
                  margin-left: -1px;
                  padding: 0;
                }
                .custom-combobox-input {
                  margin: 0;
                  padding: 5px 10px;
                }
                </style>             
                <script>
                    //TOOLTIP
                    $(function() {
                    $( document ).tooltip({
                        position: {
                        my: "center bottom-20",
                        at: "center top",
                        using: function( position, feedback ) {
                            $( this ).css( position );
                            $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                        }
                        }
                    });
                    });

                    //DIALOGO MENSAJE
                    $(function() {
                    $( "#dialog-message" ).dialog({
                        modal: true,
                        autoOpen: false,
                        width:500,
                        height:200,            
                        show: {
                            effect: "blind",
                            duration: 500
                        },
                        hide: {
                            effect: "explode",
                            duration: 500
                        },            
                        buttons: {
                        "Aceptar": function() {
                            $( this ).dialog( "close" );
                        }
                        }
                    });
                    });

                    //DIALOGO CONFIRMACION
                    $(function() {
                        $( "#dialog-confirm" ).dialog({
                        resizable: false,
                        width:500,
                        height:200,
                        modal: true,
                        autoOpen: false,
                        buttons: {
                            "Aceptar": function() {
                            $( this ).dialog( "close" );
                            EjecutarProceso();
                            },
                            "Cancelar": function() {
                            $( this ).dialog( "close" );
                            }
                        }
                        }); 
                    });
                    //DIALOGO PREGUNTA
                    $(function() {
                        $( "#dialog-ask" ).dialog({
                        resizable: false,
                        width:500,
                        height:200,
                        modal: true,
                        autoOpen: false,
                        buttons: {
                            "Aceptar": function() {
                            $( this ).dialog( "close" );
                            EjecutarPregunta();
                            $("#valorDePregunta").val("")
                            },
                            "Cancelar": function() {
                            $( this ).dialog( "close" );
                            }
                        }
                        });
                    });
                </script>
		<g:layoutHead/>
		<g:javascript library="application"/>
		<r:layoutResources />
	</head>
	<body>
            <div id="contenido">
                <div id="dialog-message" title="Mensaje del Sistema">
                    <p id="alerta" class="error"></p>
                </div>

                <div id="dialog-confirm" title="Confirmar acción">
                    <p id="confirm" class="error"></p>
                </div>
                <div id="dialog-ask" title="Confirmar acción">
                    
                    <p id="ask" class="error"></p>
                        <g:textField name="valorDePregunta" />
                </div>
		<div id="grailsLogo" role="banner">
                    <table>
                        <colgroup>
                            <col span="1" style="width: 20%">
                            <col span="1" style="width: 50%">
                            <col span="1" style="width: 30%">
                        </colgroup>
                        <tr>
                            <td width="35%" align="left">
                                <img src="${assetPath(src: 'logoIceo.png')}" align="left" alt="Instituto Catastral del Estado de Oaxaca"/>
                            </td>
                            <td width="30%" align="center" valign="bottom">
                                <br/><br/>
                                <img src="${assetPath(src: 'logoSiteoProv2.png')}" valign="bottom" align="center" alt="SITEO"/>
                            </td>
                            <td width="35%" align="right">
                                <img src="${assetPath(src: 'generandoBien.png')}" alt="Gobierno del Estado de Oaxaca" align="rigth" />
                            </td>
                        </tr>
                    </table>
		</div>
                <g:if test="${session.user}">
                <div>
                    <table style="border-top: 0px; border-spacing: 0px; background-color: #f1e19e">
                        <tr>
                            <td style="text-align:left; width:33%; font-size: 12px; font-weight: bold">
                               <g:if test="${session.nombrecompleto}">
                                    ${session.nombrecompleto}
                                </g:if>
                            </td>
                            <td  style="text-align:center; width:33%; font-size: 12px; font-weight: bold">
                                <g:if test="${session.nombrerol}">
                                    ${session.nombrerol}
                                </g:if><br/>
                                <g:if test="${session.nombrenot}">
                                    ${session.nombrenot}
                                </g:if>
                            </td>
                            <td style="text-align:right; width:33%; font-size: 12px; font-weight: bold">
                                <g:formatDate format="dd-MM-yyyy" date="${new Date()}" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 10px">&nbsp;</div>
                </g:if>
		<g:layoutBody/>
		<div class="footer" role="contentinfo"></div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<r:layoutResources />
          <script language="javascript">
              function MostrarMensaje(mensaje){
                  var mens = document.getElementById('alerta');
                  //mens.textContent = mensaje;
                  $("#alerta").html(mensaje)
                  $( "#dialog-message" ).dialog( "open" );
              }

              function Confirmar(mensaje){
                  var mens = document.getElementById('confirm');
                  mens.textContent = mensaje;
                  $( "#dialog-confirm" ).dialog( "open" );
              }
              function Preguntar(mensaje){
                  var mens = document.getElementById('ask');
                  mens.textContent = mensaje;
                  $( "#dialog-ask" ).dialog( "open" );
              }
          </script>
          </div>
         </body>
</html>