package siteo.administracion.catalogos

class EntidadesCat {

    String nombre
    Integer clave
    String abrev;
    
    static constraints = {
        nombre(nullable:false, size:1..45, blank:true)
        clave(nullable:false)
        abrev(nullable:true, size:1..5, blank:true)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String nombreconclave(){
        clave.toString().padLeft(3,'0')+" - "+nombre
    } 
    String toString(){
        clave.toString().padLeft(2,'0')+" - "+nombre
    }  
}
