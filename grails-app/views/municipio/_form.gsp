<%@ page import="siteo.seguridad.Municipio" %>



<div class="fieldcontain ${hasErrors(bean: municipioInstance, field: 'nombre', 'error')} required">
	<label for="nombre">
		<g:message code="municipio.nombre.label" default="Nombre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nombre" maxlength="150" required="" value="${municipioInstance?.nombre}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: municipioInstance, field: 'clave', 'error')} required">
	<label for="clave">
		<g:message code="municipio.clave.label" default="Clave" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="clave" type="number" min="0" max="9999" value="${municipioInstance.clave}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: municipioInstance, field: 'delegaciones', 'error')} required">
	<label for="delegaciones">
		<g:message code="municipio.delegaciones.label" default="Delegaciones" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="delegaciones" name="delegaciones.id" from="${siteo.seguridad.Delegacion.list()}" optionKey="id" required="" value="${municipioInstance?.delegaciones?.id}" class="many-to-one"/>

</div>

