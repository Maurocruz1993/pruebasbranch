<%@ page import="siteo.administracion.catalogos.TipoPropietario" %>



<div class="fieldcontain ${hasErrors(bean: tipoPropietarioInstance, field: 'tipopropietario', 'error')} required">
	<label for="tipopropietario">
		<g:message code="tipoPropietario.tipopropietario.label" default="Tipo de propietario" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipopropietario" maxlength="15" required="" value="${tipoPropietarioInstance?.tipopropietario}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoPropietarioInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoPropietario.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoPropietarioInstance?.estatus}" />

</div>

