<%@ page import="siteo.administracion.catalogos.TipoTramite" %>



<div class="fieldcontain ${hasErrors(bean: tipoTramiteInstance, field: 'clave', 'error')} required">
	<label for="clave">
		<g:message code="tipoTramite.clave.label" default="Clave" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="clave" type="number" value="${tipoTramiteInstance.clave}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoTramiteInstance, field: 'tipotramite', 'error')} required">
	<label for="tipotramite">
		<g:message code="tipoTramite.tipotramite.label" default="Tipo de trámite" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tipotramite" maxlength="25" required="" value="${tipoTramiteInstance?.tipotramite}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tipoTramiteInstance, field: 'estatus', 'error')} ">
	<label for="estatus">
		<g:message code="tipoTramite.estatus.label" default="Estatus" />
		
	</label>
	<g:checkBox name="estatus" value="${tipoTramiteInstance?.estatus}" />

</div>

