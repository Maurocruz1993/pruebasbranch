package siteo.administracion.catalogos

class Viento {

    String viento
    String abreviacion
    Boolean estatus
    
    static constraints = {
        viento (nullable:false, size: 1..9, blank: false)
        abreviacion (nullable:false, size: 1..3, blank: false)
    }
    
    static mapping = {
        id generator: 'increment'
    }
    
    String toString(){
        viento
    }
}
